export const environment = {
    prod: false,
    apiUrl: 'http://localhost:8080', //'/backend' for development with testapi
    env: 'Dev',
    appName: 'Abc-Inter',
    version: '0.0.1',
};
