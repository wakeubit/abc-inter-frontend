export const environment = {
  prod: true,
  apiUrl: '/backend',
  env: 'Prod',
  appName: 'Abc-Inter',
  version: '0.0.1'
};
