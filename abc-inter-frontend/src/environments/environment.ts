export const environment = {
  prod: false,
  apiUrl: 'http://localhost:8080',
  env: 'Test',
  appName: 'Abc-Inter',
  version: '0.0.1'
};
