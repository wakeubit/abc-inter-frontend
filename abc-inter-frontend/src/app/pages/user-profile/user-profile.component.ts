import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from "@angular/forms";
import { UtilityService } from '../../services/utility/utility.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { TokenStorageService } from '../../services/jwt/token-storage.service';
import { Operator } from '../../models/operator/operator';
import { OperatorsService } from '../../services/operators/operators.service';
import { User } from '../../models/user/user';
import { UserType } from '../../models/user/userType';
import { TranslateService } from '@ngx-translate/core';
import { UsersService } from '../../services/users/users.service';
import { CustomersService } from '../../services/customers/customers.service';
import { Customer } from '../../models/customer/customer';
import { Address } from '../../models/address/address';
import { Gender } from '../../models/gender/gender';
import { environment } from 'src/environments/environment';
import { CustomerType } from 'src/app/models/customer/customerType';
import {init} from 'protractor/built/launcher';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  currentUrl: string;
  file: File;
  imageUrl: any;
  user = new User(null, null, null, null, null, false, null, null, null, null, null);
  operator: Operator;
  customer: Customer;
  roles: string[];
  isOperator = false;
  isCustomer = false;
  isAdmin = false;
  principalRole: string;
  gender: string;
  genders = [];
  isDefaultAvatar = false;
  host = `${environment.apiUrl}/user/avatar/`;
  phoneError = '';

  public usernameCtrl: FormControl = new FormControl({value:'', disabled: true});
  public firstnameCtrl: FormControl = new FormControl({value:'', disabled: true});
  public lastnameCtrl: FormControl = new FormControl({value:'', disabled: true});
  public emailCtrl = new FormControl({value:'', disabled: true}, [Validators.email]);
  public genderCtrl: FormControl = new FormControl({value:'', disabled: true});
  public phoneCtrl: FormControl = new FormControl({value:'', disabled: true});
  public cityCtrl: FormControl = new FormControl({value:'', disabled: true});
  public countryCtrl: FormControl = new FormControl({value:'', disabled: true});
  public addressCtrl: FormControl = new FormControl({value:'', disabled: true});
  public fixCtrl: FormControl = new FormControl({value:'', disabled: true});
  public aboutCtrl: FormControl = new FormControl({value:'', disabled: true});

  telOptions = { initialCountry: 'cm', onlyCountries: ['cm'] };
  isTelValid = true;
  errors = {
    fixPhoneNb: '',
    usernameError: '',
    lastnameError: '',
    firstnameError: '',
    emailError: '',
    cityError: '',
    countryError: '',
    adrError: '',
    fixError: '',
    phoneNb: '',
    errorMessage: ''
  };

  fixPhonePattern = '^[2][0-9]{8}$';

  initialGender: string;
  isFixPhoneValid: boolean;

  constructor(
    private operatorService: OperatorsService,
    private customersService: CustomersService,
    private usersService: UsersService,
    private router: Router,
    private utility: UtilityService,
    private tokenService: TokenStorageService,
    private modalService: NgbModal,
    private toastService: ToastrService,
    private translate: TranslateService,
    private utilityService: UtilityService,
  ) {}

  ngOnInit() {
    this.currentUrl = this.router.url.substr(1);
    // fetch the user
    this.getUser();
    this.principalRole = this.utility.getPrincipalRole();
    // get information by user type
    this.getUserCompleteInformation(this.utility.getUserType());
    this.customer = new Customer();
    this.customer.postalAddress = new Address(null, null, null, null, null, null, null);
    this.operator = new Operator();
  }

  private getUser() {
    this.usersService.findById(this.tokenService.currentUser().userUuid).subscribe(user => {
      this.user = user;
      this.gender = this.translate.instant('' + this.user.gender);
      if (this.user.profileImg === '' || null === this.user.profileImg || this.user.profileImg.length === 0) {
        this.isDefaultAvatar = true;
      }
      /*customer or operator contains user as a object value.
      By retrieving the user information, populate also the list of genders*/
    }, err => {
      this.isDefaultAvatar = false;
    });
  }

  private getUserCompleteInformation(userType: string) {
    if (userType === UserType.Operator || userType === UserType.Admin) {
      this.getOperatorByUserId(this.utility.userId);
      // change the Switcher state
      if (userType === UserType.Admin) {
        this.isAdmin = true;
      }
      this.isOperator = true;
      this.isCustomer = false;
    } else {
      this.getCustomerByUserId(this.utility.userId);
      this.isCustomer = true;
    }
  }

  private getOperatorByUserId(id: string) {
    this.operatorService.getAllOperators().subscribe((data) => {
      for (let i = 0; i < data.length; ++i) {
        if (data[i].user.userUuid === id) {
          this.operator = data[i];
          this.usernameCtrl.setValue(this.operator.user.username);
          this.lastnameCtrl.setValue(this.operator.user.lastname);
          this.firstnameCtrl.setValue(this.operator.user.firstname);
          this.genderCtrl.setValue(this.operator.user.gender);
          this.emailCtrl.setValue(this.operator.user.email);
          this.phoneCtrl.setValue(this.operator.user.tel);
          //
          this.initialGender = this.displayInitialGenderValue(this.operator.user.gender);
          this.initGenderOptions(this.initialGender);
          break;
        }
      }
    });
  }

  private getCustomerByUserId(id: string) {
    this.customersService.findAllCustomers().subscribe(data => {
      if (data) {
        this.customer = new Customer();
        for (let i = 0; i < data.length; ++i) {
          if (data[i].user.userUuid === id) {

            this.customer = data[i];
            this.usernameCtrl.setValue(this.customer.user.username);
            this.lastnameCtrl.setValue(this.customer.user.lastname);
            this.firstnameCtrl.setValue(this.customer.user.firstname);
            this.genderCtrl.setValue(this.customer.user.gender);
            this.emailCtrl.setValue(this.customer.user.email);
            this.phoneCtrl.setValue(this.customer.user.tel);

            if (this.customer.postalAddress === null) {
              this.customer.postalAddress = new Address("", "", "", "", null, 0, 0);
            }

            this.cityCtrl.setValue(this.customer.postalAddress.city);
            this.countryCtrl.setValue(this.customer.postalAddress.country);
            this.addressCtrl.setValue(this.customer.postalAddress.streetAndHouseNumber);
            this.fixCtrl.setValue(this.customer.postalAddress.telAtAddress);
            this.aboutCtrl.setValue(this.customer.postalAddress.additionalInformation);
            //
            this.initialGender = this.displayInitialGenderValue(this.customer.user.gender);
            this.initGenderOptions(this.initialGender);
            this.customer.customerType = this.translate.instant(`global.${this.customer.customerType.toLocaleLowerCase()}`);
            break;
          }
        }
      }
    });
  }

  displayInitialGenderValue(value: string): string {
    if (value?.toLowerCase() === 'female' || value?.toLowerCase() === 'feminin') {
      return this.translate.instant('global.female');
    }
    if (value?.toLowerCase() === 'male' || value?.toLowerCase() === 'masculin') {
      return this.translate.instant('global.male');
    }
    if (value?.toLowerCase() === 'others' || value?.toLowerCase() === 'autres') {
      return this.translate.instant('global.others');
    }
    return this.translate.instant('global.others');
  }

  initGenderOptions(initialValue: string): void {
    if (initialValue.toLowerCase() === 'female') {
      this.genders = [
        this.translate.instant('global.male'),
        this.translate.instant('global.others')
      ]
    } else if (initialValue.toLowerCase() === 'male') {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.others')
      ]
    } else if (initialValue.toLowerCase() === 'others') {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.male')
      ]
    } else {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.male'),
        this.translate.instant('global.others')
      ]
    }
  }

  // This method is to call when a user submit information with new selected profile image
  private saveOperator() {
    // if gender change than pass the new value before sending. Since we use an event binding on it.
    if (this.genderCtrl.value.toLocaleLowerCase() === 'masculin' || this.genderCtrl.value.toLocaleLowerCase() === 'male') {
      this.user.gender = Gender.Male;
    } else if (this.genderCtrl.value.toLocaleLowerCase() === 'feminin' || this.genderCtrl.value.toLocaleLowerCase() === 'female' ||
               this.genderCtrl.value.toLocaleLowerCase() === 'féminin') {
      this.user.gender = Gender.Female;
    } else {
      this.user.gender = Gender.Others;
    }

    this.operator.user = new User(this.user.userUuid, this.usernameCtrl.value, this.lastnameCtrl.value, this.firstnameCtrl.value,
                                  this.user.password, false, this.emailCtrl.value, this.phoneCtrl.value, this.user.gender,
                                  this.user.profileImg, this.user.roles);

    /*if user avatar is to be submitted, than call the service
    that use formData as argument.*/
    if (this.file) {
      const form = new FormData();
      form.append('file', this.file);
      form.append('user', JSON.stringify(this.user));
      // first save the user before saving the operator's information.
      this.usersService.update(form).subscribe((data) => {
        if (data) {
          this.operatorService.updateOperator(this.operator).subscribe(result => {
            // If username change, than change the value in localstorage.
            this.utility.username = this.user.username;
            // notify changes to the others
            this.utility.changeDataSubject(this.user);
            // notify the user abort the save process and disable input
            this.toastService.success(this.translate.instant('user-profile.success-save'));
            this.errors.errorMessage = '';
            window.location.reload();
          }, err => {
            this.errors.errorMessage = err.error.message;
            this.toastService.error(this.translate.instant('user-profile.error-save'));
          });
        }
      }, (err) => {
        // Check the error type (either duplicate email or username) and display it.
        if (err.error?.message) {
          this.errors.errorMessage = err.error.message;
          if (this.errors.errorMessage.includes('Username')) {
            this.errors.errorMessage = this.translate.instant('global.username-taken');
          } else if (this.errors.errorMessage.includes('Email')) {
            this.errors.errorMessage =  this.translate.instant('global.email_in_use');
          } else  if (this.errors.errorMessage.includes('size')) {
            this.errors.errorMessage = this.translate.instant('global.image_size');
          } else {
            this.errors.errorMessage = this.translate.instant('global.unex_error');
          }
        }

        this.toastService.error(this.translate.instant('user-profile.error-save'));
      });
    } else {
      // otherwise call the other save service.
      this.operatorService.updateOperator(this.operator).subscribe(result => {
        if (result) {
          // If username change, than change the value in localstorage.
          this.utility.username = this.user.username;
          // notify changes
          this.utility.changeDataSubject(this.user);
          this.toastService.success(this.translate.instant('user-profile.success-save'));
          this.errors.errorMessage = '';
          window.location.reload();
        }
      }, err => {
        if (err.error?.message) {
          this.errors.errorMessage = err.error.message;
          if (this.errors.errorMessage.includes('Username')) {
            this.errors.errorMessage = this.translate.instant('global.username-taken');
          } else if (this.errors.errorMessage.includes('Email')) {
            this.errors.errorMessage =  this.translate.instant('global.email_in_use');
          } else {
            this.errors.errorMessage = this.translate.instant('global.unex_error');
          }
        }
        this.toastService.error(this.translate.instant('user-profile.error-save', this.errors.errorMessage));
      });
    }
  }

  saveCustomer() {
    // if gender change than pass the new value before sending. Since we use an event binding on it.
    if (this.genderCtrl.value.toLocaleLowerCase() === 'masculin' || this.genderCtrl.value.toLocaleLowerCase() === 'male') {
      this.user.gender = Gender.Male;
    } else if (this.genderCtrl.value.toLocaleLowerCase() === 'feminin' || this.genderCtrl.value.toLocaleLowerCase() === 'female' ||
               this.genderCtrl.value.toLocaleLowerCase() === 'féminin') {
      this.user.gender = Gender.Female;
    } else {
      this.user.gender = Gender.Others;
    }

    // convert the value of customerType to type customerType
    if (
      this.customer.customerType.toLocaleLowerCase() === 'privée' ||
      this.customer.customerType.toLocaleLowerCase() === 'privee' ||
      this.customer.customerType.toLocaleLowerCase() === 'private'
    ) {
      this.customer.customerType = CustomerType.Private;
    } else if (this.customer.customerType.toLocaleLowerCase() === 'anonym') {
      this.customer.customerType = CustomerType.Anonym;
    } else {
      this.customer.customerType = CustomerType.Business;
    }

    this.customer.user = new User(this.user.userUuid, this.usernameCtrl.value, this.lastnameCtrl.value, this.firstnameCtrl.value,
                                  this.user.password, false, this.emailCtrl.value, this.phoneCtrl.value, this.user.gender,
                                  this.user.profileImg, this.user.roles);

    this.customer.postalAddress = new Address(this.cityCtrl.value, this.countryCtrl.value, this.addressCtrl.value,
                                              this.aboutCtrl.value, this.fixCtrl.value, 0, 0);

    /*if user avatar is to be submitted, than call the service
    that use formData as argument.*/
    if (this.file) {
      const form = new FormData();
      form.append('file', this.file);
      form.append('user', JSON.stringify(this.user));
      // It is necessary to know which user whose profile picture is to be saved. So send the user.
      // first save the user and profile image before save the customer's information.
      this.usersService.update(form).subscribe(data => {
        // If the image is successfully saved in images directory, than save the user information
        if (data) {
          this.customersService.updateCustomer(this.customer).subscribe(res => {
            // If username change, than change the value in localstorage.
            this.utility.username = this.user.username;
            // notify changes to the others
            this.utility.changeDataSubject(this.user);
            // notify the user abort the save process and disable input
            this.toastService.success(this.translate.instant('user-profile.success-save'));
            this.errors.errorMessage = '';
            window.location.reload();
          }, err => {
            this.errors.errorMessage = err.error.message;
            this.toastService.error(this.translate.instant('user-profile.error-save'));
          });
        }
      }, err => {
        this.errors.errorMessage = err.error.message;
        // Check the error type (either duplicate email or username) and display it.
        if (err.error?.message) {
          this.errors.errorMessage = err.error.message;
          if (this.errors.errorMessage.includes('Username')) {
            this.errors.errorMessage = this.translate.instant('global.username-taken');
          } else if (this.errors.errorMessage.includes('Email')) {
            this.errors.errorMessage =  this.translate.instant('global.email_in_use');
          } else  if (this.errors.errorMessage.includes('size')) {
            this.errors.errorMessage = this.translate.instant('global.image_size');
          } else {
            this.errors.errorMessage = this.translate.instant('global.unex_error');
          }
        }
        this.toastService.error(this.translate.instant('user-profile.error-save'));
      });
    } else {
      // otherwise call the other save service.
      this.customersService.updateCustomer(this.customer).subscribe(result => {
        if (result) {
          // If username change, than change the value in localstorage.
          this.utility.username = this.user.username;
          // notify changes to the others
          this.utility.changeDataSubject(this.user);
          this.toastService.success(this.translate.instant('user-profile.success-save'));
          this.errors.errorMessage = '';
          window.location.reload();
        }
      }, err => {
        if (err.error?.message) {
          this.errors.errorMessage = err.error.message;
          if (this.errors.errorMessage.includes('Username')) {
            this.errors.errorMessage = this.translate.instant('global.username-taken');
          } else if (this.errors.errorMessage.includes('Email')) {
            this.errors.errorMessage =  this.translate.instant('global.email_in_use');
          } else {
            this.errors.errorMessage = this.translate.instant('global.unex_error');
          }
        }
        this.toastService.error(this.translate.instant('user-profile.error-save'));
      });
    }
  }

  onImageSelected(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
      //
      if (this.file.type.match(/image\/*/) == null) {
        this.errors.errorMessage = this.translate.instant('user-profile.err_upload');
        return;
      }

      const fr = new FileReader();
      fr.readAsDataURL(this.file);
      fr.onload = (ev => {
        this.imageUrl = fr.result;
        // notify others component
        this.utility.changeDataSubject({ 'image': this.imageUrl });
      });
    }
  }

  telInputObject(obj) {
    obj.setCountry('cm');
  }

  checkValidPhoneNumber(event) {
    const reg = this.utility.regPortableNumber;
    this.hasError(reg.test(event.target.value));
  }

  // returns true if phone number is valid otherwise false.
  hasError(isValid) {
    this.isTelValid = isValid;
    if (this.isTelValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  isFormValid(): boolean {
    if (this.isCustomer) {
      if (
        this.usernameCtrl.value && this.lastnameCtrl.value && this.firstnameCtrl.value && this.genderCtrl.value &&
        (!this.emailCtrl.value || !this.emailCtrl.invalid) && this.isTelValid && this.cityCtrl.value &&
        this.countryCtrl.value && this.addressCtrl.value && !this.fixCtrl.invalid
      ) {
        return true;
      }
    } else {
      if (
        this.usernameCtrl.value && this.lastnameCtrl.value && this.firstnameCtrl.value && this.genderCtrl.value &&
        (!this.emailCtrl.value || !this.emailCtrl.invalid) && this.isTelValid
      ) {
        return true;
      }
    }
    return false;
  }

  saveChange() {
    if (this.isTelValid) {
      if (this.isOperator) {
        this.saveOperator();
      } else {
        this.saveCustomer();
      }
    } else {
      this.errors.errorMessage = this.translate.instant('global.phone-input-error');
    }
  }

  enableInput() {
    this.usernameCtrl.enable();
    this.lastnameCtrl.enable();
    this.firstnameCtrl.enable();
    this.genderCtrl.enable();
    this.emailCtrl.enable();
    this.phoneCtrl.enable();
    this.cityCtrl.enable();
    this.countryCtrl.enable();
    this.addressCtrl.enable();
    this.fixCtrl.enable();
    this.aboutCtrl.enable();
  }

  changeUserLocation(content) {
    this.modalService.open(content, { centered: true, scrollable: true });
  }

  saveUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        if (position) {
          this.customer.postalAddress.latitude = position.coords.latitude;
          this.customer.postalAddress.longitude = position.coords.longitude;

          // convert the value of customerType to type customerType
          if (
            this.customer.customerType.toLocaleLowerCase() === 'privée' ||
            this.customer.customerType.toLocaleLowerCase() === 'privee' ||
            this.customer.customerType.toLocaleLowerCase() === 'private'
          ) {
            this.customer.customerType = CustomerType.Private;
          } else if (this.customer.customerType.toLocaleLowerCase() === 'anonym') {
            this.customer.customerType = CustomerType.Anonym;
          } else {
            this.customer.customerType = CustomerType.Business;
          }

          this.customersService.updateCustomer(this.customer).subscribe(result => {
            if (result) {
              this.toastService.success(this.translate.instant('global.update-success'));
            }
          }, err => {
            this.toastService.success(this.translate.instant('global.update-error'));
          });
        }
      }, err => {
        console.error(err);
      });
    } else {
      console.error("The browser does not support geolocation.");
    }
    this.modalService.dismissAll();
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  checkValidFixPhoneNumber(event) {
    const reg = this.utilityService.regFixOrPortableNumber;
    this.checkFixPhone(reg.test(event.target.value));
  }

  private checkFixPhone(isValid: boolean) {
    this.isFixPhoneValid = isValid;
    if (this.isFixPhoneValid) {
      this.errors.fixPhoneNb = '';
    } else {
      this.errors.fixPhoneNb = this.translate.instant('global.phone-input-error');
    }
  }
}
