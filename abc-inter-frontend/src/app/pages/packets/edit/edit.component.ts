import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Packet } from 'src/app/models/packet/packet';
import { Registration } from 'src/app/models/registration/registration';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditPacketComponent implements OnInit {

  loaded: boolean = true;
  packetEdited: Packet;
  currentUrl: string;
  route: Array<string>;
  shippingcostfail: boolean = false;
  registrationId: string;
  packetId: string;
  errors = {
    errorMsg: ''
  }

  constructor(
    private router: Router,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private registrationService: RegistrationService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
  ) {
    this.registrationId = this.activatedRoute.snapshot.paramMap.get('registrationId');
    this.packetId = this.activatedRoute.snapshot.paramMap.get('packetId');
  }

  ngOnInit(): void {
    this.getCurrentUrl();
  }

  private getCurrentUrl(): void {
    const ROUTES: Array<string> = this.router.url.substr(1).split('/');
    this.route = new Array<string>();
    for (let i = 0; i < 2; ++i) {
      this.route.push(this.currentUrl = this.router.url.substr(1).split('/')[i]);
    }
    this.currentUrl = this.router.url.substr(1).split('/')[1];
    this.loaded = false;
  }

  private getPacketCurrentValues(packet: Packet) {
    this.packetEdited = packet;
    if (this.packetEdited.shippingCost === null) {
      this.shippingcostfail = true;
    } else {
      this.shippingcostfail = false;
    }
  }

  private checkPacketForm(packet: Packet): boolean {
    if (packet.shippingCost === null) {
      this.errors.errorMsg = this.translate.instant('registration.cost_error_msg');
      return false;
    }

    if (packet.weight <= 0 || packet.weight > packet.shippingCost.category.maxWeight) {
      this.errors.errorMsg = this.translate.instant('global.weightExceeded');
      return false;
    }
    this.errors.errorMsg = '';
    return true;
  }

  updatePacket() {
    const canBeSaved: boolean = this.checkPacketForm(this.packetEdited);
    if (canBeSaved) {
      this.registrationService.updatePacketFromRegistration(
        this.registrationId,
        this.packetEdited
        ).subscribe(response => {
          this.toastrService.success(this.translate.instant('packets.successful_update'));
          this.location.back();
        }, err => {
          if (err?.error) {
            this.toastrService.error(this.translate.instant('packets.error_update'));
            console.error(err?.error?.message);
          }
        });
    }
  }

  private updateRegistration(registration: Registration): Promise<Registration> {
    return new Promise((resolve, reject) => {
      this.registrationService.updateRegistration(registration).subscribe(res => {
        resolve(res);
      }, err => {
        console.error(err?.error?.message);
      });
    });
  }

  renderRegistrationView() {
    this.router.navigate(['/packets-registration']);
  }

  refresh() {
    this.getCurrentUrl();
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }
}
