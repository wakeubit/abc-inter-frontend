import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { PacketsService } from '../../services/packets/packets.service';
import { Packet } from '../../models/packet/packet';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { PacketStore } from 'src/app/models/packet-store/packet-store';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { Registration } from 'src/app/models/registration/registration';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { STATUSTYPE } from 'src/app/models/statusType/statusType';
import { PacketStateService } from 'src/app/services/packet-state/packet-state.service';
import { PageResult } from 'src/app/models/packet/page-result';
import { PacketState } from 'src/app/models/packet-state/packetState';

@Component({
  selector: 'app-packets',
  templateUrl: './packets.component.html',
  styleUrls: ['./packets.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class PacketsComponent implements OnInit {

  form: any = {};
  currentUrl: string;
  totalItems: number;
  totalPages: number;
  sizeTable = 10;
  sizeCard = 8;
  size: number;
  page = 1;
  packets: Array<Packet>;
  packetsList = [];
  stores: Array<PacketStore>;
  protected currentSelectedPacket: Packet;
  currentPacketLocation: PacketStore;
  isSuccessful = false;
  hide = true;
  trackingNb: string;
  key: string = 'trkNb';
  reverse: boolean = false;
  isAsTable: boolean = true;
  loaded: boolean = true;
  isServerDown: boolean = false;
  protected packetRegistration: Registration;
  serverDownMsg = '';
  isOnlyOnePacket: boolean;
  isAdmin: boolean;
  deliveryStatus = {
    [STATUSTYPE.IN_STORE]: {'status': 'packets.in_store', 'color': 'text-info'},
    [STATUSTYPE.READY_FOR_DELIVERY]: {'status': 'packets.ready_for_delivery', 'color': 'text-warning'},
    [STATUSTYPE.DELIVERED]: {'status': 'packets.delivered', 'color': 'text-success'},
  };
  lastPacketStates: Map<string, PacketState> = new Map();

  constructor(
    private router: Router,
    private packetsService: PacketsService,
    private modalService: NgbModal,
    private translate: TranslateService,
    private toastr: ToastrService,
    private registrationService: RegistrationService,
    private packetStateService: PacketStateService,
    private utilityService: UtilityService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {
    this.currentUrl = this.router.url.substr(1);
    this.initData();
    // the click on the search btn
    $('#search-packet').click(() => {
      this.hide = this.hide === true ? false : true;
    });
  }

  initData() {
    this.isAdmin = this.utilityService.isAdmin;
    this.packets = new Array<Packet>();
    this.stores = new Array<PacketStore>();
    this.setItemSizePerPage();
    this.getAllPackets(0, this.size);
  }

  getAllPackets(page: number, size: number) {
    this.packetsService.findAllPackets(page, size).subscribe({
      next: (result: PageResult<Packet>) => {
        if (result != null) {
          this.packets = result.items;
          this.packetsList = [ ...result.items ];
          this.totalItems = result.totalItems;
          this.totalPages = result.totalPages;
          this.page = result.currentPage + 1;
          this.loaded = false;
        }
      },
      error: (err) => {
        if (!!err.error.message) {
          console.error(err);
        } else {
          this.isServerDown = true;
        }
      },
      complete: () => {
        this.packets.map(packet => {
          this.packetStateService.findPacketStatesByPacketUuid(packet.packetUuid).subscribe(
            states => {
              const packetStates = states.sort((a, b) => {
                return new Date(b.createdAt).getTime() -new Date(a.createdAt).getTime();
              });
              this.lastPacketStates.set(packet.packetUuid, packetStates[0]);}
          );
        })
      }
    });
  }

  searchAllPacketsContainingTrackingNumber(page: number, size: number) {
    this.packetsService.searchAllPacketsContainingTrackingNumber(this.trackingNb, page, size).subscribe(
      result => {
        if (result != null) {
          this.packetsList = [ ...result.items ];
          this.totalItems = result.totalItems;
          this.totalPages = result.totalPages;
          this.page = result.currentPage + 1;
          this.loaded = false;
        }
      }
    );
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getPackets(page?: number, size?: number) {
    if (!this.trackingNb) {
      this.getAllPackets(page, size)
    } else {
      this.searchAllPacketsContainingTrackingNumber(page, size);
    }
  }

  getPacketCurentLocation(packet: Packet) {
    // set the receiving address as packet current position
    this.registrationService.findById(packet.registrationUuid).subscribe((registration: Registration) => {
      if (registration) {
        this.packetRegistration = registration;
        // if the value of the storeId is equal than 0, than the packet has been already delivered
        if (packet.currentStoreUuid === null) {
          this.currentPacketLocation = {
            packetStoreUuid: null,
            name: null,
            address: registration.receivingAddress,
            volume: registration.store.volume
          };
        } else {
          this.currentPacketLocation = {
            packetStoreUuid: registration.store.packetStoreUuid,
            name: registration.store.name,
            address: registration.receivingAddress,
            volume: registration.store.volume
          };
        }
      }
    }, err => {
      console.error(err?.error);
    });
  }

  getcurrentSelectedPacket(packet: Packet, view) {
    this.currentSelectedPacket = packet;
    this.getPacketCurentLocation(packet);
    this.modalService.open(view, { centered: true, scrollable: true, size: 'xl' });
  }

  editSelectedPacket(packet: Packet) {
    this.currentSelectedPacket = packet;
    this.router.navigate([`registrations/${packet.registrationUuid}/packets/edit/${packet.packetUuid}`]);
  }

  deleteSelectedPacket(packet, view) {
    this.currentSelectedPacket = packet;
    this.registrationService.findById(this.currentSelectedPacket.registrationUuid).subscribe(registration => {
      if (null !== registration) {
        this.isOnlyOnePacket = registration.registeredPackets.length === 1;
        this.modalService.open(view, {
          centered: true, scrollable: true, size: 'md'
        });
      }
    }, err => {
      console.error(err?.error);
    });
  }

  deletePacket() {
    if (this.isOnlyOnePacket) {
      this.registrationService.deleteById(this.currentSelectedPacket.registrationUuid).subscribe(result => {
        for (let i = 0; i < this.packetsList.length; i++) {
          if (this.packetsList[i].packetUuid === this.currentSelectedPacket.packetUuid) {
            this.packetsList.splice(i, 1);
          }
        }
        this.toastr.success(this.translate.instant('packets.delete-success'), this.translate.instant('operators.delete'));
      });
    } else {
      this.packetsService.deletePacketById(this.currentSelectedPacket.packetUuid).subscribe(
        data => {
          for (let i = 0; i < this.packetsList.length; i++) {
            if (this.packetsList[i].packetUuid === this.currentSelectedPacket.packetUuid) {
              this.packetsList.splice(i, 1);
            }
          }
          this.toastr.success(this.translate.instant('packets.delete-success'), this.translate.instant('operators.delete'));
        },
        err => {
          this.toastr.error(this.translate.instant('operators.delete-failed'), this.translate.instant('operators.delete'));
        }
      );
    }
    this.modalService.dismissAll();
  }

  displaySelectedPacketCurrentLocation(packet: Packet, view) {
    this.getPacketCurentLocation(packet);
    this.currentSelectedPacket = packet;
    this.modalService.open(view, { centered: true, scrollable: true, size: 'sm' });
  }

  getDeliveryInformation(packet: Packet) {
    const lastState = this.lastPacketStates.get(packet.packetUuid);
    return lastState ? this.deliveryStatus[lastState.status] : this.deliveryStatus[STATUSTYPE.IN_STORE];
  }

  getPacketRegistrationInformation(packet: Packet, view) {
    this.currentSelectedPacket = packet;
    this.registrationService.findById(packet.registrationUuid).subscribe(registration => {
      if (null !== registration) {
        this.packetRegistration = registration;
        this.modalService.open(view, { centered: true, scrollable: true, size: 'sm' });
      }
    }, err => {
      console.error(err?.error);
    });
  }

  displayRegistrationDetails() {
    this.modalService.dismissAll();
    // Check whether the packet has been already delivered or not and redirect user one of those pages
    if (this.currentSelectedPacket.currentStoreUuid !== null) {
      this.router.navigate([`/orders/details/${this.packetRegistration?.registrationUuid}`]);
    } else {
      this.router.navigate([`/delivery/details/${this.packetRegistration?.registrationUuid}`]);
    }
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  setItemSizePerPage() {
    this.size = this.isAsTable ? this.sizeTable : this.sizeCard;
  }

  refresh() {
    this.isServerDown = false;
    this.getAllPackets(this.page-1, this.size);
    // clear the input
    this.trackingNb = '';
    // and hide the searchbar
    this.hide = true;
  }

  renderRegistrationView() {
    this.router.navigate(['/packets-registration']);
  }
}

