import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";
import { Packet } from "src/app/models/packet/packet";
import { Registration } from "src/app/models/registration/registration";
import { RegistrationService } from "src/app/services/registration/registration.service";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.css"],
})
export class AddPacketComponent implements OnInit {
  registration: Registration;

  packetToAdd: Packet;

  errors = {
    errorMsg: "",
  };

  currentUrl: string;

  isRegistrationFormValid: boolean;

  constructor(
    private activatedroute: ActivatedRoute,
    private registrationService: RegistrationService,
    private toastrService: ToastrService,
    private location: Location,
    private translateService: TranslateService,
    private router: Router
  ) {
    this.isRegistrationFormValid = false;
  }

  ngOnInit(): void {
    this.currentUrl = this.router.url.substr(1).split("/")[1];
    this.getRegistration();
  }

  getRegistration() {
    this.activatedroute.paramMap.subscribe((params) => {
      const registrationId = params.get("registrationId");
      this.registrationService.findById(registrationId).subscribe(
        (registration) => {
          if (registration) {
            this.registration = registration;
          }
        },
        (err) => {
          console.error(err?.error?.message);
        }
      );
    });
  }

  getPacket(packet) {
    this.packetToAdd = packet;
    // check whether the form is valid or not
    this.isFormValid();
  }

  addPacketToRegistration() {
    this.errors.errorMsg = "";
    if (this.packetToAdd && this.isRegistrationFormValid) {
      this.registration.registeredPackets.push(this.packetToAdd);
      this.registrationService.updateRegistration(this.registration).subscribe(
        (result) => {
          this.toastrService.success(
            this.translateService.instant("global.update-success")
          );
          this.errors.errorMsg = "";
          this.location.back();
        },
        (err) => {
          this.errors.errorMsg = this.translateService.instant(
            "global.update-error"
          );
          this.toastrService.error(
            this.translateService.instant("global.update-error")
          );
        }
      );
    } else {
      this.errors.errorMsg = this.translateService.instant(
        "registration.cost_error_msg"
      );
    }
  }

  isFormValid() {
    if (
      (this.packetToAdd && !this.packetToAdd.shippingCost) ||
      this.packetToAdd.weight <= 0 ||
      this.packetToAdd.shippingCost === null ||
      this.packetToAdd.positionInStore === null ||
      this.packetToAdd.positionDetails === null ||
      this.packetToAdd.weight > this.packetToAdd.shippingCost.category.maxWeight
    ) {
      this.isRegistrationFormValid = false;
    } else {
      this.isRegistrationFormValid = true;
    }
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }
}
