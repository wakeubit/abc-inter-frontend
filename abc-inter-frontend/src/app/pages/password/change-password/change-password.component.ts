import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../../models/user/user';
import {UsersService} from '../../../services/users/users.service';
import {UtilityService} from '../../../services/utility/utility.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  currentUrl: string;
  user = new User(null, null, null, null, null, false, null, null, null, null, null);
  host =  environment.apiUrl + '/user/avatar/';
  form: any = {};

  constructor(
    private router: Router,
    private usersService: UsersService,
    private utility: UtilityService,
    private modalService: NgbModal,
    private toastService: ToastrService,
    private translate: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.currentUrl = this.router.url.substr(1).toUpperCase();
    this.currentUrl = this.currentUrl.split('/')[0].toLocaleLowerCase();
    this.getUserByUserId(this.utility.userId);
  }

  private getUserByUserId(id: string) {
    this.usersService.findAll().subscribe(data => {
      for (let i = 0; i < data.length; ++i) {
        if (data[i].userUuid === id) {
          this.user = data[i];
          break;
        }
      }
    });
  }

  confirmSavingChange(content) {
    this.modalService.open(content, {centered: true, scrollable: true});
  }

  savePassword() {
    this.user.password = this.form['password'];
    this.usersService.updateById(this.user).subscribe((res) => {
      if (res) {
        this.toastService.success(this.translate.instant('password.success'));
        this.modalService.dismissAll();
      }
    }, (err) => {
      if (err) {
        this.toastService.error(this.translate.instant('password.error'));
      }
    });
  }
  
  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }
}
