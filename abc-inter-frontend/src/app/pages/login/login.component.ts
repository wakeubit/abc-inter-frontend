import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from "@angular/forms";

import { AuthService } from '../../services/auth/auth.service';
import { TokenStorageService } from '../../services/jwt/token-storage.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: any = {};
  returnUrl: string;
  hide = true;
  pwdError: string = 'Min. 6 characters!';
  usernameError: string;
  loading = false;
  isLoggedIn = false;
  errorMessage = '';

  public usernameCtrl: FormControl = new FormControl();
  public pwdCtrl: FormControl = new FormControl();

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private toastService: ToastrService,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    // check if the user is already logged in.
    if (this.tokenStorage.currentUser()) {
      this.isLoggedIn = true;
      // redirect then to dashboard
      this.router.navigate(['/dashboard']);
    }
    // is user just register display successful registration
    this.route.queryParams.subscribe(params => {
      if (params.registered !== undefined && params.registered === 'true') {
        this.toastService.success(this.translate.instant('registration.reg_success'));
      }
    });
    // get return url from route parameters or redirect to dashboard
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/dashboard';
  }

  login() {
    if (this.usernameCtrl.value && this.pwdCtrl.value) {
      const username = this.usernameCtrl.value;
    const pwd = this.pwdCtrl.value;
    // check whether there is invalid input or not
    if (username.match(/^ *$/) !== null || username.indexOf(' ') >= 0) {
      this.usernameError = this.translate.instant('global.white_space');
    } else {
      // inputs are valid
      this.loading = true;
      this.form.username = username;
      this.form.password = pwd;
      this.authService.login(this.form).subscribe(
        data => {
          this.tokenStorage.saveUser(data);
          this.tokenStorage.saveToken(data.token);
          this.tokenStorage.saveRefreshToken(data.refreshToken);
          this.isLoggedIn = true;

          this.router.navigate([this.returnUrl]);
        },
        err => {
          if (err?.error?.message) {
            // TODO: The output error msg must be translated
            this.errorMessage = err.error.message.indexOf('Username') >= 0 ? err.error.message : this.translate.instant('global.unex_error');
          } else {
            this.errorMessage = this.translate.instant('global.unex_error');
          }
          this.loading = false;
        }
      );
    }
    }
  }

  clearErrorMessage() {
    this.usernameError = '';
  }
}
