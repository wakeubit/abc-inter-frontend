import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {trigger, state, style, animate, transition} from '@angular/animations';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PacketCategory} from '../../models/packet-category/packetCategory';
import {PacketCategoryService} from '../../services/packet-category/packet-category.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import * as $ from 'jquery';
import { UtilityService } from 'src/app/services/utility/utility.service';

@Component({
  selector: 'app-packet-category',
  templateUrl: './packet-category.component.html',
  styleUrls: ['./packet-category.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({opacity: 0, transform: 'translateX(-200px)'}),
        animate(
          '300ms ease-in',
          style({opacity: 1, transform: 'translateX(0)'})
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({transform: 'translateX(0)'})),
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class PacketCategoryComponent implements OnInit {

  currentUrl: string;

  isSuccessful = false;

  form: any = {};

  errorMessage = '';

  page: number = 1;

  packetCategoriesList = [];

  packetCategory: PacketCategory = new PacketCategory(
    null,
    null,
    0,
    0,
    0,
    0,
  );

  allPacketCategories: {
    packetCategoryUuid: string,
    name: string,
    maxWeight: number,
    maxLength: number,
    maxWidth: number,
    maxHeight: number
  }[] = [];

  totalItems: number;

  hide = true;

  packetCatName: string;

  loaded = true;

  isServerDown = false;

  isAsTable = true;

  key = 'username';

  reverse = false;

  serverDownMsg = '';

  isAdmin = false;

  isCommercial = false;

  isDeliverer = false;

  constructor(private router: Router,
    private modalService: NgbModal,
    private packetCategoryService: PacketCategoryService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private utilityService: UtilityService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {

    this.getCurrentUrl();

    this.getAllPacketCategories();

    // the click on the search btn
    $('#search-packet-cat').click(() => {
      this.hide = this.hide === true ? false : true;
    });

    this.initData();
  }

  getCurrentUserRole() {
    this.isAdmin = this.utilityService.isAdmin;
    this.isCommercial = this.utilityService.isCommercial;
    this.isDeliverer = this.utilityService.isDeliverer;
  }

  initData() {
    this.getCurrentUserRole();
  }

  search() {
    if (this.packetCatName === '') {
      this.getAllPacketCategories();
    } else {
      this.packetCategoriesList = this.allPacketCategories.filter(res => {
        return res.name.toLocaleLowerCase().match(this.packetCatName.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  private getCurrentUrl() {
    this.currentUrl = this.translate.instant(`global.${this.router.url.substring(1).toLowerCase()}`);
  }

  openModal(content: any) {
    this.modalService.open(content, { centered: true, scrollable: true });
  }

  addCategory() {
    this.packetCategory.name = this.form.name;
    this.packetCategory.maxWeight = this.form.maxWeight;
    this.packetCategory.maxLength = this.form.maxLength;
    this.packetCategory.maxWidth = this.form.maxWidth;
    this.packetCategory.maxHeight = this.form.maxHeight;
    this.createOrUpdateCategory(this.packetCategory);
  }

  clearInput(form) {
    form.resetForm();
  }

  preventInput(event, field) {
    let value = null;
    if (field.name === 'maxWeight') {
      value = this.form.maxWeight;
      if (value >= 999) {
        event.preventDefault();
        this.form.maxWeight = parseInt(value.toString().substring(0, 3));
      }
    }
    if (field.name === 'maxLength') {
      value = this.form.maxLength;
      if (value >= 999) {
        event.preventDefault();
        this.form.maxLength = parseInt(value.toString().substring(0, 3));
      }
    }
    if (field.name === 'maxWidth') {
      value = this.form.maxWidth;
      if (value >= 999) {
        event.preventDefault();
        this.form.maxWidth = parseInt(value.toString().substring(0, 3));
      }
    }
    if (field.name === 'maxHeight') {
      value = this.form.maxHeight;
      if (value >= 999) {
        event.preventDefault();
        this.form.maxHeight = parseInt(value.toString().substring(0, 3));
      }
    }

  }

  private createOrUpdateCategory(packetCategory: PacketCategory) {
    this.packetCategoryService.createPacketCategory(packetCategory).subscribe(
      data => {
        if (null != data) {
          // refresh the user list
          this.getAllPacketCategories();
          // if new user successful added, close the modal and reload the location
          this.modalService.dismissAll();
          // than notify
          this.toastr.success(this.translate.instant('packet-category.create-success'), this.translate.instant('packet-category.create'));
        }
      },
      err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastr.error(this.translate.instant('packet-category.create-failed'), this.translate.instant('packet-category.create'));
        } else {
          this.isServerDown = true;
        }
      }
    );
  }

  private getAllPacketCategories() {
    this.packetCategoryService.getAllPacketCategories().subscribe(
      data => {
        if (null != data) {
          this.allPacketCategories = data;
          this.totalItems = data.length;
          // save all user
          this.packetCategoriesList = data;
          // set the flag
          this.loaded = false;
        }
      },
      err => {
        this.isServerDown = true;
        console.error(err);
      }
    );
  }

  getPacketCategoryById(packetCategoryUuid: string) {
    if (packetCategoryUuid != null) {
      this.packetCategoryService.getPacketCategoryById(packetCategoryUuid).subscribe(
        data => {
          if (null != data) {
            this.packetCategory = data;
          }
        },
        err => {
          if (!!err.error.message) {
            this.errorMessage = err.error.message;
          } else {
            this.errorMessage = this.translate.instant('global.unex_error');;
          }
        });
    } else {
      this.form.name = null;
      this.form.maxWeight = null;
      this.form.maxLength = null;
      this.form.maxWidth = null;
      this.form.maxHeight = null;
    }
  }

  deletePacketCategory(packetCategoryUuid: string) {
    this.packetCategoryService.deletePacketCategoryById(packetCategoryUuid).subscribe(
      data => {
        for (let i = 0; i < this.packetCategoriesList.length; ++i) {
          if (this.packetCategoriesList[i].packetCategoryUuid === packetCategoryUuid) {
            // decremente the value
            this.totalItems--;
            // Notify the user
            this.toastr.success(this.translate.instant('packet-category.delete-success'), this.translate.instant('packet-category.delete'));
            // remove the deleted category from the list and exit.
            return this.packetCategoriesList.splice(i, 1);
          }
        }
      },
      err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastr.error(this.translate.instant('packet-category.delete-failed'), this.translate.instant('packet-category.delete'));
        } else {
          this.isServerDown = true;
        }
      }
    );
    this.modalService.dismissAll();
  }

  updatePacketCategory(form) {
    this.packetCategory.name = form.value.nameEdit;
    this.packetCategory.maxHeight = form.value.maxHeightEdit;
    this.packetCategory.maxLength = form.value.maxLengthEdit;
    this.packetCategory.maxWeight = form.value.maxWeightEdit;
    this.packetCategory.maxWidth = form.value.maxWidthEdit;
    this.packetCategoryService.updateCategoryById(this.packetCategory).subscribe(
      data => {
        if (null != data) {
          // close the modal window
          this.modalService.dismissAll();
          // notify the user
          this.toastr.success(this.translate.instant('packet-category.update-success'), this.translate.instant('packet-category.update'));
          // refresh the list
          this.getAllPacketCategories();
        }
      },
      err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastr.error(this.translate.instant('packet-category.update-failed'), this.translate.instant('packet-category-update'));
        } else {
          this.isServerDown = true;
        }
      }
    );
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.packetCatName = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.getAllPacketCategories();
  }
}
