import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketCategoryComponent } from './packet-category.component';

describe('PacketCategoryComponent', () => {
  let component: PacketCategoryComponent;
  let fixture: ComponentFixture<PacketCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
