import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Address } from 'src/app/models/address/address';
import { Registration } from 'src/app/models/registration/registration';
import { RegistrationService } from 'src/app/services/registration/registration.service';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {

  currentUrl: string;

  addressToSave: Address;

  registration: Registration;

  type: string;

  errors = {
    errorMsg: ''
  }

  constructor(
    private registrationService: RegistrationService,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.currentUrl = this.router.url.substr(1).split('/')[1];
  }

  getAddress(object) {
    this.addressToSave = object.address;
    this.registration = object.registration;
    this.type = object.type;
  }

  updateAddress() {
    if (this.addressToSave && this.isAddressValid()) {
      // if a packet receiving address has to be changed
      if (this.type === 'packet' && this.registration) {
        this.registration.receivingAddress = this.addressToSave;
        this.registrationService.updateRegistration(this.registration).subscribe(result => {
          this.toastrService.success(this.translate.instant('global.update-success'));
          this.errors.errorMsg = '';
        }, err => {
          this.errors.errorMsg = this.translate.instant('global.update-error');
          this.toastrService.success(this.translate.instant('global.update-error'));
        });
      }
    } else {
      this.errors.errorMsg = this.translate.instant('global.required-fields');
    }
  }

  isAddressValid(): boolean {
    if (
      this.addressToSave.city?.length === 0 ||
      this.addressToSave.country?.length === 0 ||
      this.addressToSave.streetAndHouseNumber?.length === 0
    ) {
      return false;
    }
    return true;
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }
}
