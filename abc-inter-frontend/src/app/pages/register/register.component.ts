import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { TokenStorageService } from '../../services/jwt/token-storage.service';

import { Gender } from '../../models/gender/gender';
import {UtilityService} from '../../services/utility/utility.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: any = {};
  public firstnameCtrl: FormControl = new FormControl();
  public lastnameCtrl: FormControl = new FormControl();
  email = new FormControl('', [Validators.email]);
  public genderCtrl: FormControl = new FormControl();
  public phoneCtrl: FormControl = new FormControl();
  public pwdCtrl: FormControl = new FormControl();
  public pwdRepeatCtrl: FormControl = new FormControl();

  public hide = true;
  public isSignUpFailed = true;
  public usernameError: string;
  public firstnameError: string;
  public lastnameError: string;
  public emailError: string;
  public phoneError: string = '';
  public minLengthError1 = '';
  public minLengthError2 = '';
  public pwdError1: string;
  public pwdError2: string;
  public errorMessage = '';
  public isValid = false;

  errors = {
    phoneNb: '',
    errorMessage: ''
  };
  phone = '';

  genders = [];

  constructor(
    private authService: AuthService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private translate: TranslateService,
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    // check if the user is already logged in.
    if (this.tokenStorage.currentUser()) {
      // redirect then to dashboard
      this.router.navigate(['/dashboard']);
    }

    this.minLengthError1 = this.translate.instant('global.password-error');
    this.minLengthError2 = this.translate.instant('global.password-error');

    this.genders = [
      'male',
      'female',
      'others'
    ];
  }

  getErrorMessage() {
    return this.email.hasError('email') || this.email.hasError('required') ? this.translate.instant('global.email-error') : '';
  }

  checkFields() {
    const firstname = this.firstnameCtrl.value;
    const lastname = this.lastnameCtrl.value;
    const phone = this.phoneCtrl.value;
    const gender = this.genderCtrl.value;
    const email = this.email.value;
    const pwd = this.pwdCtrl.value;
    const pwdR = this.pwdRepeatCtrl.value;

    if (this.email.hasError('email') ||
      !firstname ||
      !lastname ||
      !phone ||
      phone && phone.length < 9 ||
      phone && phone.length > 14 ||
      !gender ||
      !pwd ||
      pwd && pwd.length < 6 ||
      !pwdR ||
      pwdR && pwdR.length < 6) {
      return true;
    } else {
      this.form.username = phone;
      this.form.firstname = firstname;
      this.form.lastname = lastname;
      this.form.email = email;
      this.form.tel = this.phone;
      if (gender.toLowerCase() === 'masculin' || gender.toLowerCase() === 'male' || gender.toLowerCase() === 'global.male') {
        this.form.gender = Gender.Male;
      } else if (gender.toLowerCase() === 'feminin' || gender.toLowerCase() === 'female' || gender.toLowerCase() === 'féminin' ||
                 gender.toLowerCase() === 'global.female'
      ) {
        this.form.gender = Gender.Female;
      } else {
        this.form.gender = Gender.Others;
      }

      this.form.password = pwd;
      return false;
    }
  }

  register() {
    // check whether the username is valid or not
    if (this.form.username && (this.form.username.toString().match(/^ *$/) !== null ||
      this.form.username.toString().indexOf(' ') >= 0)) {
      this.usernameError = this.translate.instant('global.white_space');
      return true;
    } else if (!this.isValid) {
      this.phoneError = this.translate.instant('global.phone-input-error');
      return true;
    } else if (this.form.password !== this.pwdRepeatCtrl.value) {
      this.pwdError2 = this.translate.instant('password.pwd_not_match');
      return true;
    } else {
      this.form.tel = this.phoneCtrl.value;
      this.authService.register(this.form).subscribe(
        data => {
          this.isSignUpFailed = false;
          // if registration successful redirect to the login page
          this.router.navigate(['login'], { queryParams: { registered: 'true' } });
        },
        err => {
          if (err.error.message) {
            this.errorMessage = err.error.message;
          } else {
            this.errorMessage = this.translate.instant('global.unex_error');
          }
          this.isSignUpFailed = true;
        }
      );
    }
  }

  clearErrorMessage() {
    this.pwdError1 = '';
    this.pwdError2 = '';
  }

  hasError(isValid) {
    this.isValid = isValid;
  }

  checkValidPhoneNumber(event) {
    const reg = this.utilityService.regPortableNumber;
    this.hasError(reg.test(event.target.value));
  }

}
