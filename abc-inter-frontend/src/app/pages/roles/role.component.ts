import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Role } from '../../models/role/role';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { RolesService } from '../../services/roles/roles.service';
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class RoleComponent implements OnInit {

  currentUrl: string;
  roleList: Role[] = [];
  role: Role;
  totalItems: number;
  page: number = 1;
  errorMessage: string;
  hide = true;
  roleName: string;
  isAsTable: boolean = true;
  key: string = 'username';
  reverse: boolean = false;
  loaded: boolean = true;
  isServerDown: boolean = false;
  serverDownMsg = '';

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private toastService: ToastrService,
    private roleService: RolesService,
    private translate: TranslateService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {
    this.currentUrl = this.translate.instant('roles.' + this.router.url.substring(1));
    // roles list initialisation
    this.initData();
    // the click on the search btn
    $('#search-role').on('click', () => {
      this.hide = this.hide === true ? false : true;
    });
  }

  initData() {
    this.roleService.findAllRoles().subscribe({
      next: (result) => {
        this.totalItems = result.length;
        // save all roles
        this.roleList = this.extractRole(result);
      },
      error: (err) => {
        if (!!err.error.message) {
          console.error(err);
        } else {
          this.isServerDown = true;
        }
      },
      complete: () => this.loaded = false
    });
  }

  search() {
    if (this.roleName == "") {
      this.ngOnInit();
    } else {
      this.roleList = this.roleList.filter(res => {
        return res.name.toLocaleLowerCase().match(this.roleName.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getRole(id: string) {
    // check if the id is not null
    if (null != id) {
      this.role = new Role(null, null, null);
      this.role = this.roleList.find(x => x.roleUuid === id);
    } else {
      // new role initialisation
      this.role = new Role(null, null, null);
    }
  }

  addNewRole() {
    // transform name to uppercase before send.
    this.role.name = this.role.name.toUpperCase();
    this.roleService.addRole(this.role).subscribe({
      next: (result) => {
        this.roleList.push(result);
        this.totalItems = this.roleList.length;
      },
      error: (err) => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastService.error(this.translate.instant('roles.error'));
        } else {
          this.isServerDown = true;
        }
      },
      complete: () => {
        this.roleList = this.extractRole(this.roleList);
        // if new user successful added, close the modal and reload the location
        this.modalService.dismissAll();
        // than notify
        this.toastService.success(this.translate.instant('roles.role_add'));
      }
    });
  }

  updateRole() {
    this.roleService.updateRole(this.role).subscribe({
      next: (result) => {
        const idx = this.roleList.findIndex(x => x.roleUuid === result.roleUuid);
        this.roleList[idx].name = result.name.toUpperCase();
        this.roleList[idx].description = result.description;
      },
      error: (err) => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastService.error(this.translate.instant('global.update-error'));
        } else {
          this.isServerDown = true;
        }
      },
      complete: () => {
        this.roleList = this.extractRole(this.roleList);
        // close the modal window
        this.modalService.dismissAll();
        // notify the user
        this.toastService.success(this.role.name + this.translate.instant('global.update-success'));
      }
    });
  }

  deleteRole() {
    this.roleService.deleteRoleById(this.role.roleUuid).subscribe({
      next: () => {
        this.roleList = this.roleList.filter(x => x.roleUuid !== this.role.roleUuid);
      },
      error: (err) => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastService.error(this.translate.instant('global.delete-error'));
        } else {
          this.isServerDown = true;
        }
      },
      complete: () => {
        this.modalService.dismissAll();
        this.toastService.success(this.translate.instant('global.delete-success'));
      }
    });
  }

  openModal(content) {
    this.errorMessage = '';
    this.modalService.open(content, { centered: true, scrollable: true });
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.roleName = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.initData();
  }

  private extractRole(roleList: Role[]): Array<Role> {
    return roleList.map<Role>(x => {
      x.name = x.name.replace('ROLE_', '');
      x.description = x.description.substring(0, 50);
      return x;
    });
  }
}
