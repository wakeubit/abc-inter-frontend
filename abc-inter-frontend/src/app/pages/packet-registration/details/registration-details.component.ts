import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from "@angular/animations";
import { ReplaySubject, Subject } from "rxjs";
import {
  debounceTime,
  delay,
  filter,
  takeUntil,
  tap,
  map,
} from "rxjs/operators";

import { Packet } from "src/app/models/packet/packet";
import { Registration } from "src/app/models/registration/registration";
import { Operator } from "src/app/models/operator/operator";
import { Address } from "src/app/models/address/address";

import { RegistrationService } from "src/app/services/registration/registration.service";
import { UtilityService } from "src/app/services/utility/utility.service";
import { OperatorsService } from "src/app/services/operators/operators.service";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";
import { PacketStore } from "src/app/models/packet-store/packet-store";
import { PacketStoreService } from "src/app/services/packet-store/packet-store.service";

import { Action } from "src/app/variables/action/action";
import { STATUSTYPE } from "src/app/models/statusType/statusType";
import { DataSubject } from "src/app/variables/dataSubject";
import { RoleType } from "src/app/models/role/RoleType";
import { OperatorType } from "src/app/models/operator/operatorType";

@Component({
  selector: "app-registration-details",
  templateUrl: "./registration-details.component.html",
  styleUrls: ["./registration-details.component.css"],
  animations: [
    trigger("fadeInOut", [
      state(
        "void",
        style({
          opacity: 0,
        })
      ),
      transition("void <=> *", animate(1000)),
    ]),
  ],
})
export class RegistrationDetailsComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  loaded: boolean = true;

  isServerDown: boolean = false;

  registrationID: string;

  currentUrl: string;

  route: Array<string>;

  registration: Registration;

  selectedPacket: Packet;

  completedPacketList: Array<Packet>;

  allCompleted: boolean = false;

  bulkActions = [];

  actionControl = new FormControl("", Validators.required);

  currentPacketLocation: PacketStore;

  isAdmin: boolean = false;

  isCommercial: boolean = false;

  isDeliverer: boolean = false;

  currentSelectedPacket: Packet;

  isOnlyOnePacket: boolean = false;

  addressFormGroup: FormGroup;

  postmanFilteringCtrl: FormControl = new FormControl();

  selectedPostman: FormControl = new FormControl();

  public searching = false;

  protected _onDestroy = new Subject<void>();

  operators: Operator[];

  filteredOperators: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(
    private activatedroute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private utilityService: UtilityService,
    private registrationService: RegistrationService,
    private operatorServie: OperatorsService,
    private translateService: TranslateService,
    private toastrService: ToastrService,
    private packetStoreService: PacketStoreService
  ) {
    this.completedPacketList = new Array<Packet>();
    this.operators = [];
  }

  ngOnInit(): void {
    this.getCurrentUserRole();
    this.dataInitialization();
  }

  ngAfterViewInit(): void {
    this.getPostmanList();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private getPostmanList() {
    this.operatorServie.getAllOperators().subscribe({
      next: (result) => {
        this.operators = result.filter(
          (x) => x.operatorType === OperatorType.Deliverer
        );
      },
      error: () => {
        this.toastrService.error(
          this.translateService.instant("operators.read_error")
        );
      },
      complete: () => this.filterOperatorByPersonalNB(),
    });
  }

  getCurrentUserRole() {
    this.isAdmin = this.utilityService.isAdmin;
    this.isCommercial = this.utilityService.isCommercial;
    this.isDeliverer = this.utilityService.isDeliverer;
  }

  private getCurrentUrl(): void {
    this.route = new Array<string>();
    for (let i = 0; i < 2; ++i) {
      this.route.push(this.router.url.substring(1).split("/")[i]);
    }
    this.currentUrl = this.router.url.substring(1).split("/")[1];
  }

  private dataInitialization(): void {
    this.getRegistration();
    this.getCurrentUrl();
    this.generateBulkActions();
  }

  private generateBulkActions() {
    let actions: Action = new Action(this.translateService);
    // get current logged in user role
    if (this.utilityService.isAdmin) {
      actions.getActions().forEach((row) => {
        if (row.roles.includes(RoleType.Admin) && row.status === "todo") {
          this.bulkActions.push(row);
        }
      });
    }

    if (this.utilityService.isCommercial) {
      actions.getActions().forEach((row) => {
        if (row.roles.includes(RoleType.Commercial) && row.status === "todo") {
          this.bulkActions.push(row);
        }
      });
    }

    if (this.utilityService.isDeliverer) {
      actions.getActions().forEach((row) => {
        if (row.roles.includes(RoleType.Deliverer) && row.status === "todo") {
          this.bulkActions.push(row);
        }
      });
    }
  }

  refresh() {
    this.isServerDown = false;
    // reset the list
    this.dataInitialization();
  }

  getPacketCurentLocation(storeUuid: string) {
    this.packetStoreService.findAllStore().subscribe((stores) => {
      this.currentPacketLocation = stores.find(
        (x) => x.packetStoreUuid === storeUuid
      );
    });
  }

  private getRegistration(): void {
    this.activatedroute.paramMap.subscribe((params) => {
      this.registrationID = params.get("id");
    });
    this.registrationService.findById(this.registrationID).subscribe(
      (registration) => {
        if (registration) {
          this.registration = registration;
          DataSubject.newPacketRegistration = this.registration;
          this.utilityService.changeDataSubject(DataSubject);
          if (this.registration.registeredPackets.length === 1) {
            this.isOnlyOnePacket = true;
          }
          this.loaded = false;
        }
      },
      (err) => {
        if (err?.error?.message) {
          console.error(err?.error?.message);
        } else {
          this.isServerDown = true;
          console.error(err?.error?.message);
        }
      }
    );
  }

  confirmRegistrationDeletion(view) {
    this.modalService.open(view, {
      centered: true,
      scrollable: true,
      size: "md",
    });
  }

  deleteRegistration() {
    this.registrationService
      .deleteById(this.registration.registrationUuid)
      .subscribe(
        (response) => {
          this.toastrService.success(
            this.translateService.instant("global.delete-success")
          );
          DataSubject.newPacketRegistration = null;
          this.utilityService.changeDataSubject(DataSubject);
          this.modalService.dismissAll();
          this.router.navigate(["/packets/registration/list"]);
        },
        (err) => {
          if (err) {
            this.toastrService.error(
              this.translateService.instant("global.delete-error")
            );
            console.error(err?.error?.message);
          }
        }
      );
  }

  displayRegistrationDetails(content): void {
    this.modalService.open(content, { centered: true, scrollable: true });
  }

  updateAddPacket(): void {
    this.router.navigate([
      "/packets/add",
      `${this.registration.registrationUuid}`,
    ]);
  }

  updatePacketSender() {
    this.router.navigate([
      "/customers/edit",
      `${this.registration.registrationUuid}`,
    ]);
  }

  updatePacketReceiver() {
    this.router.navigate([
      "/users/edit",
      `${this.registration.registrationUuid}`,
    ]);
  }

  updatePacketDestination() {
    this.router.navigate([
      "/address/edit",
      `${this.registration.registrationUuid}`,
      "packet",
    ]);
  }

  updateDestination() {
    const address = new Address(
      this.addressFormGroup.value.cityCtrl,
      this.addressFormGroup.value.countryCtrl,
      this.addressFormGroup.value.streetAndHNBCtrl,
      this.addressFormGroup.value.fixNumberCtrl,
      this.addressFormGroup.value.detailsCtrl,
      0,
      0
    );
    this.registration.receivingAddress = address;
    this.registrationService.updateRegistration(this.registration).subscribe(
      (result) => {
        if (result) {
          this.toastrService.success(
            this.translateService.instant("global.update-success")
          );
          this.modalService.dismissAll();
        }
      },
      (err) => {
        console.error(err?.error?.message);
        this.toastrService.error(
          this.translateService.instant("global.update-error")
        );
      }
    );
  }

  displayPacketDetails(packetID: string, content): void {
    this.getPacketCurentLocation(this.registration.store.packetStoreUuid);
    this.selectedPacket = this.registration.registeredPackets.find(
      (x) => x.packetUuid === packetID
    );
    this.modalService.open(content, { centered: true, scrollable: true });
  }

  public deletePacket() {
    this.completedPacketList = [];
    this.completedPacketList.push(this.currentSelectedPacket);
    this.removeSelectedPacketFromTheList();
  }

  private removeSelectedPacketFromTheList() {
    this.completedPacketList.forEach((selectedpacket) => {
      const idx = this.registration.registeredPackets.findIndex(
        (x) => x.packetUuid === selectedpacket.packetUuid
      );
      this.registration.registeredPackets.splice(idx, 1);
      this.registrationService
        .deletePacketFromRegistration(
          this.registration.registrationUuid,
          selectedpacket.packetUuid
        )
        .subscribe({
          next: (result) => {
            this.registration = result;
          },
          error: (err) => {
            this.toastrService.error(
              this.translateService.instant("global.update-error")
            );
            console.error(err);
          },
          complete: () => {
            this.modalService.dismissAll();
            this.toastrService.success(
              this.translateService.instant("global.update-success")
            );
          },
        });
    });
  }

  updateRemovePacket(packet: Packet, view) {
    this.currentSelectedPacket = packet;
    this.modalService.open(view, {
      centered: true,
      scrollable: true,
      size: "md",
    });
  }

  updateSavedPacket(packetID: string) {
    // fetch the packet to be updated
    this.router.navigate([
      `registrations/${this.registrationID}/packets/edit/${packetID}`,
    ]);
  }

  onPacketCheck(completed: boolean, packet: Packet): void {
    if (completed) {
      // Add the packet to the list of selected packets
      if (!this.completedPacketList.includes(packet)) {
        this.completedPacketList.push(packet);
      }
    } else {
      // Remove the packet from the list of selected packets
      if (this.completedPacketList.includes(packet)) {
        const idx = this.completedPacketList.findIndex(
          (x) => x.packetUuid === packet.packetUuid
        );
        this.completedPacketList.splice(idx, 1);
      }
    }

    this.allCompleted =
      this.registration.registeredPackets.length ===
      this.completedPacketList.length;
  }

  applyAction(modal: any) {
    const selectedAction = this.actionControl.value;

    if (this.completedPacketList.length === 0) {
      this.toastrService.error(
        this.translateService.instant("packets.no_selection")
      );
      return;
    }

    if (selectedAction === "" || selectedAction.length === 0) {
      this.toastrService.error(
        this.translateService.instant("global.no_action")
      );
      return;
    }

    /* do not removed
    if (selectedAction.action === "add_to_my_task") {
      if (
        this.completedPacketList.length !==
        this.registration.registeredPackets.length
      ) {
        this.toastrService.error(
          this.translateService.instant("global.select_all")
        );
      } else {
        this.toastrService.info("Coming soon!");
        this.assignAsJobToBeDone();
      }
      return;
    }
    */

    if (selectedAction.action === "change_location") {
      this.toastrService.error("Coming soon");
      return;
    }

    if (
      selectedAction.action === "delete" &&
      this.completedPacketList.length > 0
    ) {
      this.modalService.open(modal, {
        centered: true,
        scrollable: true,
        size: "md",
      });
      return;
    }

    if (selectedAction.action === "assign") {
      if (
        this.completedPacketList.length !==
        this.registration.registeredPackets.length
      ) {
        this.toastrService.error(
          this.translateService.instant("global.select_all")
        );
      } else {
        this.assignAsJobToBeDone();
      }
      return;
    }
  }

  public deleteSelectedPackets() {
    if (this.completedPacketList.length > 0) {
      if (
        this.completedPacketList.length ===
        this.registration.registeredPackets.length
      ) {
        this.deleteRegistration();
      } else {
        // fetch the selected packets and removing them from the registration
        this.removeSelectedPacketFromTheList();
      }
      this.completedPacketList = [];
    }
  }

  private assignAsJobToBeDone() {
    // update the status of the current registration
    this.registration.status = STATUSTYPE.READY_FOR_DELIVERY;

    if (this.selectedPostman.value) {
      this.registration.assignedTo = this.selectedPostman.value.user.userUuid;
    } else {
      if (this.utilityService.isDeliverer) {
        this.registration.assignedTo = this.utilityService.userId;
      }
    }

    this.registrationService.updateRegistration(this.registration).subscribe({
      next: (result) => {
        if (result) {
          DataSubject.notification.taskAdded = true;
          this.utilityService.changeDataSubject(DataSubject);
          this.router.navigate(["/orders"]);
        }
      },
      error: (err) => {
        console.error(err?.error?.message);
        this.toastrService.error(
          this.translateService.instant("global.assign_task_error")
        );
      },
      complete: () =>
        this.toastrService.success(
          this.translateService.instant("registration.success_assigned")
        ),
    });
  }

  toAddView(): void {
    this.router.navigate(["/packets-registration"]);
  }

  toRegistrationList(): void {
    this.router.navigate(["/orders"]);
  }

  navigateToHome() {
    this.router.navigate(["/dashboard"]);
  }

  reverseTime(time: string): string {
    return time.split("-").reverse().join("-");
  }

  filterOperatorByPersonalNB() {
    this.postmanFilteringCtrl.valueChanges
      .pipe(
        filter((search) => !!search),
        tap(() => (this.searching = true)),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map((search) => {
          if (!this.operators) {
            return [];
          }
          return this.operators.filter(
            (x) => x.personalNumber.toLowerCase().indexOf(search) > -1
          );
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(
        (x) => {
          this.searching = false;
          this.filteredOperators.next(x);
        },
        (error) => {
          this.searching = false;
          console.error(error);
        }
      );
  }
}
