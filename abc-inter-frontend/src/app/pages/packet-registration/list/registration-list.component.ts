import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import * as $ from 'jquery';
import { debounceTime, delay, filter, takeUntil, tap, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';

import { RegistrationService } from 'src/app/services/registration/registration.service';
import { TranslateService } from '@ngx-translate/core';
import { PacketStoreService } from '../../../services/packet-store/packet-store.service';

import { PacketStore } from '../../../models/packet-store/packet-store';
import { STATUSTYPE } from 'src/app/models/statusType/statusType';
import { Registration } from 'src/app/models/registration/registration';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-registration-list',
  templateUrl: './registration-list.component.html',
  styleUrls: ['./registration-list.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(2000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class RegistrationListComponent implements OnInit, OnDestroy {

  data: Array<Registration>;
  currentUrl: string;
  registrationList: Array<Registration>;
  totalItems: number;
  page: number = 1;
  totalPages: number;
  sizeTable = 10;
  sizeCard = 8;
  size: number;
  isAsTable = false;
  errorMessage: string;
  hide = true;
  isSuccessful = false;
  host = `${environment.apiUrl}/user/avatar/`;
  loaded: boolean = true;
  isServerDown: boolean = false;
  key: string = 'globaltrackingnb';
  reverse: boolean = false;
  serverDownMsg = '';
  globaltrackingnb: string = '';
  storeFilteringCtrl: FormControl = new FormControl();
  public searching = false;
  protected _onDestroy = new Subject<void>();
  stores: PacketStore[] = [];
  filteredStores: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  deliveryStatus = {
    [STATUSTYPE.IN_STORE]: {'status': 'packets.in_store', 'color': 'text-info'},
    [STATUSTYPE.READY_FOR_DELIVERY]: {'status': 'packets.ready_for_delivery', 'color': 'text-warning'},
    [STATUSTYPE.DELIVERED]: {'status': 'packets.delivered', 'color': 'text-success'},
  };

  constructor(
    private router: Router,
    private registrationService: RegistrationService,
    private translate: TranslateService,
    private packetStoreService: PacketStoreService
  ) { }

  ngOnInit(): void {
    $('#search-registration').click(() => {
      this.hide = this.hide === true ? false : true;
    });

    this.currentUrl = this.translate.instant(`global.${this.router.url.substring(1).split('/')[0].toLowerCase()}`);
    this.initData();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  initData() {
    this.serverDownMsg = this.translate.instant('global.server-down');
    this.data = new Array<Registration>();
    this.registrationList = new Array<Registration>();
    this.setItemSizePerPage();
    this.getAllRegistrations(0, this.size);

    this.getAllStore();

    this.storeFiltering();
  }

  getAllRegistrations(page: number, size: number) {
    this.registrationService.findAllByStatus(STATUSTYPE.IN_STORE, page, size).subscribe(result => {
      if (result) {
        this.data = result.items;
        this.totalItems = result.totalItems;
        this.totalPages = result.totalPages;
        this.page = result.currentPage + 1;
        this.registrationList = result.items;
        this.loaded = false;
      }
    }, err => {
      console.error(err?.error?.message);
      this.isServerDown = true;
    });
  }

  // Fetch and save the list of stores
  getAllStore() {
    this.packetStoreService.findAllStore().subscribe(response => {
      if (null != response) {
        this.stores = response;
        this.filteredStores.next(this.stores.slice());
      }
    }, err => {
      if (!!err.error.message) {
        console.error(err);
      } else {
        this.isServerDown = true;
      }
    });
  }

  search() {
    if (this.globaltrackingnb == "") {
      // Initialize the list
      this.initData();
    } else {
      this.registrationList = this.data.filter(res => {
        return res.globalTrackingNumber.toLocaleLowerCase().match(this.globaltrackingnb.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.globaltrackingnb = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.getAllRegistrations(0, this.size);
  }

  addRegistration() {
    this.router.navigate(['/packets-registration']);
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  reverseTime(time: string): string {
    return time.split("-").reverse().join("-");
  }

  storeFiltering() {
    this.storeFilteringCtrl.valueChanges
      .pipe(
        filter(search => !!search),
        tap(() => this.searching = true),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map(search => {
          if (!this.stores) {
            return [];
          }
          return this.stores.filter(store => store.name.toLowerCase().indexOf(search) > -1);
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(filteredStores => {
        this.searching = false;
        this.filteredStores.next(filteredStores);
      }, error => {
        this.searching = false;
        console.error(error);
      });
  }

  onStoreChange(store) {
    const selectedStore = store.value;
    this.registrationList = this.data.filter(registration => {
      return registration.store.name.toLowerCase() === selectedStore.toLowerCase();
    });
  }

  getcurrentSelectedRegistration(registration) {
    this.router.navigate(['/orders/details', registration.registrationUuid]);
  }

  setItemSizePerPage() {
    this.size = this.isAsTable ? this.sizeTable : this.sizeCard;
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }
}
