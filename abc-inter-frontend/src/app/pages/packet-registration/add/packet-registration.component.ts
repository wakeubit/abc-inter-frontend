import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  debounceTime,
  delay,
  filter,
  takeUntil,
  tap,
  map,
} from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatStepper } from '@angular/material/stepper';
import * as $ from 'jquery';
import { ReplaySubject, Subject } from 'rxjs';

import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { PacketStoreService } from '../../../services/packet-store/packet-store.service';
import { PacketCategoryService } from '../../../services/packet-category/packet-category.service';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { ShippingCostService } from 'src/app/services/shipping-cost/shipping-cost.service';
import { UtilityService } from '../../../services/utility/utility.service';
import { OperatorsService } from 'src/app/services/operators/operators.service';
import { UsersService } from 'src/app/services/users/users.service';

import { CustomerType } from '../../../models/customer/customerType';
import { Customer } from '../../../models/customer/customer';
import { User } from '../../../models/user/user';
import { Role } from '../../../models/role/role';
import { Address } from '../../../models/address/address';
import { RoleType } from '../../../models/role/RoleType';
import { ShippingType } from '../../../models/shippingType/ShippingType';
import { Packet } from '../../../models/packet/packet';
import { PacketStore } from '../../../models/packet-store/packet-store';
import { PacketCategory } from '../../../models/packet-category/packetCategory';
import { Registration } from 'src/app/models/registration/registration';
import { ShippingCost } from 'src/app/models/shippingCost/shippingCost';
import { Operator } from 'src/app/models/operator/operator';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { STATUSTYPE } from 'src/app/models/statusType/statusType';
import { Gender } from 'src/app/models/gender/gender';
import { weightValidator } from '../../../components/packets/packet-form/weight-validator';

@Component({
  selector: 'app-packet-registration',
  templateUrl: './packet-registration.component.html',
  styleUrls: ['./packet-registration.component.css'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
  animations: [
    trigger('searchClicked', [
      state(
        'true',
        style({
          color: 'white',
        })
      ),
      state(
        'false',
        style({
          color: 'white',
        })
      ),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        ),
      ]),
    ]),
    trigger('fadeInOut', [
      state(
        'void',
        style({
          opacity: 0,
        })
      ),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in'),
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' })),
      ]),
    ]),
  ],
})
export class PacketRegistrationComponent
  implements OnInit, AfterViewInit, OnDestroy {
  route: Array<string>;

  DEFAULTPWD = 'ABCINTER!1234';

  // has to be automaticaly computed
  long = 0;

  lat = 0;

  currentUrl: string;

  hide = true;

  configurationFormGroup: FormGroup;

  senderFormGroup: FormGroup;

  receiverFormGroup: FormGroup;

  packetFormGroup: FormGroup;

  confirmationFormGroup: FormGroup;

  customerTypes = [];

  genders = [];

  stores: PacketStore[] = [];

  public storeFilteringCtrl: FormControl = new FormControl();

  public filteredStores: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  shippingTypes = [];

  telOptions = { initialCountry: 'cm', onlyCountries: ['cm'] };

  isSenderPhoneValid = false;

  isCustomerPhoneValid = false;

  isAutoCompleted = false;

  senderList: Customer[] = [];

  sender: Customer;

  receiverList = [];

  receiver: User;

  packetCategories: PacketCategory[] = [];

  public categoryFilteringCtrl: FormControl = new FormControl();

  /** indicate search operation is in progress */
  public searching = false;

  public filteredCategories: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  protected _onDestroy = new Subject<void>();

  shippingCosts: ShippingCost[] = [];

  checked = false;

  isSuccessfulSave = false;

  isSaveInprogress = false;

  currentOperator: Operator = null;

  errors = {
    fixPhoneNb: '',
    phoneNb: '',
    errorMessage: '',
  };

  registeredPackets: Array<Packet>;

  loaded = true;

  isServerDown = false;

  packetCategory: PacketCategory;

  computedShippingcost: ShippingCost;

  isFixPhoneValid: boolean;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private utilityService: UtilityService,
    private packetStoreService: PacketStoreService,
    private packetCategoryService: PacketCategoryService,
    private customerService: CustomersService,
    private userService: UsersService,
    private operatorService: OperatorsService,
    private shippingCostService: ShippingCostService,
    private registrationService: RegistrationService
  ) {}

  ngOnInit(): void {
    this.getCurrentUrl();
    // the click on the search btn
    $('#search-packet').click(() => {
      this.hide = this.hide !== true;
    });
    this.initForm();
    this.initFields();
    this.initData();
    //
    this.onSenderPhoneChange();
    this.onReceiverPhoneChange();
  }

  ngAfterViewInit(): void {
    // On shippingtypectrl or categeroyctrl value changed, we check whether a shippingcost could generated or not
    this.packetFormGroup
      .get('shippingTypeCtrl')
      .valueChanges.subscribe((selectedShippingType) => {
        // clear the weight input field
        this.packetFormGroup.get('weightCtrl').setValue('');
        const selectedcategory = this.packetFormGroup.get('categoryCtrl').value;
        this.getAutogeneratedShippingcost(
          selectedShippingType,
          selectedcategory
        ).then((value) => (this.computedShippingcost = value));
      });

    this.packetFormGroup
      .get('categoryCtrl')
      .valueChanges.subscribe((selectedcategory) => {
        // clear the weight input field
        this.packetFormGroup.get('weightCtrl').setValue('');
        const selectedShippingType =
          this.packetFormGroup.get('shippingTypeCtrl').value;
        this.getAutogeneratedShippingcost(
          selectedShippingType,
          selectedcategory
        ).then((value) => (this.computedShippingcost = value));
      });

    this.packetFormGroup.get('weightCtrl').valueChanges.subscribe((value) => {
      if (this.computedShippingcost) {
        this.packetCategory = this.computedShippingcost.category;
        this.packetFormGroup
          .get('weightCtrl')
          .setValidators(weightValidator(this.packetCategory));
      } else {
        if (value) {
          this.toastr.info(
            this.translate.instant('registration.cost_error_msg')
          );
        }
      }
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private getCurrentUrl(): void {
    this.route = new Array<string>();
    for (let i = 0; i < 2; ++i) {
      this.route.push(this.router.url.substring(1).split('/')[i]);
    }
    this.currentUrl = this.translate.instant(`registration.title`);
  }

  initForm() {
    // stepper
    this.configurationFormGroup = this.formBuilder.group({
      totalPacketsToBeRegistered: [1, Validators.required],
      storeInfosCtrl: ['', Validators.required],
      destCityCtrl: ['Douala', Validators.required],
      destCountryCtrl: ['Cameroun', Validators.required],
      destStreetAndHNBCtrl: ['', Validators.required],
      destFixNumberCtrl: ['', Validators.required],
      destDetailsCtrl: '',
    });

    this.senderFormGroup = this.formBuilder.group({
      senderCustomerTypeCtrl: ['', Validators.required],
      senderFirstnameCtrl: ['', Validators.required],
      senderLastnameCtrl: ['', Validators.required],
      senderEmailCtrl: '',
      senderPhoneCtrl: '',
      senderGenderCtrl: ['', Validators.required],
      senderCityCtrl: ['Douala', Validators.required],
      senderCountryCtrl: ['Cameroun', Validators.required],
      senderStreetAndHNBCtrl: ['', Validators.required],
      senderFixNumberCtrl: '',
      senderDetailsCtrl: '',
    });

    this.receiverFormGroup = this.formBuilder.group({
      receiverFirstnameCtrl: ['', Validators.required],
      receiverLastnameCtrl: ['', Validators.required],
      receiverEmailCtrl: '',
      receiverPhoneCtrl: ['', Validators.required],
      receiverGenderCtrl: ['', Validators.required],
    });

    this.packetFormGroup = this.formBuilder.group({
      shippingTypeCtrl: ['', Validators.required],
      categoryCtrl: ['', Validators.required],
      weightCtrl: [
        '',
        Validators.compose([
          Validators.required,
          weightValidator(this.packetCategory),
        ]),
      ],
      positionInStoreCtrl: [''],
      positionDetailsCtrl: [''],
      descriptionCtrl: [''],
    });

    this.confirmationFormGroup = this.formBuilder.group({});
  }

  initFields() {
    this.customerTypes = [
      this.translate.instant(`global.${CustomerType.Anonym.toLowerCase()}`),
      this.translate.instant(`global.${CustomerType.Private.toLowerCase()}`),
      this.translate.instant(`global.${CustomerType.Business.toLowerCase()}`),
    ];

    this.genders = [
      this.translate.instant('global.female'),
      this.translate.instant('global.male'),
      this.translate.instant('global.others'),
    ];

    this.shippingTypes = [
      this.translate.instant(`global.${ShippingType.Normal.toLowerCase()}`),
      this.translate.instant(`global.${ShippingType.Secure.toLowerCase()}`),
      this.translate.instant(
        `global.${ShippingType.International.toLowerCase()}`
      ),
    ];
  }

  initData() {
    this.registeredPackets = new Array<Packet>();
    // fetch the stores
    this.getAllStore();
    this.storeValueChangeController();
    this.getPacketCategories();
    this.controlCategoryValueChange();
    this.getReceiverList();
    this.getSenderList();
    this.getCurrentOperator();
    this.loaded = false;
  }

  // Fetch and save the list of stores
  getAllStore() {
    this.packetStoreService.findAllStore().subscribe(
      (response) => {
        if (null != response) {
          this.stores = response;
          this.filteredStores.next(this.stores.slice());
        }
      },
      (err) => this.onError(err)
    );
  }

  storeValueChangeController() {
    this.storeFilteringCtrl.valueChanges
      .pipe(
        filter((search) => !!search),
        tap(() => (this.searching = true)),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map((search) => {
          if (!this.stores) {
            return [];
          }
          return this.stores.filter(
            (store) => store.name.toLowerCase().indexOf(search) > -1
          );
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(
        (filteredStores) => {
          this.searching = false;
          this.filteredStores.next(filteredStores);
        },
        (error) => {
          this.searching = false;
          console.error(error);
        }
      );
  }

  // Fetch and save the list of packet categories
  getPacketCategories() {
    this.packetCategoryService.getAllPacketCategories().subscribe(
      (response) => {
        if (null != response) {
          this.packetCategories = response;
          this.filteredCategories.next(this.packetCategories.slice());
        }
      },
      (err) => this.onError(err)
    );
  }

  controlCategoryValueChange() {
    this.categoryFilteringCtrl.valueChanges
      .pipe(
        filter((search) => !!search),
        tap(() => (this.searching = true)),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map((search) => {
          if (!this.packetCategories) {
            return [];
          }
          return this.packetCategories.filter(
            (category) => category.name.toLowerCase().indexOf(search) > -1
          );
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(
        (filteredCategories) => {
          this.searching = false;
          this.filteredCategories.next(filteredCategories);
        },
        (err) => {
          this.searching = false;
          console.error(err?.error?.message);
        }
      );
  }

  // Fetch and save the list of all senders (The are customers)
  getSenderList() {
    this.customerService.findAllCustomers().subscribe(
      (customerList) => {
        if (null != customerList) {
          this.senderList = customerList;
        }
      },
      (err) => this.onError(err)
    );
  }

  // Fetch and save the list of all receivers (The are users)
  getReceiverList() {
    this.userService.findAll().subscribe(
      (userList) => {
        if (null != userList) {
          this.receiverList = userList;
        }
      },
      (err) => this.onError(err)
    );
  }

  private getAllShippingCosts(): Promise<ShippingCost[]> {
    return new Promise((resolve, reject) => {
      this.shippingCostService.getAllShippingCosts().subscribe(
        (res) => {
          if (null != res) {
            resolve(res);
          }
        },
        (err) => this.onError(err)
      );
    });
  }

  getCurrentOperator() {
    this.operatorService.getAllOperators().subscribe(
      (res) => {
        if (null != res) {
          this.currentOperator = res.find(
            (x) => x.user.userUuid === this.utilityService.userId
          );
        }
      },
      (err) => this.onError(err)
    );
  }

  private translateInputValue(value: string): string {
    if (
      value.toLowerCase() === CustomerType.Anonym.toLowerCase() ||
      value.toLowerCase() === 'global.anonym'
    ) {
      return this.translate.instant('global.anonym');
    }

    if (
      value.toLowerCase() === CustomerType.Business.toLowerCase() ||
      value.toLowerCase() === 'global.business'
    ) {
      return this.translate.instant('global.business');
    }

    if (
      value.toLowerCase() === CustomerType.Private.toLowerCase() ||
      value.toLowerCase() === 'privée' ||
      value.toLowerCase() === 'global.private'
    ) {
      return this.translate.instant('global.private');
    }

    if (
      value.toLowerCase() === 'male' ||
      value.toLowerCase() === 'masculin' ||
      value.toLowerCase() === 'global.male'
    ) {
      return this.translate.instant('global.male');
    }

    if (
      value.toLowerCase() === 'female' ||
      value.toLowerCase() === 'feminin' ||
      value.toLowerCase() === 'global.female'
    ) {
      return this.translate.instant('global.female');
    }

    if (
      value.toLowerCase() === 'others' ||
      value.toLowerCase() === 'autres' ||
      value.toLowerCase() === 'global.others'
    ) {
      return this.translate.instant('global.others');
    }
  }

  convertInputValue(value: string): string {
    if (
      value.toLowerCase() === 'global.anonym' ||
      value.toLowerCase() === 'anonym'
    ) {
      return 'Anonym';
    }

    if (
      value.toLowerCase() === 'global.business' ||
      value.toLowerCase() === 'business'
    ) {
      return 'Business';
    }

    if (
      value.toLowerCase() === 'global.private' ||
      value.toLowerCase() === 'private' ||
      value.toLowerCase() === 'privée'
    ) {
      return this.translate.instant('global.private');
    }

    if (
      value.toLowerCase() === 'global.male' ||
      value.toLowerCase() === 'male' ||
      value.toLowerCase() === 'masculin'
    ) {
      return this.translate.instant('global.male');
    }

    if (
      value.toLowerCase() === 'global.female' ||
      value.toLowerCase() === 'female' ||
      value.toLowerCase() === 'feminin'
    ) {
      return this.translate.instant('global.female');
    }

    if (value.toLowerCase() === 'global.others') {
      return this.translate.instant('global.others');
    }

    return value;
  }

  // auto compilation methode
  autocompleteSenderInformation() {
    // we check whether the value of customer type, first- and lastname from the form is already exists
    // in the list of senders or not. If exists than we autocomplete the form, otherwise the
    // user have to fill it to the end.
    const senderFound: Customer = this.senderList.find(
      (x) => x.user.tel === this.senderFormGroup.value.senderPhoneCtrl
    );
    if (senderFound) {
      // complete the rest
      this.senderFormGroup.setValue({
        senderCustomerTypeCtrl: this.convertInputValue(
          this.translateInputValue(senderFound.customerType)
        ),
        senderFirstnameCtrl: senderFound.user.firstname,
        senderLastnameCtrl: senderFound.user.lastname,
        senderEmailCtrl: senderFound.user.email,
        senderPhoneCtrl: senderFound.user.tel,
        senderGenderCtrl: this.convertInputValue(
          this.translateInputValue(senderFound.user.gender)
        ),
        senderCityCtrl: senderFound.postalAddress.city,
        senderCountryCtrl: senderFound.postalAddress.country,
        senderStreetAndHNBCtrl: senderFound.postalAddress.streetAndHouseNumber,
        senderDetailsCtrl: senderFound.postalAddress.additionalInformation,
        senderFixNumberCtrl: senderFound.postalAddress.telAtAddress,
      });
      // set phone flag as valid
      this.isSenderPhoneValid = true;
      // the form has been autocompleted
      this.isAutoCompleted = true;
    } else {
        const senderFoundInReceiverList = this.receiverList.find(
          (x) => x.tel === this.senderFormGroup.value.senderPhoneCtrl
        );
        if (senderFoundInReceiverList) {
          this.senderFormGroup.setValue({
            senderCustomerTypeCtrl: this.senderFormGroup.value.senderCustomerTypeCtrl,
            senderFirstnameCtrl: senderFoundInReceiverList.firstname,
            senderLastnameCtrl: senderFoundInReceiverList.lastname,
            senderEmailCtrl: senderFoundInReceiverList.email,
            senderPhoneCtrl: senderFoundInReceiverList.tel,
            senderGenderCtrl: this.convertInputValue(
              this.translateInputValue(senderFoundInReceiverList.gender)
            ),
            senderCityCtrl: this.senderFormGroup.value.senderCityCtrl,
            senderCountryCtrl: this.senderFormGroup.value.senderCountryCtrl,
            senderStreetAndHNBCtrl: this.senderFormGroup.value.senderStreetAndHNBCtrl,
            senderDetailsCtrl: this.senderFormGroup.value.senderDetailsCtrl,
            senderFixNumberCtrl: senderFoundInReceiverList.tel,
          });
        }
    }
  }

  autocompleteReceiverInformation() {
    const receiverFound = this.receiverList.find(
      (x) => x.tel === this.receiverFormGroup.value.receiverPhoneCtrl
    );
    if (receiverFound) {
      // complete the rest
      this.receiverFormGroup.setValue({
        receiverFirstnameCtrl: receiverFound.firstname,
        receiverLastnameCtrl: receiverFound.lastname,
        receiverEmailCtrl: receiverFound.email,
        receiverPhoneCtrl: receiverFound.tel,
        receiverGenderCtrl: this.translateInputValue(receiverFound.gender),
      });
      // Set phone flag as valid
      this.isCustomerPhoneValid = true;
      // the form has been autocompleted
      this.isAutoCompleted = true;
    } else {
      const receiverFoundInSenderList = this.senderList.find(
        (x) => x.user.tel === this.receiverFormGroup.value.receiverPhoneCtrl
      );
      if (receiverFoundInSenderList) {
        // complete the rest
      this.receiverFormGroup.setValue({
        receiverFirstnameCtrl: receiverFoundInSenderList.user.firstname,
        receiverLastnameCtrl: receiverFoundInSenderList.user.lastname,
        receiverEmailCtrl: receiverFoundInSenderList.user.email,
        receiverPhoneCtrl: receiverFoundInSenderList.user.tel,
        receiverGenderCtrl: this.translateInputValue(receiverFoundInSenderList.user.gender),
      });
      }
    }
  }

  checkValidReceiverPhoneNumber(event) {
    const reg = this.utilityService.regPortableNumber;
    const isValid = reg.test(event.target.value);
    if (isValid) {
      this.autocompleteReceiverInformation();
    }
    this.checkReceiverPhone(isValid);
  }


  checkValidSenderPhoneNumber(event) {
    const reg = this.utilityService.regPortableNumber;
    const isValid = reg.test(event.target.value);
    if (isValid) {
      this.autocompleteSenderInformation();
    }
    this.checkSenderPhone(isValid);
  }

  checkValidStorePhoneNumber(event) {
    const reg = this.utilityService.regFixOrPortableNumber;
    this.checkStorePhone(reg.test(event.target.value));
  }

  checkValidFixPhoneNumber(event) {
    const reg = this.utilityService.regFixOrPortableNumber;
    this.checkFixPhone(reg.test(event.target.value));
  }

  private checkFixPhone(isValid: boolean) {
    this.isFixPhoneValid = isValid;
    if (this.isFixPhoneValid) {
      this.errors.fixPhoneNb = '';
    } else {
      this.errors.fixPhoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  // returns true if phone number is valid otherwise false.
  checkSenderPhone(isValid: boolean) {
    this.isSenderPhoneValid = isValid;
    if (this.isSenderPhoneValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  checkReceiverPhone(isValid: boolean) {
    this.isCustomerPhoneValid = isValid;
    if (this.isCustomerPhoneValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  checkStorePhone(isValid: boolean) {
    if (isValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  telInputObject(obj: any) {
    obj.setCountry('cm');
  }

  onSenderPhoneChange() {
    this.senderFormGroup
      .get('senderPhoneCtrl')
      .valueChanges.subscribe((value) => {
        this.errors.phoneNb = '';
        if (this.isAutoCompleted && value.length !== 9) {
          this.isAutoCompleted = false;
          return this.resetSenderForm();
        }
      });
  }

  onReceiverPhoneChange() {
    this.receiverFormGroup
      .get('receiverPhoneCtrl')
      .valueChanges.subscribe((value) => {
        this.errors.phoneNb = '';
        this.receiverFormGroup
          .get('destFixNumberCtrl')
          ?.setValue(this.receiverFormGroup.get('receiverPhoneCtrl').value);
        if (this.isAutoCompleted && value.length !== 9) {
          this.isAutoCompleted = false;
          return this.resetReceiverForm();
        }
      });
  }

  resetSenderForm() {
    this.senderFormGroup.setValue({
      senderPhoneCtrl: this.senderFormGroup.get('senderPhoneCtrl').value,
      senderCustomerTypeCtrl: '',
      senderEmailCtrl: '',
      // senderBonusCtrl: "",
      senderFirstnameCtrl: '',
      senderLastnameCtrl: '',
      senderGenderCtrl: '',
      senderCityCtrl: '',
      senderCountryCtrl: '',
      senderStreetAndHNBCtrl: '',
      senderFixNumberCtrl: '',
      senderDetailsCtrl: '',
    });
  }

  resetReceiverForm() {
    this.receiverFormGroup.setValue({
      receiverFirstnameCtrl: '',
      receiverLastnameCtrl: '',
      receiverEmailCtrl: '',
      receiverPhoneCtrl: this.receiverFormGroup.get('receiverPhoneCtrl').value,
      receiverGenderCtrl: '',
    });
  }

  resetPacketForm() {
    // register the next one
    this.errors.errorMessage = '';
    this.packetFormGroup.setValue({
      shippingTypeCtrl: '',
      categoryCtrl: '',
      weightCtrl: '',
      positionInStoreCtrl: '',
      positionDetailsCtrl: '',
      descriptionCtrl: '',
    });
  }

  convertSelectedCustomerType(value: string): CustomerType {
    if (
      value.toLowerCase() === 'anonym' ||
      value.toLowerCase() === 'global.anonym'
    ) {
      return CustomerType.Anonym;
    }

    if (
      value.toLowerCase() === 'private' ||
      value.toLowerCase() === 'global.private' ||
      value.toLowerCase() === 'privée' ||
      value.toLowerCase() === 'privee'
    ) {
      return CustomerType.Private;
    }

    if (
      value.toLowerCase() === 'business' ||
      value.toLowerCase() === 'global.business'
    ) {
      return CustomerType.Business;
    }
  }

  // save sender information
  readSenderInformation(stepper: MatStepper) {
    // First check whether the phone number is valid or not
    if (this.isSenderPhoneValid) {
      // check in the customer list whether the customer already exists or not before save his information
      const userPhone = this.senderFormGroup.value.senderPhoneCtrl;
      this.sender = this.senderList.find((x) => x.user.tel === userPhone);
      // if the sender was not already registered, we created a new user
      if (!this.sender) {
        this.sender = new Customer();
        // set the userID to null
        this.sender.customerUuid = null;
        // save information
        this.sender.customerType = this.convertSelectedCustomerType(
          this.senderFormGroup.value.senderCustomerTypeCtrl
        );
        const senderFoundInReceiverList = this.receiverList.find(
          (x) => x.tel === userPhone
        );
        if (senderFoundInReceiverList) {
          this.sender.user = senderFoundInReceiverList;
        } else {
          // set sender default role
          const senderRole = [new Role(null, RoleType.Customer, null)];
          // build sender user object
          this.sender.user = new User(
            null,
            this.senderFormGroup.value.senderPhoneCtrl,
            this.senderFormGroup.value.senderLastnameCtrl,
            this.senderFormGroup.value.senderFirstnameCtrl,
            this.DEFAULTPWD,
            true,
            this.senderFormGroup.value.senderEmailCtrl,
            this.senderFormGroup.value.senderPhoneCtrl,
            this.convertSelectedValue(
              this.senderFormGroup.value.senderGenderCtrl
            ),
            null,
            senderRole
          );
        }

        this.sender.postalAddress = new Address(
          this.senderFormGroup.value.senderCityCtrl,
          this.senderFormGroup.value.senderCountryCtrl,
          this.senderFormGroup.value.senderStreetAndHNBCtrl,
          this.senderFormGroup.value.senderDetailsCtrl,
          this.senderFormGroup.value.senderFixNumberCtrl,
          this.long,
          this.lat
        );
      }
      // next step
      stepper.next();
    } else {
      return;
    }
  }

  readCustomerInformation(stepper: MatStepper) {
    if (this.isCustomerPhoneValid) {
      // check whether the user already exists or not
      const phone = this.receiverFormGroup.value.receiverPhoneCtrl;
      this.receiver = this.receiverList.find((x) => x.tel === phone);
      // if the customer was not already registered, we created a new user
      if (!this.receiver) {

        const receiverFoundInSenderList = this.senderList.find(
          (x) => x.user.tel === phone
        );
        if (receiverFoundInSenderList) {
          this.receiver = receiverFoundInSenderList.user;
        } else {
          // receiver default role
          const receiverRole = [new Role(null, RoleType.Customer, null)];
          this.receiver = new User(
            null,
            this.receiverFormGroup.value.receiverPhoneCtrl,
            this.receiverFormGroup.value.receiverLastnameCtrl,
            this.receiverFormGroup.value.receiverFirstnameCtrl,
            this.DEFAULTPWD,
            true,
            this.receiverFormGroup.value.receiverEmailCtrl,
            this.receiverFormGroup.value.receiverPhoneCtrl,
            this.convertSelectedValue(
              this.receiverFormGroup.value.receiverGenderCtrl
            ),
            null,
            receiverRole
          );
        }

      }
      // next step
      stepper.next();
    } else {
      return;
    }
  }

  convertSelectedValue(value: string): string {
    if (
      value.toLowerCase() === 'global.normal' ||
      value.toLowerCase() === 'normal' ||
      value.toLowerCase() === 'normale'
    ) {
      return ShippingType.Normal;
    }

    if (
      value.toLowerCase() === 'global.secure' ||
      value.toLowerCase() === 'secure' ||
      value.toLowerCase() === 'sécuriser'
    ) {
      return ShippingType.Secure;
    }

    if (
      value.toLowerCase() === 'global.international' ||
      value.toLowerCase() === 'international' ||
      value.toLowerCase() === 'internationale'
    ) {
      return ShippingType.International;
    }

    if (
      value.toLowerCase() === 'global.male' ||
      value.toLowerCase() === 'male' ||
      value.toLowerCase() === 'masculin'
    ) {
      return Gender.Male.toString();
    }

    if (
      value.toLowerCase() === 'global.female' ||
      value.toLowerCase() === 'female' ||
      value.toLowerCase() === 'feminin' ||
      value.toLowerCase() === 'féminin'
    ) {
      return Gender.Female.toString();
    }

    if (
      value.toLowerCase() === 'global.others' ||
      value.toLowerCase() === 'others' ||
      value.toLowerCase() === 'autres'
    ) {
      return Gender.Others.toString();
    }
  }

  saveCurrentPacketInformation(stepper: MatStepper) {
    if (!this.packetFormGroup.invalid) {
      if (!this.computedShippingcost) {
        this.toastr.error(
          this.translate.instant('registration.cost_error_msg')
        );
      } else {
        this.registeredPackets.push(
          this.buildPacket(this.computedShippingcost)
        );
        // check whether to show the next step or not
        const totalPacketsToBeRegistered = parseInt(
          this.configurationFormGroup.get('totalPacketsToBeRegistered').value,
          10
        );

        const counter =
          totalPacketsToBeRegistered - this.registeredPackets.length;
        if (counter > 0) {
          this.resetPacketForm();
        } else {
          stepper.next();
        }
      }
    }
  }

  private buildPacket(shippingCost: ShippingCost): Packet {
    return {
      packetUuid: null,
      currentStoreUuid: this.retrieveSelectedStore().packetStoreUuid,
      registrationUuid: null,
      delivranceUuid: null,
      trackingNumber: null,
      positionInStore: this.packetFormGroup.value.positionInStoreCtrl,
      positionDetails: this.packetFormGroup.value.positionDetailsCtrl,
      shippingCost: shippingCost,
      weight: this.packetFormGroup.value.weightCtrl,
      description: this.packetFormGroup.value.descriptionCtrl,
    };
  }

  clearPacketList() {
    this.registeredPackets = [];
    this.resetPacketForm();
  }

  toPreviousView() {
    // disable the check box
    this.checked = false;
    // remove the last added packet from the list
    this.registeredPackets.pop();
  }

  retrieveSelectedStore(): PacketStore {
    return this.stores.find(
      (x) => x.name === this.configurationFormGroup.value.storeInfosCtrl
    );
  }

  computeTotalPrice(packages: Array<Packet>): number {
    let price = 0;
    packages.forEach((packet) => {
      price += packet.shippingCost.price;
    });
    return price;
  }

  saveRegistration() {
    const address = new Address(
      this.configurationFormGroup.value.destCityCtrl,
      this.configurationFormGroup.value.destCountryCtrl,
      this.configurationFormGroup.value.destStreetAndHNBCtrl,
      this.configurationFormGroup.value.destDetailsCtrl,
      this.configurationFormGroup.value.destFixNumberCtrl,
      this.long,
      this.lat
    );

    const newRegistration: Registration = {
      registrationUuid: null,
      globalTrackingNumber: null,
      delivered: false,
      status: STATUSTYPE.IN_STORE,
      registeredBy: this.currentOperator,
      sender: this.sender,
      receiver: this.receiver,
      receivingAddress: address,
      registrationDetails: `This registration contains ${
        this.configurationFormGroup.get('totalPacketsToBeRegistered').value
      } package(s).`,
      registeredPackets: this.registeredPackets,
      totalPrice: this.computeTotalPrice(this.registeredPackets),
      store: this.retrieveSelectedStore(),
      currency: this.registeredPackets[0].shippingCost.currency,
      assignedTo: null,
      createdBy: '',
      modifiedBy: '',
      createdAt: '',
      modifiedAt: '',
    };

    this.addRegistration(newRegistration);
  }

  addRegistration(registration: Registration) {
    this.isSuccessfulSave = false;
    this.isSaveInprogress = true;
    this.registrationService.addRegistration(registration).subscribe(
      (response) => {
        // if the registration has been successful saved, than we save the registration locally.
        if (null != response) {
          // share the information with other components
          this.utilityService.changeDataSubject({
            newPacketRegistration: response,
          });
          this.isSuccessfulSave = true;
          this.isSaveInprogress = false;
          this.toastr.success(
            this.translate.instant('registration.reg_success')
          );
          this.router.navigate([`orders/details/${response.registrationUuid}`]);
        }
      },
      (err) => {
        this.isSaveInprogress = false;
        if (!!err.error.message) {
          console.error(err);
          this.toastr.error(this.translate.instant('registration.reg_error'));
          this.errors.errorMessage = err.error.message;
        } else {
          this.toastr.error(this.translate.instant('registration.reg_error'));
        }
      }
    );
  }

  navigateToHome() {
    return this.router.navigate(['/dashboard']);
  }

  // Compute shippingcost
  private getAutogeneratedShippingcost(
    shippingtype: string,
    category: string
  ): Promise<ShippingCost> {
    return new Promise((resolve) => {
      shippingtype = this.convertSelectedValue(shippingtype);
      let generatedShippingcost = null;
      if (shippingtype?.length > 0 && category?.length > 0) {
        this.shippingCostService.getAllShippingCosts().subscribe({
          next: (result) => {
            this.shippingCosts = result;
          },
          complete: () => {
            generatedShippingcost = this.shippingCosts.find(
              (x) =>
                x.shippingType.toLowerCase() === shippingtype.toLowerCase() &&
                x.category.name.toLowerCase() === category.toLowerCase()
            );
            resolve(generatedShippingcost);
          },
        });
      }
    });
  }

  private onError(err: any) {
    if (!!err.error.message) {
      console.error(err);
    } else {
      this.isServerDown = true;
    }
  }


}
