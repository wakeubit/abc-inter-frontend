import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketRegistrationComponent } from './packet-registration.component';

describe('PacketRegistrationComponent', () => {
  let component: PacketRegistrationComponent;
  let fixture: ComponentFixture<PacketRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
