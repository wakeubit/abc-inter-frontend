import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { debounceTime, delay, filter, takeUntil, tap, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';

import { DeliveriesService } from '../../services/deliveries/deliveries.service';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { TranslateService } from '@ngx-translate/core';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { PacketStoreService } from 'src/app/services/packet-store/packet-store.service';

import { Delivery } from '../../models/delivery/delivery';
import { Operator } from '../../models/operator/operator';
import { Registration } from 'src/app/models/registration/registration';
import { PacketStore } from 'src/app/models/packet-store/packet-store';
import { environment } from 'src/environments/environment';

interface DeliveryInfo {
  registration: Registration,
  customer: string,
  destination: string,
  delivery: Delivery
}

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(2000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class DeliveryComponent implements OnInit, OnDestroy {

  data: Array<DeliveryInfo>;
  currentUrl: string;
  deliveriesList: Array<DeliveryInfo>;
  totalItems: number;
  page: number = 1;
  errorMessage: string;
  operators: Array<Operator>;
  operator: Operator;
  operatorUuid: string;
  isOperatorChange = false;
  delivery = [];
  hide = true;
  isSuccessful = false;
  host = `${environment.apiUrl}/user/avatar/`;
  loaded: boolean = true;
  isServerDown: boolean = false;
  isAsTable: boolean = false;
  key: string = 'globaltrackingnb';
  reverse: boolean = false;
  serverDownMsg = '';
  totalPackets: number = 0;
  globaltrackingnb: string = '';

  storeFilteringCtrl: FormControl = new FormControl();

  public searching = false;

  protected _onDestroy = new Subject<void>();

  stores: PacketStore[] = [];

  filteredStores: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(
    private router: Router,
    private deliveriesService: DeliveriesService,
    private utilityService: UtilityService,
    private translate: TranslateService,
    private registrationService: RegistrationService,
    private packetStoreService: PacketStoreService
  ) {

  }

  ngOnInit(): void {
    // the click on the search btn
    $('#search-delivery').on('click', () => {
      this.hide = !this.hide;
    });

    this.currentUrl = this.translate.instant(`global.${this.router.url.substring(1).split('/')[0].toLowerCase()}`);
    this.initData();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  initData() {
    this.serverDownMsg = this.translate.instant('global.server-down');
    this.getDeliveries();
    this.getAllStores();
  }

  getAllStores() {
    this.packetStoreService.findAllStore().subscribe({
      next: (result) => {
        if (result) {
          this.stores = result;
          this.filteredStores.next(this.stores.slice());
        }
      },
      error: (err) => {
        if (!!err.error.message) {
          console.error(err);
        } else {
          this.isServerDown = true;
        }
      }
    });
  }

  getDeliveries() {
    this.data = new Array<DeliveryInfo>();
    this.deliveriesList = new Array<DeliveryInfo>();
    this.deliveriesService.findAll().subscribe({
      next: (result) => {
        if (result) {
          // compute total packets on each delivery
          result.forEach(delivery => {
            this.getRegistration(delivery.deliveredPackets[0]?.registrationUuid).then(registration => {
              if (registration) {
                this.addDataIntoList(registration, delivery);
              }
            });
          });
          this.deliveriesList = this.data;
          this.totalItems = this.deliveriesList.length;
        }
      },
      error: (err) => {
        if (err?.error?.error) {
          console.error(err?.error?.error);
        }
        this.isServerDown = true;
      },
      complete: () => this.loaded = false
    });
  }

  private addDataIntoList(registration: Registration, delivery: Delivery): void {
    const data: DeliveryInfo = {
      registration: registration,
      customer: `${registration.sender.user.lastname} ${registration.sender.user.firstname}`,
      destination: `${registration.receivingAddress.streetAndHouseNumber} (${registration.receivingAddress.city})`,
      delivery: delivery
    }

    // filter deliveries by operator
    if (this.utilityService.isAdmin || this.utilityService.isCommercial) {
      this.data.push(data);
      this.totalPackets += delivery.deliveredPackets?.length;
    } else {
      if (delivery.deliverBy.user.userUuid == this.utilityService.userId) {
        this.data.push(data);
        this.totalPackets += delivery.deliveredPackets?.length;
      }
    }
  }

  getRegistration(registrationID: string): Promise<Registration> {
    return new Promise((resolve, _) => {
      this.registrationService.findById(registrationID).subscribe(registration => {
        if (registration) {
          resolve(registration);
        }
      });
    });
  }

  getUserDeliveries(data: DeliveryInfo[]): Promise<DeliveryInfo[]> {
    return new Promise((resolve, _) => {
      let result = data.filter(item => {
        return item;
      });
      resolve(result);
    });
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  search() {
    if (this.globaltrackingnb == '') {
      this.initData();
    } else {
      this.deliveriesList = this.data.filter(res => {
        return res.registration.globalTrackingNumber.toLocaleLowerCase().match(this.globaltrackingnb.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.globaltrackingnb = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.initData();
  }

  reverseTime(time: string): string {
    return time.split("-").reverse().join("-");
  }

  storeFiltering() {
    this.storeFilteringCtrl.valueChanges
      .pipe(
        filter(search => !!search),
        tap(() => this.searching = true),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map(search => {
          if (!this.stores) {
            return [];
          }
          return this.stores.filter(store => store.name.toLowerCase().indexOf(search) > -1);
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe({
        next: (filteredStores) => {
          this.searching = false;
          this.filteredStores.next(filteredStores);
        },
        error: (err) => {
          this.searching = false;
          console.error(err);
        }
      });
  }

  onStoreChange(store) {
    const selectedStore = store.value;
    this.deliveriesList = this.data.filter(delivery => {
      return delivery.registration.store.name.toLowerCase() === selectedStore.toLowerCase();
    });
  }
}
