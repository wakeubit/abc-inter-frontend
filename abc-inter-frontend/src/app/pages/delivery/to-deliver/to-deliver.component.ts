import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import * as $ from 'jquery';
import { debounceTime, delay, filter, takeUntil, tap, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';

import { RegistrationService } from 'src/app/services/registration/registration.service';
import { TranslateService } from '@ngx-translate/core';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { PacketStoreService } from '../../../services/packet-store/packet-store.service';
import { UsersService } from 'src/app/services/users/users.service';

import { Registration } from 'src/app/models/registration/registration';
import { STATUSTYPE } from 'src/app/models/statusType/statusType';
import { PacketStore } from '../../../models/packet-store/packet-store';
import { User } from 'src/app/models/user/user';
import { environment } from 'src/environments/environment';
import { OperatorsService } from 'src/app/services/operators/operators.service';
import { Operator } from 'src/app/models/operator/operator';
import { OperatorType } from 'src/app/models/operator/operatorType';

@Component({
  selector: 'app-to-deliver',
  templateUrl: './to-deliver.component.html',
  styleUrls: ['./to-deliver.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(2000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class ToDeliverComponent implements OnInit, OnDestroy, AfterViewInit {

  data: Array<Registration>;

  currentUrl: string;

  registrationList: Array<Registration>;

  totalItems: number;
  size = 8;
  page: number = 1;

  errorMessage: string;

  hide = true;

  isSuccessful = false;

  host = `${environment.apiUrl}/user/avatar/`;

  loaded: boolean = true;

  isServerDown: boolean = false;

  key: string = 'globaltrackingnb';

  reverse: boolean = false;

  serverDownMsg = '';

  globaltrackingnb: string = '';

  sLat = 0;

  sLong = 0;

  storeFilteringCtrl: FormControl = new FormControl();

  public searching = false;

  protected _onDestroy = new Subject<void>();

  stores: PacketStore[] = [];

  filteredStores: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  filteredResponsibles: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  responsibleFilteringCtrl: FormControl = new FormControl();

  operators: Operator[];

  responsible: string;

  constructor(
    private router: Router,
    private registrationService: RegistrationService,
    private translate: TranslateService,
    private utilityService: UtilityService,
    private packetStoreService: PacketStoreService,
    private operatorService: OperatorsService
  ) { 
    this.serverDownMsg = this.translate.instant('global.server-down');
    this.data = new Array<Registration>();
    this.registrationList = new Array<Registration>();
  }

  ngOnInit(): void {
    $('#search-registration').click(() => {
      this.hide = this.hide === true ? false : true;
    });

    this.currentUrl = this.router.url.substr(1).split('/')[1];
    this.initData();
    this.getLocation();
  }

  ngAfterViewInit(): void {
    this.getAllOperators();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  initData() {
    this.getAllDeliveries();
    this.getAllStores();
  }

  getAllDeliveries(page?: number, size?: number) {
    if (this.utilityService.isDeliverer) {
      this.registrationService.findAllByAssignedTo(this.utilityService.userId, page, size).subscribe(result => {
        if (result) {
          this.data = result.items;
          this.totalItems = result?.totalItems;
          this.registrationList = this.data;
          this.page = result.currentPage + 1;
          this.loaded = false;
        }
      });
    } else if (this.utilityService.isCommercial || this.utilityService.isAdmin) {
      this.registrationService.findAllByStatus(STATUSTYPE.READY_FOR_DELIVERY, page, size).subscribe(result => {
        if (result) {
          this.data = result.items;
          this.totalItems = result?.totalItems;
          this.registrationList = this.data;
          this.page = result.currentPage + 1;
          this.loaded = false;
        }
      });
    } else {
      // If the user role is not definied, than display a empty list
      this.loaded = false;
    }
  }

  getAllStores() {
    this.packetStoreService.findAllStore().subscribe(response => {
      if (null != response) {
        this.stores = response;
        this.filteredStores.next(this.stores.slice());
      }
    }, err => {
      if (!!err.error.message) {
        console.error(err);
      } else {
        this.isServerDown = true;
      }
    });
  }

  private getAllOperators() {
    this.operatorService.getAllOperators().subscribe({
      next: (result) => this.operators = result,
      complete: () => {
        this.operators = this.operators.filter(x => x.operatorType === OperatorType.Deliverer);
        this.filteredResponsibles.next(this.operators.slice());
      }
    });
  }


  search() {
    if (this.globaltrackingnb == "") {
      // Initialize the list
      this.initData();
    } else {
      this.registrationList = this.data.filter(res => {
        return res.globalTrackingNumber.toLocaleLowerCase().match(this.globaltrackingnb.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.globaltrackingnb = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.initData();
  }

  toOrderList() {
    this.router.navigate(['/orders']);
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        if (position) {
          this.sLat = position.coords.latitude;
          this.sLong = position.coords.longitude;
        }
      }, err => {
        console.error(err);
      });
    } else {
      console.error("The browser does not support geolocation.");
    }
  }

  openMaps(registration: Registration) {
    const dLat = registration.receivingAddress.latitude;
    const dLong = registration.receivingAddress.longitude;
    const route = `http://maps.google.com/maps?saddr=${this.sLat},${this.sLong}&daddr=${dLat},${dLong}`;
    window.open(route, '_blank');
  }

  reverseTime(time: string): string {
    return time.split("-").reverse().join("-");
  }

  storeFiltering() {
    this.storeFilteringCtrl.valueChanges
      .pipe(
        filter(search => !!search),
        tap(() => this.searching = true),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map(search => {
          if (!this.stores) {
            return [];
          }
          return this.stores.filter(store => store.name.toLowerCase().indexOf(search) > -1);
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(filteredStores => {
        this.searching = false;
        this.filteredStores.next(filteredStores);
      }, error => {
        this.searching = false;
        console.error(error);
      });
  }

  onStoreChange(store) {
    const selectedStore = store.value;
    this.registrationList = this.data.filter(registration => {
      return registration.store.name.toLowerCase() === selectedStore.toLowerCase();
    });
  }

  onResponsibleChanged(responsible: any) {
    const postmanUserId = responsible.value.user.userUuid;
    this.registrationList = this.data.filter(x => x.assignedTo === postmanUserId);
  }

  filterOperatorByPersonalNB() {
    this.responsibleFilteringCtrl.valueChanges
      .pipe(
        filter((search) => !!search),
        tap(() => (this.searching = true)),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map((search) => {
          if (!this.operators) {
            return [];
          }
          return this.operators.filter(
            (x) => x.personalNumber.toLowerCase().indexOf(search) > -1
          );
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(
        (x) => {
          this.searching = false;
          this.filteredResponsibles.next(x);
        },
        (error) => {
          this.searching = false;
          console.error(error);
        }
      );
  }

}
