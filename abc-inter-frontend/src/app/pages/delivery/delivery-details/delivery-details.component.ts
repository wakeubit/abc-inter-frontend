import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators } from '@angular/forms';

import { Packet } from 'src/app/models/packet/packet';
import { Registration } from 'src/app/models/registration/registration';
import { Action } from 'src/app/variables/action/action';

import { RegistrationService } from 'src/app/services/registration/registration.service';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { PacketStore } from 'src/app/models/packet-store/packet-store';
import { PacketStateGroup } from 'src/app/models/packet-state-group/packet-state-group';
import { STATUSTYPE } from 'src/app/models/statusType/statusType';
import { PacketStateService } from 'src/app/services/packet-state/packet-state.service';
import { Delivery } from 'src/app/models/delivery/delivery';
import { OperatorsService } from 'src/app/services/operators/operators.service';
import { Operator } from 'src/app/models/operator/operator';
import { DeliveriesService } from 'src/app/services/deliveries/deliveries.service';
import { Location } from '@angular/common';
import { DataSubject } from 'src/app/variables/dataSubject';
import { RoleType } from 'src/app/models/role/RoleType';

@Component({
  selector: 'app-delivery-details',
  templateUrl: './delivery-details.component.html',
  styleUrls: ['./delivery-details.component.css']
})
export class DeliveryDetailsComponent implements OnInit {

  loaded: boolean = true;

  isServerDown: boolean = false;

  registrationID: string;

  currentUrl: string;

  route: Array<string>;

  registration: Registration;

  delivery: Delivery;

  selectedPacket: Packet;

  completedPacketList: Array<Packet>;

  allCompleted: boolean = false;

  bulkActions = [];

  actionControl = new FormControl('', Validators.required);

  signatureCtl = new FormControl('', Validators.required);

  reasonCtrl = new FormControl('', Validators.required);

  currentPacketLocation: PacketStore;

  isAdmin: boolean = false;

  isCommercial: boolean = false;

  isDeliverer: boolean = false;

  currentSelectedPacket: Packet;

  isOnlyOnePacket: boolean = false;

  isAlredyDeliver: boolean = false;

  reasons = [
    { name: this.translateService.instant('deliveries.not_reachable'), action: 'not_present' },
    { name: this.translateService.instant('deliveries.mistake'), action: 'mistake' }
  ];

  verificationKey: string = '';

  constructor(
    private activatedroute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private utilityService: UtilityService,
    private registrationService: RegistrationService,
    private translateService: TranslateService,
    private toastrService: ToastrService,
    private packetStateService: PacketStateService,
    private operatorService: OperatorsService,
    private deliveryService: DeliveriesService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getCurrentUserRole();
    this.dataInitialization();
  }

  refresh() {
    this.isServerDown = false;
    // reset the list
    this.dataInitialization();
  }

  getCurrentUserRole() {
    this.isAdmin = this.utilityService.isAdmin;
    this.isCommercial = this.utilityService.isCommercial;
    this.isDeliverer = this.utilityService.isDeliverer;
  }

  private getCurrentUrl(): void {
    this.currentUrl = this.router.url.substr(1).split('/')[1];
  }

  private getRegistration(): void {
    this.activatedroute.paramMap.subscribe(params => {
      this.registrationID = params.get('id');
    });
    this.registrationService.findById(this.registrationID).subscribe(registration => {
      if (registration) {
        this.registration = registration;
        this.isAlredyDeliver = this.registration.delivered;
        // Helper for printing
        DataSubject.newPacketRegistration = this.registration;
        this.utilityService.changeDataSubject(DataSubject);
        this.utilityService.changeDataSubject(DataSubject);
        if (this.registration.registeredPackets.length === 1) {
          this.isOnlyOnePacket = true;
        }
        this.loaded = false;
        // fetch also the delivery
        this.getDelivery();
      }
    }, err => {
      if (err?.error?.message) {
        console.error(err?.error?.message);
      } else {
        this.isServerDown = true;
        console.error(err?.error?.message);
      }
    });
  }

  getDelivery() {
    const deliveryId = this.registration?.registeredPackets[0].delivranceUuid;
    if (deliveryId !== null) {
      this.deliveryService.findById(deliveryId).subscribe(delivery => {
        if (delivery) {
          this.delivery = delivery;
        }
      });
    }
  }

  private dataInitialization(): void {
    this.getRegistration();
    this.getCurrentUrl();
    this.generateBulkActions();
    this.completedPacketList = new Array<Packet>();
  }

  private generateBulkActions() {
    let actions: Action = new Action(this.translateService);
    // get current logged in user role
    if (this.utilityService.isAdmin) {
      actions.getActions().forEach(row => {
        if (row.roles.includes(RoleType.Admin) && row.status === 'delivery') {
          this.bulkActions.push(row);
        }
      });
    }

    if (this.utilityService.isCommercial) {
      actions.getActions().forEach(row => {
        if (row.roles.includes(RoleType.Commercial) && row.status === 'delivery') {
          this.bulkActions.push(row);
        }
      });
    }

    if (this.utilityService.isDeliverer) {
      actions.getActions().forEach(row => {
        if (row.roles.includes(RoleType.Deliverer) && row.status === 'delivery') {
          this.bulkActions.push(row);
        }
      });
    }
  }

  onPacketCheck(completed: boolean, packet: Packet): void {
    if (completed) {
      // Add the packet to the list of selected packets
      if (!this.completedPacketList.includes(packet)) {
        this.completedPacketList.push(packet);
      }
    } else {
      // Remove the packet from the list of selected packets
      if (this.completedPacketList.includes(packet)) {
        let i = -1;
        this.completedPacketList.map(_packet => {
          ++i;
          if (packet.packetUuid === _packet.packetUuid) {
            return this.completedPacketList.splice(i, 1);
          }
        });
      }
    }

    this.allCompleted = this.registration.registeredPackets.length === this.completedPacketList.length;
  }

  applyAction(confirmationView) {
    const selectedAction = this.actionControl.value;

    if (this.completedPacketList.length === 0) {
      this.toastrService.error(this.translateService.instant('packets.no_selection'));
      return;
    }

    if (selectedAction === '' || selectedAction.length === 0) {
      this.toastrService.error(this.translateService.instant('global.no_action'));
      return;
    }

    if (selectedAction.action === 'deliver') {
      if (this.completedPacketList.length !== this.registration.registeredPackets.length) {
        this.toastrService.error(this.translateService.instant('global.select_all'));
        return;
      } else {
        this.modalService.open(confirmationView, { centered: true, scrollable: true, size: 'md' });
      }
    }

    if (selectedAction.action === 'return') {
      this.generateRandomString();
      if (this.completedPacketList.length !== this.registration.registeredPackets.length) {
        this.toastrService.error(this.translateService.instant('global.select_all'));
        return;
      } else {
        if (this.reasonCtrl.value) {
          this.modalService.open(confirmationView, { centered: true, scrollable: true, size: 'md' });
        } else {
          this.toastrService.error(this.translateService.instant('global.reason'));
        }
      }
    }
  }

  getPacketUuids(): string[] {
    let packetids = [];
    this.registration.registeredPackets.forEach(packet => {
      packetids.push(packet.packetUuid);
    });
    return packetids;
  }

  getCurrentOperator(): Promise<Operator> {
    return new Promise((resolve, reject) => {
      this.operatorService.getAllOperators().subscribe(operators => {
        const currentOperator = operators.find(x => x.user.userUuid === this.utilityService.userId);
        if (currentOperator) resolve(currentOperator);
      });
    });
  }

  applyDelivery() {
    const signature = this.signatureCtl.value;
    const packets = this.registration.registeredPackets.filter(packet => {
      return packet;
    });

    if (signature && signature.length > 0) {
      this.getCurrentOperator().then(operator => {
        if (operator) {
          const delivery: Delivery = {
            delivranceUuid: null,
            deliverBy: operator,
            delivranceDetails: signature,
            deliveredPackets: packets,
            createdBy: operator.user.userUuid,
            modifiedBy: operator.user.userUuid,
            createdAt: '',
            modifiedAt: ''
          }
  
          this.deliveryService.addOne(delivery).subscribe(addedDelivery => {
            if (delivery) {
              DataSubject.notification.taskRemoved = true;
              this.utilityService.changeDataSubject(DataSubject);
              this.modalService.dismissAll();
              this.toastrService.success(this.translateService.instant('deliveries.success_msg'));
              this.router.navigate(['/delivery/todeliver']);
            }
          }, err => {
            this.toastrService.error(this.translateService.instant('global.unex_error'));
            console.error(err?.error?.message);
          });
        }
      });
    }
  }

  applyReturn() {
    const signature = this.signatureCtl.value;

    if (signature && signature.length > 0 && signature === this.verificationKey) {
      const reason = this.reasonCtrl.value.name;

      const packetStateGroup: PacketStateGroup = {
        registrationUuid: this.registration.registrationUuid,
        status: STATUSTYPE.IN_STORE,
        actualLocation: this.registration.store.address,
        details: reason,
        packetUuids: this.getPacketUuids()
      }

      this.packetStateService.createPacketStateGroup(packetStateGroup).subscribe(result => {
        DataSubject.notification.taskRemoved = true;
        this.utilityService.changeDataSubject(DataSubject);

        this.toastrService.success(this.translateService.instant('actions.success_task_remove'));
        this.modalService.dismissAll();
        this.router.navigate(['/delivery/todeliver']);
      }, err => {
        this.toastrService.error(this.translateService.instant('actions.action_error'));
        console.error(err?.error?.message);
      });
    }
  }

  generateRandomString() {
    this.verificationKey = Math.random().toString(36).substring(7);
  }

  toPreviousView() {
    this.location.back();
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  reverseTime(time: string): string {
    return time != null ? time.split("-").reverse().join("-") : '';
  }
}
