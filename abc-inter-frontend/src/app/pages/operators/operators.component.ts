import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { OperatorsService } from '../../services/operators/operators.service';
import { User } from '../../models/user/user';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Operator } from '../../models/operator/operator';
import { Gender, allGenders } from '../../models/gender/gender';
import { OperatorType, allOperatorTypes } from '../../models/operator/operatorType';
import { Role } from '../../models/role/role';
import { RolesService } from '../../services/roles/roles.service';
import { RoleType } from '../../models/role/RoleType';
import * as $ from 'jquery';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class OperatorsComponent implements OnInit {

  currentUrl: string;
  errorMessage = '';
  totalItems: number;
  page = 1;
  operators: Array<Operator>;
  operator: Operator;
  roles: Array<Role>;
  // selectedRole: Role;
  operatorTypes: Array<string>;
  operatorType: OperatorType;
  gender: string;
  genders = Array<string>();
  host = `${environment.apiUrl}/user/avatar/`;
  hide = true;
  firstname: string;
  isAsTable = true;
  loaded = true;
  isServerDown = false;
  key = 'username';
  reverse = false;
  operatorList = [];
  serverDownMsg = '';
  isAdmin = false;
  isCommercial = false;
  isDeliverer = false;
  telOptions = { initialCountry: 'cm', onlyCountries: ['cm'] };
  private isOperatorPhoneValid: any;
  errors = {
    phoneNb: '',
    errorMessage: ''
  };

  constructor(
    private operatorService: OperatorsService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private router: Router,
    private modalService: NgbModal,
    private rolesService: RolesService,
    private utilityService: UtilityService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {
    this.currentUrl = this.translate.instant('global.' + this.router.url.substring(1));
    this.initData();
    // the click on the search btn
    $('#search-operator').on('click', () => {
      this.hide = !this.hide;
    });
  }

  getCurrentUserRole() {
    this.isAdmin = this.utilityService.isAdmin;
    this.isCommercial = this.utilityService.isCommercial;
    this.isDeliverer = this.utilityService.isDeliverer;
  }

  initData() {
    this.getCurrentUserRole();

    this.operatorService.getAllOperators().subscribe(data => {
      if (null != data) {
        this.operators = data;
        this.totalItems = data.length;
        // save all operator
        this.operatorList = data;
        // filter no admin if the current connected user is not an admin
        if (!this.isAdmin) {
          this.operatorList = this.operatorList.filter(x => x.operatorType.toLowerCase() !== 'admin');
        }
        // set the flag
        this.loaded = false;
        // to populate list of role as a list of string
        this.getAllRoles();
        this.populateOperatorTypeList();
        this.populateGenderList();
      }
    }, err => {
      this.isServerDown = true;
      console.error(err);
    });
  }

  search() {
    if (this.firstname === '') {
      this.initData();
    } else {
      // remove the admin from the list iff current connected user is not an admin
      if (this.isCommercial || this.isDeliverer) {
        this.operators = this.operators.filter(x => !x.user.roles.find(y => y.name.toLowerCase() === RoleType.Admin.toLowerCase()))
      }
      this.operatorList = this.operators.filter(res => {
        return res.user.firstname.toLowerCase().match(this.firstname.toLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getOperator(id: string, modalView: any) {
    if (this.isAdmin) {
      this.operator = new Operator();
      this.operator.user = new User(null, null, null, null, null, null, null, null, null, null, []);
      if (null != id) {
        this.operatorService.getOperatorById(id).subscribe({
          next: (result: Operator) => {
            this.operator = result;
            const idx = allOperatorTypes.findIndex(x => x === this.operator.operatorType);
            this.operator.operatorType = this.translate.instant(`roles.${allOperatorTypes[idx].toLowerCase()}`);
            this.operator.user.gender = this.translate.instant('global.' + this.operator.user.gender.toLowerCase());
          },
          error: (err) => {
            if (!!err.error.message) {
              this.errorMessage = err.error.message;
            } else {
              this.isServerDown = true;
            }
          },
          complete: () => {
            this.checkOperatorPhone(this.operator.user.tel);
            this.openModal(modalView);
          }
        });
      } else {
        this.openModal(modalView);
      }
    }
  }

  getAllRoles() {
    this.roles = new Array<Role>();
    this.rolesService.findAllRoles().subscribe((roles) => {
      if (roles) {
        /* Customer is not needed since the user is an operator. If role customer
        assigned to any operator,than this operator will be logged in as customer */
        roles.forEach(x => {
          if (x.name.toLowerCase() !== RoleType.Customer.toLowerCase()) {
            this.roles.push(x);
          }
        });
      }
    }, (err) => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
      } else {
        this.isServerDown = true;
      }
    });
  }

  isDefaultAvatar(user: User): boolean {
    return user.profileImg === null || user.profileImg.length === 0;
  }

  private getOperatorRole(operatorType: string): Role {
    if (operatorType.toLowerCase() === OperatorType.Admin.toLowerCase()) {
      return this.roles.find(x => x.name.toLowerCase() === RoleType.Admin.toLowerCase());
    } else if (operatorType.toLowerCase() === OperatorType.Commercial.toLowerCase()) {
      return this.roles.find(x => x.name.toLowerCase() === RoleType.Commercial.toLowerCase());
    } else if (operatorType.toLowerCase() === OperatorType.Deliverer.toLowerCase()) {
      return this.roles.find(x => x.name.toLowerCase() === RoleType.Deliverer.toLowerCase());
    } else if (operatorType.toLowerCase() === OperatorType.Storekeeper.toLowerCase()) {
      return this.roles.find(x => x.name.toLowerCase() === RoleType.Storekeeper.toLowerCase());
    }
    // A role could not be associated to the selected operator type
    return null;
  }

  addNewOperator() {
    // set new values
    if (this.gender.toLowerCase() === 'masculin' || this.gender.toLowerCase() === 'male' ||
      this.gender.toLowerCase() === 'global.male') {
      this.operator.user.gender = Gender.Male;
    } else if (this.gender.toLowerCase() === 'feminin' || this.gender.toLowerCase() === 'female' ||
      this.gender.toLowerCase() === 'global.female') {
      this.operator.user.gender = Gender.Female;
    } else {
      this.operator.user.gender = Gender.Others;
    }

    this.operator.user.roles = [];
    // this.operator.user.roles.push(this.selectedRole);
    // set the user role before saved
    const role = this.getOperatorRole(this.operator.operatorType);
    // first check whether any role was found or not
    if (role) {
      this.operator.user.roles.push(role);
      // set operator type before save the object
      this.operator.operatorType = this.operatorType.toUpperCase();
      this.operatorService.createOperator(this.operator).subscribe((res) => {
        if (null != res) {
          // refresh the list
          this.initData();
          // and notify the user
          this.toastService.success(this.translate.instant('operators.create-success'));
          this.modalService.dismissAll();
        }
      }, (err) => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastService.error(this.translate.instant('operators.create-failed'));
          // needed to display the old value of operator type when error happened.
          this.operator.operatorType = this.translate.instant(this.operator.operatorType);
          this.operator.user.gender = this.translate.instant('' + this.operator.user.gender);
        }
      });
    } else {
      this.toastService.error(this.translate.instant('operators.role_not_found'));
    }
  }

  deleteOperator() {
    this.operatorService.deleteOperatorById(this.operator.operatorUuid).subscribe((res) => {
      for (let i = 0; i < this.operatorList.length; ++i) {
        if (this.operatorList[i].operatorUuid === this.operator.operatorUuid) {
          // decremente the value
          this.totalItems--;
          // Notify the user
          this.toastService.success(this.operator.user.username + ' ' + this.translate.instant('operators.delete-success'));
          // remove the deleted user from the list and exit.
          return this.operatorList.splice(i, 1);
        }
      }
    }, err => {
      if (err) {
        this.toastService.error(this.operator.user.username + ' ' + this.translate.instant('operators.delete-failed'));
      }
    });
    // close modal and notified
    this.modalService.dismissAll();
  }

  updateOperator() {
    this.operator.user.roles = [];
    // set the user role before saved
    if (this.operatorType) {
      this.operator.operatorType = this.operatorType.toUpperCase();
    } else {
      this.operator.operatorType = this.operator.operatorType.toUpperCase();
    }
    const role = this.getOperatorRole(this.operator.operatorType);
    // first check whether any role was found or not
    if (role) {
      this.gender = this.operator.user.gender;
      if (this.gender.toLowerCase() === 'masculin' || this.gender.toLowerCase() === 'male' ||
        this.gender.toLowerCase() === 'global.male') {
        this.operator.user.gender = Gender.Male;
      } else if (this.gender.toLowerCase() === 'feminin' || this.gender.toLowerCase() === 'female' ||
        this.gender.toLowerCase() === 'global.female') {
        this.operator.user.gender = Gender.Female;
      } else {
        this.operator.user.gender = Gender.Others;
      }
      this.operator.user.roles = Array.of(role);
      this.operatorService.updateOperator(this.operator).subscribe((res) => {
        if (null != res) {
          // close the modal window
          this.modalService.dismissAll();
          // refresh the list
          this.initData();
          // notify the user
          this.toastService.success(this.operator.user.username + ' ' + this.translate.instant('customers.customer-update'));
        }
      }, err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastService.error(this.translate.instant('customers.error-update'));
          this.operator.operatorType = this.translate.instant(this.operator.operatorType);
          this.operator.user.gender = this.translate.instant('' + this.operator.user.gender.toLowerCase());
        }
      });
    } else {
      this.toastService.error(this.translate.instant('operators.role_not_found'));
    }
  }

  openModal(content) {
    this.modalService.open(content, { centered: true, scrollable: true, size: 'xl' });
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  clearErrorMessage() {
    if (this.errorMessage.length > 0) {
      this.errorMessage = '';
    }
  }

  byRoleUuid(item1: Role, item2: Role) {
    return item1 && item2 && item1.roleUuid === item2.roleUuid;
  }

  roleNameToBeDisplaying(name: string) {
    const displayingName = name.split('_')[1].toLowerCase();
    return this.translate.instant('roles.' + displayingName);
  }

  populateOperatorTypeList() {
    // If the current user is not an admin, than we remove the role admin from the list.
    // The user should not be able to add an administrator
    this.operatorTypes = this.isAdmin ? allOperatorTypes.map(x => this.translate.instant(`roles.${x.toLowerCase()}`))
      : allOperatorTypes.map(x => this.translate.instant(`roles.${x.toLowerCase()}`)).filter(x => x !== OperatorType.Admin);
  }

  populateGenderList() {
    this.genders = allGenders.map(gender => this.translate.instant('' + gender));
  }

  onTypeChange(event) {
    const value = event.target.value;
    if (value === 'Livreur') {
      this.operatorType = OperatorType.Deliverer;
    } else if (value === 'Administrateur') {
      this.operatorType = OperatorType.Admin;
    } else if (value === 'Magasinier') {
      this.operatorType = OperatorType.Storekeeper;
    } else if (value === 'Commerciale') {
      this.operatorType = OperatorType.Commercial;
    } else {
      this.operatorType = value;
    }
  }

  onGenderChange(event) {
    const value: string = event.target.value;
    if (value.toLowerCase() === 'masculin' || value.toLowerCase() === 'male' || value.toLowerCase() === 'global.male') {
      this.gender = Gender.Male;
    } else if (value.toLowerCase() === 'feminin' || value.toLowerCase() === 'female' || value.toLowerCase() === 'global.female') {
      this.gender = Gender.Female;
    } else {
      this.gender = Gender.Others;
    }
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.firstname = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.initData();
  }

  // returns true if phone number is valid otherwise false.
  checkOperatorPhone(isValid) {
    this.isOperatorPhoneValid = isValid;
    if (this.isOperatorPhoneValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  checkValidPhoneNumber(event) {
    const reg = this.utilityService.regPortableNumber;
    this.checkOperatorPhone(reg.test(event.target.value));
  }
}

