import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Registration } from 'src/app/models/registration/registration';
import { User } from 'src/app/models/user/user';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  public currentUrl: string;

  public userToSave: User;

  public isValidPhone: boolean = false;

  public repeatPwdValue: string;

  // if a simple user, we just update the user, otherwise we update the user and update the registration also
  public isUserToUpdate: boolean = false;

  errors = {
    errorMessage: ''
  };

  constructor(
    private router: Router,
    private userService: UsersService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private registrationService: RegistrationService,
    private activatedroute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.currentUrl = this.currentUrl = this.router.url.substr(1).split('/')[1];
  }

  getUser(userForm) {
    this.userToSave = userForm.user;
    this.isValidPhone = userForm.phoneStatus;
    this.repeatPwdValue = userForm.rPwd;
    this.isUserToUpdate = userForm.isUser;
  }

  updateUser() {
    if (this.userToSave && this.isUserFormValid()) {
      // check whether is to update a user or a packet receiver
      if (this.isUserToUpdate) {
        this.userService.updateById(this.userToSave).subscribe(result => {
          if (result) {
            this.errors.errorMessage = '';
            this.toastrService.success(this.translateService.instant('global.update-success'));
          }
        }, err => {
          if (err?.error?.message) {
            if (err?.error?.message.includes('Email')) {
              this.errors.errorMessage = this.translateService.instant('global.email_in_use');
            } else if (err?.error?.message.includes('Username')) {
              this.errors.errorMessage = this.translateService.instant('global.username_taken');
            }
          }
          this.toastrService.error(this.translateService.instant('global.update-error'));
        });
      } else {
        this.updateRegistration(this.userToSave).then(result => {
          if (result) {
            this.toastrService.success(this.translateService.instant('global.update-success'));
          }
        });
      }
    }
  }

  isUserFormValid(): boolean {
    if (this.userToSave) {
      if (
        this.userToSave.username.length > 0 &&
        this.userToSave.firstname.length > 0 &&
        this.userToSave.lastname.length > 0 &&
        this.userToSave.gender.length > 0 &&
        this.userToSave.password === this.repeatPwdValue &&
        this.isValidPhone
      ) {
        return true;
      }
    }
    return false;
  }

  getRegistration(): Promise<Registration> {
    return new Promise((resolve, reject) => {
      this.activatedroute.paramMap.subscribe(params => {
        this.registrationService.findById(params.get('id')).subscribe(registration => {
          resolve(registration);
        }, err => {
          console.log(err?.error?.message);
        });
      });
    });
  }

  updateRegistration(receiver: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.getRegistration().then(registration => {
        if (receiver.userUuid !== null) {
          this.userService.updateById(this.userToSave).subscribe(response => {
            if (response) {
              resolve(true);
            }
          });
        } else {
          this.userService.addOne(receiver).subscribe(_receiver => {
            if (_receiver) {
              registration.receiver = _receiver;
              this.registrationService.updateRegistration(registration).subscribe(response => {
                if (response) {
                  resolve(true);
                }
              });
            }
          });
        }
      });
    });
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

}
