import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { User } from '../../models/user/user';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from '../../services/users/users.service';
import * as $ from 'jquery';
import { TranslateService } from '@ngx-translate/core';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class UsersComponent implements OnInit {

  // will help to filter the list of users
  data: Array<User>;
  // list of all users
  userList = [];
  user = new User(null, null, null, null, null, false, null, null, null, null, null);
  totalItems: number;
  page: Number = 1;
  currentUrl: string;
  form: any = {};
  isSuccessful = false;
  errorMessage = '';
  host = `${environment.apiUrl}/user/avatar/`;
  loaded: boolean = true;
  isServerDown: boolean = false;
  hide = true;
  firstname: string;
  isAsTable: boolean = true;
  key: string = 'username';
  reverse: boolean = false;
  serverDownMsg = '';

  constructor(
    private router: Router,
    private usersService: UsersService,
    private modalService: NgbModal,
    private toastService: ToastrService,
    private translate: TranslateService,
    private utilityService: UtilityService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {
    // the click on the search btn
    $('#search-user').click(() => {
      this.hide = this.hide === true ? false : true;
    });

    this.data = new Array<User>();
    this.initData();
    this.currentUrl = this.router.url.substr(1).toUpperCase();
  }

  initData() {
    this.usersService.findAll().subscribe((res) => {
      if (null != res) {
        this.data = res;
        this.totalItems = res.length;
        // save all user
        this.userList = this.data;
        // set the flag
        this.loaded = false;
      }
    }, err => {
      if (err?.error?.error === 'Unauthorized') {
        this.utilityService.logout();
      }
      this.isServerDown = true;
    });
  }

  search() {
    if (this.firstname == "") {
      // Initialize the list
      this.initData();
    } else {
      this.userList = this.data.filter(res => {
        return res.firstname.toLocaleLowerCase().match(this.firstname.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  openModal(content) {
    this.modalService.open(content, { centered: true, scrollable: true });
  }

  addUser() {
    this.usersService.addOne(this.form).subscribe(res => {
      if (null != res) {
        this.isSuccessful = true;
        // refresh the user list
        this.initData();
        // if new user successful added, close the modal and reload the location
        this.modalService.dismissAll();
        // than notify
        this.toastService.success(this.translate.instant('users.success-add'));
      }
    }, err => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
      } else {
        this.isServerDown = true;
      }
    }
    );
  }

  getUser(id: string) {
    this.usersService.findById(id).subscribe((data) => {
      if (null != data) {
        this.user = data;
      }
    }, (err) => {
      console.error("getUser: ", err);
    });
  }

  updateUser() {
    this.usersService.updateById(this.user).subscribe(data => {
      if (null != data) {
        // close the modal window
        this.modalService.dismissAll();
        // refresh the list
        this.initData();
        // notify the user
        this.toastService.success(`${this.user.username} ${this.translate.instant('users.success-update')}`);
      }
    }, err => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(`${this.translate.instant('users.error-update')}`);
      } else {
        this.isServerDown = true;
      }
    });
  }

  deleteUser(id: string, username: string) {
    this.usersService.deleteOne(id).subscribe((res) => {
      for (let i = 0; i < this.userList.length; ++i) {
        if (this.userList[i].userUuid === id) {
          // decremente the value
          this.totalItems--;
          // close modal and notified
          this.modalService.dismissAll();
          // Notify the user
          this.toastService.success(`${username} ${this.translate.instant('users.success-delete')}`);
          // remove the deleted user from the list and exit.
          return this.userList.splice(i, 1);
        }
      }
    }, (err) => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(`${username} ${this.translate.instant('users.error-delete')}`);
      } else {
        this.isServerDown = true;
      }
    });
  }

  isDefaultAvatar(user: User): boolean {
    return user.profileImg === null || user.profileImg.length === 0;
  }

  clearInput(form) {
    form.resetForm();
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.firstname = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.initData();
  }
}
