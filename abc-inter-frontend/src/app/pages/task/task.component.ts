import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, animate, transition } from '@angular/animations';
import * as $ from 'jquery';

import { ToastrService } from 'ngx-toastr';
import { UtilityService } from 'src/app/services/utility/utility.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ])
  ]
})
export class TaskComponent implements OnInit {

  currentUrl: string;
  hide = true;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private toastService: ToastrService,
    private utilityService: UtilityService
  ) { }

  ngOnInit(): void {
    // the click on the search btn
    $('#search-task').click(() => {
      this.hide = this.hide === true ? false : true;
    });

    this.currentUrl = this.router.url.substr(1).split('/')[0];
  }

  addTask(content) {
    this.utilityService.changeDataSubject({ 'isPacketToBeDeliveredCall': true });
    this.modalService.open(content, { centered: true, scrollable: true, size: 'lg' });
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

}
