import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketStoreComponent } from './packet-store.component';

describe('PacketStoreComponent', () => {
  let component: PacketStoreComponent;
  let fixture: ComponentFixture<PacketStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
