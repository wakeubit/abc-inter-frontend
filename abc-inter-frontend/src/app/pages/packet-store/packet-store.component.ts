import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { PacketStore } from 'src/app/models/packet-store/packet-store';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Address } from '../../models/address/address';
import { PacketStoreService } from 'src/app/services/packet-store/packet-store.service';
import * as $ from 'jquery';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-package-store',
  templateUrl: './packet-store.component.html',
  styleUrls: ['./packet-store.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})

export class PacketStoreComponent implements OnInit {

  currentUrl: string;

  packetStores: Array<PacketStore>;

  packetStoreList = [];

  packetStore: PacketStore;

  totalItems: number;

  page = 1;

  errorMessage = '';

  host = environment.apiUrl + '/user/avatar/';

  hide = true;

  storeName: string;

  isAsTable = true;

  key = 'username';

  reverse = false;

  loaded = true;

  isServerDown = false;

  serverDownMsg = '';

  isAdmin = false;

  isCommercial = false;

  isDeliverer = false;
  telOptions = { initialCountry: 'cm', onlyCountries: ['cm'] };
  isStorePhoneValid: Boolean;
  errors = {
    phoneNb: '',
    errorMessage: ''
  };

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private toastService: ToastrService,
    private packetStoreService: PacketStoreService,
    private translate: TranslateService,
    private utilityService: UtilityService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {
    this.initData();
    this.currentUrl = this.translate.instant(`packet-store.${this.router.url.substring(1).toLowerCase()}`);
    // the click on the search btn
    $('#search-customer').click(() => {
      this.hide = this.hide === true ? false : true;
    });
  }

  getCurrentUserRole() {
    this.isAdmin = this.utilityService.isAdmin;
    this.isCommercial = this.utilityService.isCommercial;
    this.isDeliverer = this.utilityService.isDeliverer;
  }

  initData() {

    this.getCurrentUserRole();

    this.packetStoreService.findAllStore().subscribe((packetStores) => {
      if (null != packetStores) {
        this.packetStores = packetStores;
        this.totalItems = packetStores.length;
        // save all packetstores
        this.packetStoreList = this.packetStores;
        // set the flag
        this.loaded = false;
      }
    }, err => {
      if (!!err.error.message) {
        console.error(err);
      } else {
        this.isServerDown = true;
      }
    });
  }

  search() {
    if (this.storeName === '') {
      this.ngOnInit();
    } else {
      this.packetStoreList = this.packetStores.filter(res => {
        return res.name.toLocaleLowerCase().match(this.storeName.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getPacketStore(id: string) {
    if (id != null) {
      this.packetStore = new PacketStore();
      this.packetStore.address = new Address(null, null, null, null, null, 0, 0);
      this.packetStoreService.findStoreById(id).subscribe((packetStore) => {
        if (null != packetStore) {
          this.packetStore = packetStore;
        }
      });
    } else {
      this.packetStore = new PacketStore();
      this.packetStore.address = new Address(null, null, null, null, null, 0, 0);
    }
  }

  addNewPacketStore() {
    this.packetStoreService.addStore(this.packetStore).subscribe((packetStore) => {
      if (null != packetStore) {
        // refresh the user list
        this.initData();
        // if new user successful added, close the modal and reload the location
        this.modalService.dismissAll();
        // than notify
        this.toastService.success(this.translate.instant('packet-store.new-packet-store-add'));
      }
    }, (err) => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(this.translate.instant('global.error-save'));
      } else {
        this.isServerDown = true;
      }
    });
  }

  updateStore() {
    this.packetStoreService.updateStore(this.packetStore).subscribe((packetStore) => {
      if (null != packetStore) {
        // close the modal window
        this.modalService.dismissAll();
        // refresh the list
        this.initData();
        // notify the user
        this.toastService.success(this.packetStore.name + ' ' + this.translate.instant('packet-store.packet-store-update'));
      }
    }, err => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(this.translate.instant('customers.error-update'));
      } else {
        this.isServerDown = true;
      }
    });
  }

  deleteStore() {
    this.packetStoreService.deleteStoreById(this.packetStore.packetStoreUuid).subscribe((packetStore) => {
      for (let i = 0; i < this.packetStoreList.length; ++i) {
        if (this.packetStoreList[i].packetStoreUuid === this.packetStore.packetStoreUuid) {
          // decremente the value
          this.totalItems--;
          // Notify the user
          this.toastService.success(this.packetStore.name + ' ' + this.translate.instant('customers.success-del'));
          // remove the deleted packetstore from the list and exit.
          return this.packetStoreList.splice(i, 1);
        }
      }
    }, err => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(this.packetStore.name + ' ' + this.translate.instant('customers.success-del'));
      } else {
        this.isServerDown = true;
      }
    });

    this.modalService.dismissAll();
  }

  openModal(content) {
    this.modalService.open(content, { centered: true, scrollable: true, size: 'xl' });
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  clearErrorMessage() {
    if (this.errorMessage.length > 0) {
      this.errorMessage = '';
    }
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.storeName = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.initData();
  }

  preventInput(event, field) {
    let value = null;
    if (field.name === 'latitude') {
      value = this.packetStore.address.latitude;
      if (value >= 99.999999) {
        event.preventDefault();
        this.packetStore.address.latitude = parseInt(value.toString().substring(0, 2));
      }
    }
    if (field.name === 'longitude') {
      value = this.packetStore.address.longitude;
      if (value >= 99.999999) {
        event.preventDefault();
        this.packetStore.address.longitude = parseInt(value.toString().substring(0, 2));
      }
    }
    if (field.name === 'volume') {
      value = this.packetStore.volume;
      if (value > 100) {
        event.preventDefault();
        this.packetStore.volume = parseInt(value.toString().substring(0, 2));
      }
    }
  }

  checkValidPhoneNumber(event) {
    const reg = this.utilityService.regPortableNumber;
    this.checkStorePhone(reg.test(event.target.value));
  }

  // returns true if phone number is valid otherwise false.
  checkStorePhone(isValid) {
    this.isStorePhoneValid = isValid;
    if (this.isStorePhoneValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }
}
