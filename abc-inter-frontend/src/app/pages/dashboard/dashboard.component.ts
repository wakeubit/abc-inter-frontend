import { Component, OnInit, AfterViewInit } from '@angular/core';

import Chart from 'chart.js';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample2
} from '../../variables/charts';

import { UtilityService } from 'src/app/services/utility/utility.service';
import { TranslateService } from '@ngx-translate/core';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { DeliveriesService } from 'src/app/services/deliveries/deliveries.service';
import { STATUSTYPE } from 'src/app/models/statusType/statusType';
import {CustomersService} from "../../services/customers/customers.service";
import {Customer} from "../../models/customer/customer";
import {User} from "../../models/user/user";
import {UsersService} from "../../services/users/users.service";


import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  public brandName = "AbcInter";
  public logistics = [];
  public totalToBeDelivered: number = 0;
  public totalDeliveriesInprogress: number = 0;
  public totalReturnedDeliveries: number = 0;
  public totalDeliveries: number = 0;
  public isCustomer: boolean;
  public bonusCount: number = 0;
  public user: User;
  public isAdmin: boolean;
  public isCommercial: boolean;
  public isDeliverer: boolean;

  constructor(
    private utilityService: UtilityService,
    private translateService: TranslateService,
    private registrationService: RegistrationService,
    private deliveryService: DeliveriesService,
    private usersService: UsersService,

    private modalService: NgbModal
  ) { 
    this.isAdmin = false;
    this.isCommercial = false;
    this.isDeliverer = false;
    this.isCustomer = false;
  }

  ngOnInit() {
    this.initData();
  }

  ngAfterViewInit() {
    this.initChartOrders();
  }

  initChartOrders() {
    const chartOrders = document.getElementById('chart-orders');
    parseOptions(Chart, chartOptions());
    /*const ordersChart = new Chart(chartOrders, {
      type: 'bar',
      options: chartExample2.options,
      data: chartExample2.data
    });*/
  }

  private addRow(name: string, icon: string, total: number, bg: string, link: string): RowData {
    let row: RowData = {
      name: name,
      icon: icon,
      total: total,
      bg: bg,
      link: link
    }
    return row;
  }

  public initData() {
    this.getTotalDeliveriesInprogress();
    if (this.utilityService.isAdmin || this.utilityService.isCommercial) {
      
      this.isAdmin = true;
      this.isCommercial = true;
      this.getAllRegistrations().then(result => {
        this.totalToBeDelivered = result;
        // init deliverer's widgets
        this.logistics = [
          this.addRow(this.translateService.instant('deliveries.to_be_delivered'), 'fas fa-shipping-fast', this.totalToBeDelivered, 'bg-gradient-info', '/orders'),
          this.addRow(this.translateService.instant('packets.in_progress'), 'fas fa-spinner', this.totalDeliveriesInprogress, 'bg-warning', '/delivery/todeliver'),
          this.addRow(this.translateService.instant('registration.returned'), 'fas fa-arrow-left', this.totalReturnedDeliveries, 'bg-danger', '#'),
          this.addRow(this.translateService.instant('packets.to_be_collected'), 'fas fa-people-carry', 0, 'bg-info', '#')
        ];
      });

    } else if (this.utilityService.isDeliverer) {
      this.isDeliverer = true;
      this.getAllDeliveries();
      this.getAllRegistrations().then(result => {
        this.totalToBeDelivered = result;
        // init deliverer's widgets
        this.logistics = [
          this.addRow(this.translateService.instant('deliveries.my_todo'), 'fas fa-shipping-fast', this.totalToBeDelivered, 'bg-gradient-info', '/delivery/todeliver'),
          this.addRow(this.translateService.instant('deliveries.deliveries'), 'fas fa-check', this.totalDeliveries, 'bg-success', '/deliveries'),
          this.addRow(this.translateService.instant('packets.to_be_collected'), 'fas fa-people-carry', 0, 'bg-info', '#')
        ];
      });

    } else if (this.utilityService.isCustomer) {
      this.isCustomer = true;
      this.user = this.utilityService.user;
      if(null !== this.user) {
        this.usersService.getCustomerByUserUuid(this.user.userUuid).subscribe(res => {
          if (null != res) {
            this.bonusCount = res.bonusCount;
            this.logistics = [
              this.addRow(this.translateService.instant('dashboard.bonus'), 'far fa-hand-point-left', this.bonusCount, 'bg-gradient-info', ''),
              this.addRow(this.translateService.instant('dashboard.my_ship'), 'fas fa-list', 3, 'bg-gradient-default', '')
            ];
          }
        }, err => {
          console.error(err?.error?.message);
        });
      }
    }

  }

  getTotalDeliveriesInprogress() {
    this.registrationService.findAllByStatus(STATUSTYPE.READY_FOR_DELIVERY).subscribe(result => {
      if (result) {
        this.totalDeliveriesInprogress = result.totalItems;
      }
    }, err => {
      console.error(err?.error?.message);
    });
  }

  getAllRegistrations(): Promise<number> {
    return new Promise((resolve, reject) => {
      if (this.utilityService.isAdmin || this.utilityService.isCommercial) {
        this.registrationService.findAllByStatus(STATUSTYPE.IN_STORE).subscribe(result => {
          if (result) {
            // filter those who have the status "IN_STORE"
            const toBeDelivered = result.totalItems;
            resolve(toBeDelivered);
          }
        }, err => {
          console.error(err?.error?.message);
        });
      } else {
        this.registrationService.findAllByAssignedTo(this.utilityService.userId).subscribe(result => {
          if (result) {
            // filter those who have the status "IN_STORE"
            const toBeDelivered = result.totalItems;
            resolve(toBeDelivered);
          }
        }, err => {
          console.error(err?.error?.message);
        });
      }
      
    });
  }

  getAllDeliveries() {
    this.deliveryService.findAll().subscribe(deliveries => {
      // filter deliveries by user role
      this.totalDeliveries = this.utilityService.isDeliverer ? deliveries.filter(delivery => delivery.deliverBy.user.userUuid === this.utilityService.userId).length : deliveries.length;
    }, err => {
      console.error(err?.error?.message);
    });
  }
}

interface RowData {
  name: string,
  icon: string,
  total: number,
  bg: string,
  link: string
}
