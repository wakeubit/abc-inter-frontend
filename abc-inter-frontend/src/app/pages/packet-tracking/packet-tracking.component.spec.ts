import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketTrackingComponent } from './packet-tracking.component';

describe('PacketTrackingComponent', () => {
  let component: PacketTrackingComponent;
  let fixture: ComponentFixture<PacketTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
