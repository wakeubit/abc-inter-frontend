import { Component, OnInit } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { PacketStateService } from "src/app/services/packet-state/packet-state.service";
import { TranslateService } from "@ngx-translate/core";
import { SharedDataService } from "src/app/services/sharedData/shared-data.service";

import { PacketTracking } from "src/app/models/packet-tracking/packet-tracking";
import { PacketState } from "src/app/models/packet-state/packetState";

import { STATUSTYPE } from "../../models/statusType/statusType";

@Component({
  selector: "app-packet-tracking",
  templateUrl: "./packet-tracking.component.html",
  styleUrls: ["./packet-tracking.component.css"],
  animations: [
    trigger("searchClicked", [
      state(
        "true",
        style({
          color: "white",
        })
      ),
      state(
        "false",
        style({
          color: "white",
        })
      ),
      transition("true => false", [
        style({ opacity: 0, transform: "translateX(-200px)" }),
        animate(
          "300ms ease-in",
          style({ opacity: 1, transform: "translateX(0)" })
        ),
      ]),
    ]),
    trigger("fadeInOut", [
      state(
        "void",
        style({
          opacity: 0,
        })
      ),
      transition("void <=> *", animate(1000)),
    ]),
    trigger("EnterLeave", [
      state("flyIn", style({ transform: "translateX(0)" })),
      transition(":enter", [
        style({ transform: "translateX(-100%)" }),
        animate("0.5s 300ms ease-in"),
      ]),
      transition(":leave", [
        animate("0.3s ease-out", style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})
export class PacketTrackingComponent implements OnInit {
  currentUrl: string;

  totalItems: number;

  page: number = 1;

  errorMessage = "";

  trackingNbr: string;

  isAsTable: boolean = true;

  reverse: boolean = false;

  loaded: boolean = true;

  isServerDown: boolean = false;

  serverDownMsg = "";

  packetStateList: PacketTracking;

  public statusType = STATUSTYPE;

  countSearch = 0;

  inProgress: boolean;
  packetStateLoaded: boolean;
  trackingNbFound: boolean;

  constructor(
    private router: Router,
    private packetStatesService: PacketStateService,
    private translate: TranslateService,
    private modalService: NgbModal,
    private activatedroute: ActivatedRoute,
    private sharedDataService: SharedDataService
  ) {
    this.serverDownMsg = this.translate.instant("global.server-down");
    this.packetStateLoaded = false;
    this.trackingNbFound = false;
    this.inProgress = false;
  }

  ngOnInit(): void {
    this.initData();
    this.currentUrl = this.router.url.substring(1).toUpperCase();
    this.getTrackingNumberInParam();
  }

  private getTrackingNumberInParam() {
    this.activatedroute.paramMap.subscribe((params) => {
      const trackingnb = params.get("nb");
      this.getPacketStateInfos(trackingnb);
    });
  }

  getPacketStateInfos(GlobaltrackingNumber: string) {
    this.countSearch++;
    this.packetStatesService
      .findPacketStatesByGlobalTrackingNumber(GlobaltrackingNumber)
      .subscribe({
        next: (result) => {
          this.packetStateList = {
            globalStatus: result.globalStatus,
            globalTrackingNumber: result.globalTrackingNumber,
            registrationUuid: result.registrationUuid,
            packetsStates: [],
          };
          // set packets state list
          for (const key of Object.keys(result.packetsStates)) {
            this.packetStateList.packetsStates.push(
              result.packetsStates[`${key}`]
            );
          }
        },
        error: (err) => (this.errorMessage = err.error.errorMessage),
        complete: () => {
          this.inProgress = false;
          this.trackingNbFound = true;
        },
      });
  }

  formatDate(date: Date): string {
    var dateString = date
      .toString()
      .split("T")[0]
      .split("-")
      .reverse()
      .join("-");
    var uhr = date.toString().substring(11, 18).split(".")[0].substring(0, 5);
    return `${dateString} / ${uhr}`;
  }

  initData() {
    this.loaded = false;
  }

  search() {
    this.trackingNbFound = false;
    this.inProgress = this.trackingNbr.length > 0;
    if (this.trackingNbr.length == 10) {
      this.getPacketStateInfos(this.trackingNbr);
    } else if (this.trackingNbr.length > 11) {
      this.trackingNbFound = false;
      this.inProgress = false;
    }
  }

  navigateToHome() {
    this.router.navigate(["/dashboard"]);
  }

  clearErrorMessage() {
    if (this.errorMessage.length > 0) {
      this.errorMessage = "";
    }
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.trackingNbr = "";
    // reset the list
    this.initData();
  }

  openHelp(content) {
    this.modalService.open(content, {
      centered: true,
      scrollable: true,
      size: "md",
    });
  }

  getDeliveryDate(packetStates: PacketState[]): string {
    const delivery = packetStates?.find(
      (x) => x.status === this.statusType.DELIVERED
    );
    return this.formatDate(delivery?.createdAt);
  }

  getDeliveryConfirmation(packetStates: PacketState[]): string {
    const delivery = packetStates?.find(
      (x) => x.status === this.statusType.DELIVERED
    );
    return delivery.details;
  }

  getPacketStatus(status: string): string {
    if (status === this.statusType.IN_STORE) {
      return this.translate.instant("trackings.inStore");
    } else if (status === this.statusType.READY_FOR_DELIVERY) {
      return this.translate.instant("trackings.readyForDelivrance");
    } else if (status === this.statusType.DELIVERED) {
      return this.translate.instant("trackings.delivered");
    } else if (status === this.statusType.COLLECTED) {
      return this.translate.instant("trackings.collected");
    } else if (status === this.statusType.TO_BE_COLLECTED) {
      return this.translate.instant("trackings.toBeCollected");
    }
  }

  startScanner(content) {
    this.modalService.open(content, {
      centered: true,
      scrollable: true,
      size: "xl",
    });
  }

  closeModal(close: boolean) {
    this.modalService.dismissAll();
  }

  onStopClicked() {
    // close the modal
    this.modalService.dismissAll();
    // Get current scanner instance and stop it
    this.sharedDataService.currentScanner
      .subscribe((curInstance) => {
        if (curInstance && curInstance.isScanning) {
          curInstance.stop();
        }
      })
      .unsubscribe();
  }
}
