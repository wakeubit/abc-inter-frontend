import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ShippingCostService } from '../../services/shipping-cost/shipping-cost.service';
import { ShippingCost } from '../../models/shippingCost/shippingCost';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { PacketCategoryService } from '../../services/packet-category/packet-category.service';
import { PacketCategory } from '../../models/packet-category/packetCategory';
import * as $ from 'jquery';
import { UtilityService } from 'src/app/services/utility/utility.service';

@Component({
  selector: 'app-shipping-cost',
  templateUrl: './shipping-cost.component.html',
  styleUrls: ['./shipping-cost.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class ShippingCostComponent implements OnInit {

  hide = true;
  shippingCosts: any[];
  shippingCostList = [];
  currentUrl: string;
  page: number = 1;
  totalItems: number;
  isSuccessful = false;
  allShippingCost: {
    shippingCostUuid: string,
    price: number,
    currency: string,
    bonusPoints: number,
    category: PacketCategory,
    shippingType: string
  }[] = [];
  currentShippingCost: ShippingCost = new ShippingCost(
    null,
    0,
    null,
    0,
    null,
    null
  );
  form: any = {};
  types: string[];
  currencies: string[];
  errorMessage: string;
  shippingCost: ShippingCost = new ShippingCost(
    null,
    0,
    null,
    0,
    null,
    null
  );
  allPacketCategories: PacketCategory[] = [];
  packetCategory: PacketCategory = new PacketCategory(
    null,
    null,
    0,
    0,
    0,
    0,
  );
  allPacketCategoriesName: string[] = [];
  catName: string;
  isAsTable: boolean = true;
  loaded: boolean = true;
  isServerDown: boolean = false;
  key: string = 'name';
  reverse: boolean = false;
  serverDownMsg = '';
  isAdmin: boolean = false;
  isCommercial: boolean = false;
  isDeliverer: boolean = false;

  constructor(private router: Router, private modalService: NgbModal,
    private shippingCostService: ShippingCostService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private packetCategoryService: PacketCategoryService,
    private utilityService: UtilityService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {
    this.initData();
    // the click on the search btn
    $('#search-ship-coast').click(() => {
      this.hide = this.hide === true ? false : true;
    });
  }

  getCurrentUserRole() {
    this.isAdmin = this.utilityService.isAdmin;
    this.isCommercial = this.utilityService.isCommercial;
    this.isDeliverer = this.utilityService.isDeliverer;
  }

  initData() {
    this.getCurrentUserRole();
    this.getAllShippingCosts();
    this.currencies = ['FCFA', 'EUR', 'DOLLAR'];
    this.types = [
      'NORMAL',
      'SECURE',
      'INTERNATIONAL'
    ];
    this.getCurrentUrl();
    this.selectAllPacketCategories();
  }

  search() {
    if (this.catName == "") {
      this.ngOnInit();
    } else {
      this.shippingCostList = this.allShippingCost.filter(res => {
        return res.category.name.toLowerCase().match(this.catName.toLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  get categoryName() {
    return (!!this.currentShippingCost.category) ? this.currentShippingCost.category.name : null;
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  private getCurrentUrl() {
    this.currentUrl = this.translate.instant(`global.${this.router.url.substring(1).toLowerCase()}`);
  }

  openModal(content: any) {
    this.modalService.open(content, { centered: true, scrollable: true });
  }

  getShippingCost(shippingCostUuid: string) {
    this.shippingCostService.getShippingCostById(shippingCostUuid).subscribe(
      (data) => {
        if (null != data) {
          this.currentShippingCost = data;
          this.currentShippingCost.category = data.category;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  deleteShippingCost(shippingCostUuid: string) {
    this.shippingCostService.deleteShippingCostById(shippingCostUuid).subscribe(
      data => {
        for (let i = 0; i < this.shippingCostList.length; ++i) {
          if (this.shippingCostList[i].shippingCostUuid === shippingCostUuid) {
            // decremente the value
            this.totalItems--;
            // close modal and notified
            this.modalService.dismissAll();
            // Notify the user
            this.toastr.success(this.translate.instant('shipping-cost.delete-success'), this.translate.instant(('shipping-cost.delete')));
            // remove the deleted user from the list and exit.
            return this.shippingCostList.splice(i, 1);
          }
        }
      },
      err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastr.error(this.translate.instant('shipping-cost.delete-failed'), this.translate.instant('shipping-cost.delete'));
        } else {
          this.isServerDown = true;
          this.toastr.error(this.translate.instant('global.unex_error'));
          console.error(err);
        }
      }
    );
    this.modalService.dismissAll();
  }

  addShippingCost() {
    this.shippingCost.price = this.form.price;
    this.shippingCost.bonusPoints = this.form.bonusPoint;
    var shippingType: string = this.form.shippingType;
    this.shippingCost.shippingType = shippingType.toUpperCase();
    this.shippingCost.currency = this.form.currency;
    for (let i = 0; i < this.allPacketCategories.length; i++) {
      if (this.allPacketCategories[i].name === this.form.category) {
        this.packetCategory = this.allPacketCategories[i];
      }
    }
    this.shippingCost.category = this.packetCategory;

    this.shippingCostService.addShippingCost(this.shippingCost).subscribe(
      data => {
        if (null != data) {
          // refresh the user list
          this.getAllShippingCosts();
          // if new user successful added, close the modal and reload the location
          this.modalService.dismissAll();
          // than notify
          this.toastr.success(this.translate.instant('shipping-cost.create-success'), this.translate.instant('shipping-cost.create'));
        }
      },
      err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastr.error(this.translate.instant('global.error-save'));
        } else {
          this.isServerDown = true;
          console.error(err);
        }
      }
    );
  }

  clearInput(form) {
    form.resetForm();
  }

  updateShippingCost(form) {
    this.currentShippingCost.price = form.value.price;
    this.currentShippingCost.currency = form.value.currency;
    this.currentShippingCost.shippingType = form.value.shippingType;
    this.currentShippingCost.bonusPoints = form.value.bonusPoint;
    for (let i = 0; i < this.allPacketCategories.length; i++) {
      if (this.allPacketCategories[i].name === form.value.PacketCategory) {
        this.packetCategory = this.allPacketCategories[i];
      }
    }
    this.currentShippingCost.category = this.packetCategory;
    this.shippingCostService.updateShippingCostById(this.currentShippingCost).subscribe(
      data => {
        if (null != data) {
          // close the modal window
          this.modalService.dismissAll();
          // refresh the list
          this.getAllShippingCosts();
          // notify the user
          this.toastr.success(this.translate.instant('shipping-cost.update-success'), this.translate.instant('shipping-cost.update'));
        }
      },
      err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
          this.toastr.error(this.translate.instant('shipping-cost.update-failed'), this.translate.instant('shipping-cost.update'));
        } else {
          this.isServerDown = true;
          console.error(err);
        }
      }
    );
  }

  private getAllShippingCosts() {
    this.shippingCostService.getAllShippingCosts().subscribe(
      data => {
        if (null != data) {
          let shippingCostResult: {
            shippingCostUuid: string,
            price: number,
            currency: string,
            bonusPoints: number,
            category: PacketCategory,
            shippingType: string
          }[] = [];
          shippingCostResult = data;
          this.allShippingCost = shippingCostResult.sort((n1, n2) => {
            if (n1.category.maxWeight > n2.category.maxWeight) {
              return 1;
            }

            if (n1.category.maxWeight < n2.category.maxWeight) {
              return -1;
            }

            return 0;
          });

          this.totalItems = shippingCostResult.length;
          // save all coast
          this.shippingCostList = this.allShippingCost;
          // set the flag
          this.loaded = false;
          this.getAllPacketCategoriesInfosForShippingCost(this.allShippingCost);
        }
      },
      err => {
        this.isServerDown = true;
        console.error(err);
      }
    );
  }

  private selectAllPacketCategories() {
    this.packetCategoryService.getAllPacketCategories().subscribe(
      data => {
        if (null != data) {
          this.allPacketCategories = data;
          this.setPacketCategoriesName();
        }
      },
      err => {
        if (!!err.error.message) {
          this.errorMessage = err.error.message;
        } else {
          this.errorMessage = this.translate.instant('global.unex_error');
        }
      }
    );
  }

  private setPacketCategoriesName() {
    for (let i = 0; i < this.allPacketCategories.length; i++) {
      this.allPacketCategoriesName.push(this.allPacketCategories[i].name);
    }
  }

  private getAllPacketCategoriesInfosForShippingCost(allShippingCost) {
    for (let i = 0; i < allShippingCost.length; i++) {
      if (allShippingCost[i].category !== null) {
        this.packetCategoryService.getPacketCategoryById(allShippingCost[i].category.packetCategoryUuid).subscribe(
          data => {
            if (null != data) {
              allShippingCost[i].category = data;
            }
          }, err => {
            console.error(err);
          }
        );
      }
    }
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.catName = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.getAllShippingCosts();
  }

}
