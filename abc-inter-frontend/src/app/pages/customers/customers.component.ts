import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { CustomersService } from '../../services/customers/customers.service';
import { Customer } from '../../models/customer/customer';
import { User } from '../../models/user/user';
import { allCustomerTypes, CustomerType } from '../../models/customer/customerType';
import { TranslateService } from '@ngx-translate/core';
import { Address } from '../../models/address/address';
import { allGenders, Gender } from '../../models/gender/gender';
import * as $ from 'jquery';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],
  animations: [
    trigger('searchClicked', [
      state('true', style({
        color: 'white'
      })),
      state('false', style({
        color: 'white'
      })),
      transition('true => false', [
        style({ opacity: 0, transform: 'translateX(-200px)' }),
        animate(
          '300ms ease-in',
          style({ opacity: 1, transform: 'translateX(0)' })
        )
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class CustomersComponent implements OnInit {

  currentUrl: string;
  customers: Array<Customer>;
  // list of all users
  customerList = [];
  customer: Customer;
  totalItems: number;
  page: Number = 1;
  errorMessage = '';
  customerTypes: Array<CustomerType>;
  customerType: CustomerType;
  isTypeChange = false;
  gender: string;
  genders = Array<Gender>();
  isGenderChange = false;
  host = `${environment.apiUrl}/user/avatar/`;
  loaded = true;
  isServerDown = false;
  hide = true;
  firstname: string;
  isAsTable = true;
  key = 'username';
  reverse = false;
  serverDownMsg = '';
  isAdmin = false;

  isCommercial = false;

  isDeliverer = false;
  telOptions = { initialCountry: 'cm', onlyCountries: ['cm'] };
  isCustomerPhoneValid: Boolean;
  errors = {
    phoneNb: '',
    errorMessage: ''
  };
  fixPhonePattern = '^[26][0-9]{8}$';
  emailPattern = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';

  public country: string;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private toastService: ToastrService,
    private customersService: CustomersService,
    private translate: TranslateService,
    private utilityService: UtilityService
  ) {
    this.serverDownMsg = this.translate.instant('global.server-down');
  }

  ngOnInit(): void {
    this.currentUrl = this.translate.instant('customers.' + this.router.url.substr(1));
    // customers list initialisation
    this.initData();
    // the click on the search btn
    $('#search-customer').click(() => {
      this.hide = this.hide === true ? false : true;
    });
  }

  getCurrentUserRole() {
    this.isAdmin = this.utilityService.isAdmin;
    this.isCommercial = this.utilityService.isCommercial;
    this.isDeliverer = this.utilityService.isDeliverer;
  }

  initData() {
    this.getCurrentUserRole();

    this.customersService.findAllCustomers().subscribe((customers) => {
      if (null != customers) {
        this.customers = customers;
        this.totalItems = customers.length;
        // save all user
        this.customerList = this.customers;
        // set the flag
        this.loaded = false;
        this.populateCustomerTypeList();
        this.populateGenderList();
      }
    }, err => {
      this.isServerDown = true;
      console.error(err);
    });
  }

  search() {
    if (this.firstname === '') {
      this.initData();
    } else {
      this.customerList = this.customers.filter(res => {
        return res.user.firstname?.toLocaleLowerCase().match(this.firstname?.toLocaleLowerCase());
      });
    }
  }

  sort(key: string) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getCustomer(id: string) {
    // clear errorMessage
    this.errorMessage = '';
    if (null != id) {
      this.customer = new Customer();
      this.customer.user = new User(null, null, null, null, null, null, null, null, null, null, null);
      this.customer.postalAddress = new Address('', '', '', '', null, 0, 0);
      this.customersService.findCustomerById(id).subscribe((customer) => {
        if (null != customer) {
          this.customer = customer;
          if (this.customer.postalAddress == null) {
            this.customer.postalAddress = new Address('', '', '', '', null, 0, 0);
          }
          this.customer.customerType = this.translate.instant('customers.' + customer.customerType);
          this.customerType = this.customer.customerType;
          this.customer.user.gender = this.translate.instant('global.' + this.customer.user.gender?.toLowerCase());
          this.gender = this.customer.user.gender;
        }
      });
    } else {
      this.customer = new Customer();
      this.customer.user = new User(null, null, null, null, null, null, null, null, null, null, null);
      this.customer.postalAddress = new Address(null, null, null, null, null, 0, 0);
    }
  }

  addNewCustomer() {
    // set new values
    if (this.gender?.toLocaleLowerCase() === 'masculin' || this.gender?.toLocaleLowerCase() === 'male' ||
        this.gender?.toLocaleLowerCase() === 'global.male') {
      this.customer.user.gender = Gender.Male;
    } else if (this.gender?.toLocaleLowerCase() === 'feminin' || this.gender?.toLocaleLowerCase() === 'female' ||
               this.gender?.toLocaleLowerCase() === 'global.female') {
      this.customer.user.gender = Gender.Female;
    } else {
      this.customer.user.gender = Gender.Others;
    }

    if (this.isTypeChange) {
      this.customer.customerType = this.customerType;
    }
    // if type equal prive than change to private to avoid error
    const type = this.customer.customerType;
    if (type.includes('PRIVE')) {
      this.customer.customerType = CustomerType.Private;
    }
    this.customersService.addCustomer(this.customer).subscribe((res) => {
      if (null != res) {
        // refresh the user list
        this.initData();
        // if new user successful added, close the modal and reload the location
        this.modalService.dismissAll();
        // than notify
        this.toastService.success(this.translate.instant('customers.new-customer-add'));
      }
    }, (err) => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(this.translate.instant('global.error-save'));
        // needed to display the old value of customer type when error happened.
        this.customer.customerType = this.translate.instant('customers.' + this.customer.customerType);
        this.customer.user.gender = this.translate.instant('global.' + this.customer.user.gender);
      } else {
        this.isServerDown = true;
      }
    });
  }

  updateCustomer() {
    // if type equal prive (fr) than change to private to avoid error
    const type = this.customer.customerType;
    if (type.includes('PRIVEE')) {
      this.customer.customerType = CustomerType.Private;
    }
    
    if (this.gender?.toLocaleLowerCase() === 'masculin' || this.gender?.toLocaleLowerCase() === 'male' ||
        this.gender?.toLocaleLowerCase() === 'global.male') {
      this.customer.user.gender = Gender.Male;
    } else if (this.gender?.toLocaleLowerCase() === 'feminin' || this.gender?.toLocaleLowerCase() === 'female' ||
               this.gender?.toLocaleLowerCase() === 'global.female') {
      this.customer.user.gender = Gender.Female;
    } else {
      this.customer.user.gender = Gender.Others;
    }

    this.customersService.updateCustomer(this.customer).subscribe((res) => {
      if (null != res) {
        // close the modal window
        this.modalService.dismissAll();
        // refresh the list
        this.initData();
        // notify the user
        this.toastService.success(this.customer.user.username + ' ' + this.translate.instant('customers.customer-update'));
      }
    }, (err) => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(this.translate.instant('customers.error-update'));
        this.customer.customerType = this.translate.instant('customers.' + this.customer.customerType);
        this.customer.user.gender = this.translate.instant('global.' + this.customer.user.gender?.toLowerCase());
      } else {
        this.isServerDown = true;
      }
    });
  }

  deleteCustomer() {
    this.customersService.deleteCustomerById(this.customer.customerUuid).subscribe((res) => {
      for (let i = 0; i < this.customerList.length; ++i) {
        if (this.customerList[i].user.userUuid === this.customer.user.userUuid) {
          // decremente the value
          this.totalItems--;
          // close modal and notified
          this.modalService.dismissAll();
          // Notify the user
          this.toastService.success(this.customer.user.username + ' ' + this.translate.instant('customers.success-del'));
          // remove the deleted user from the list and exit.
          return this.customerList.splice(i, 1);
        }
      }
    }, err => {
      if (!!err.error.message) {
        this.errorMessage = err.error.message;
        this.toastService.error(this.customer.user.username + ' ' + this.translate.instant('customers.success-del'));
        this.customer.customerType = this.translate.instant('customers.' + this.customer.customerType);
        this.customer.user.gender = this.translate.instant('global.' + this.customer.user.gender?.toLowerCase());
      } else {
        this.isServerDown = true;
      }
    });
    this.modalService.dismissAll();
  }

  isDefaultAvatar(user: User): boolean {
    return user.profileImg === null || user.profileImg.length === 0;
  }

  populateCustomerTypeList() {
    this.customerTypes = allCustomerTypes.map(type => this.translate.instant('' + type));
  }

  populateGenderList() {
    this.genders = allGenders.map(gender => this.translate.instant('' + gender));
  }

  onTypeChange(event) {
    const value = event.target.value;
    if (value === 'ANONYM') {
      this.customerType = CustomerType.Anonym;
    } else if (value === 'BUSINESS') {
      this.customerType = CustomerType.Business;
    } else if (value === 'PRIVEE' || value === 'PRIVATE') {
      this.customerType = CustomerType.Private;
    }
    this.isTypeChange = true;
  }

  onGenderChange(event) {
    const value = event.target.value;
    if (value === 'Masculin' || value === 'Male') {
      this.gender = Gender.Male;
    } else if (value === 'Feminin' || value === 'Female') {
      this.gender = Gender.Female;
    } else {
      this.gender = Gender.Others;
    }
    this.isGenderChange = true;
  }

  openModal(content) {
    this.modalService.open(content, { centered: true, scrollable: true, size: 'xl' });
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }

  clearErrorMessage() {
    if (this.errorMessage.length > 0) {
      this.errorMessage = '';
    }
  }

  displayAsList() {
    this.isAsTable = true;
  }

  displayAsCard() {
    this.isAsTable = false;
  }

  refresh() {
    this.isServerDown = false;
    // clear the input
    this.firstname = '';
    // and hide the searchbar
    this.hide = true;
    // reset the list
    this.initData();
  }

  checkValidPhoneNumber(event) {
    const reg = this.utilityService.regPortableNumber;
    this.checkCustomerPhone(reg.test(event.target.value));
  }

  // returns true if phone number is valid otherwise false.

  checkCustomerPhone(isValid) {
    this.isCustomerPhoneValid = isValid;
    if (this.isCustomerPhoneValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  preventInput(event, field) {
    let value = null;
    if (field.name === 'latitude') {
      value = this.customer.postalAddress.latitude;
      if (value >= 99.999999) {
        event.preventDefault();
        this.customer.postalAddress.latitude = parseInt(value.toString().substring(0, 2));
      }
    }
    if (field.name === 'longitude') {
      value = this.customer.postalAddress.longitude;
      if (value >= 99.999999) {
        event.preventDefault();
        this.customer.postalAddress.longitude = parseInt(value.toString().substring(0, 2));
      }
    }
  }
}
