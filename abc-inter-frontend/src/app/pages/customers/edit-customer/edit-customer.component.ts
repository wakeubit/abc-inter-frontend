import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Customer } from 'src/app/models/customer/customer';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { RegistrationService } from 'src/app/services/registration/registration.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {

  currentUrl: string;

  customerToSave: Customer;

  isPhoneValid: boolean = false;

  isCustomerToUpdate: boolean = false;

  errors = {
    errorMsg: ''
  };

  constructor(
    private customerService: CustomersService,
    private router: Router,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private registrationService: RegistrationService,
    private activatedroute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.currentUrl = this.router.url.substr(1).split('/')[1];
  }

  getCustomer(form) {
    this.customerToSave = form.customer;
    this.isPhoneValid = form.phoneStatus;
    this.isCustomerToUpdate = form.isCustomer;
  }

  isFormValid(): boolean {
    if (
      !this.isPhoneValid ||
      this.customerToSave.customerType?.length === 0 ||
      this.customerToSave.user.username?.length === 0 ||
      this.customerToSave.user.firstname?.length === 0 ||
      this.customerToSave.user.lastname?.length === 0 ||
      this.customerToSave.user.gender?.length === 0 ||
      this.customerToSave.user.password?.length === 0 ||
      this.customerToSave.postalAddress?.city.length === 0 ||
      this.customerToSave.postalAddress?.country.length === 0 ||
      this.customerToSave.postalAddress?.streetAndHouseNumber?.length === 0
    ) {
      return false;
    }
    return true;
  }

  saveCustomer() {
    if (this.isFormValid() && this.customerToSave) {
      if (this.isCustomerToUpdate) {
        this.customerService.updateCustomer(this.customerToSave).subscribe(result => {
          this.errors.errorMsg = '';
          this.toastrService.success(this.translateService.instant('global.update-success'));
        });
      } else {
        // first fetch the registration
        this.activatedroute.paramMap.subscribe(params => {
          this.registrationService.findById(params.get('id')).subscribe(registration => {
            if (registration) {
              // check whether it is a new customer to be saved or not
              if (this.customerToSave.customerUuid === null) {
                this.customerService.addCustomer(this.customerToSave).subscribe(result => {
                  if (result) {
                    registration.sender = result;
                    this.registrationService.updateRegistration(registration).subscribe(updatedReg => {
                      if (updatedReg) {
                        this.toastrService.success(this.translateService.instant('global.update-success'));
                      }
                    }, err => {
                      this.toastrService.success(this.translateService.instant('global.update-error'));
                      console.error(err?.error?.message);
                    });
                  }
                }, err => {
                  console.error(err?.error?.message);
                  if (err?.error.message.includes('taken')) {
                    this.errors.errorMsg = this.translateService.instant('global.username_taken');
                  } else if (err?.error.message.includes('Email')) {
                    this.errors.errorMsg = this.translateService.instant('global.email_in_use');
                  }
                });
              } else {
                // otherwise we update the value of the sender and assign it to the registration
                this.customerService.updateCustomer(this.customerToSave).subscribe(result => {
                  if(result) {
                    registration.sender = this.customerToSave;
                    this.registrationService.updateRegistration(registration).subscribe(updatedReg => {
                      if (updatedReg) {
                        this.toastrService.success(this.translateService.instant('global.update-success'));
                      }
                    }, err => {
                      this.toastrService.error(this.translateService.instant('global.update-error'));
                      console.error(err?.error?.message);
                    });
                  }
                }, err => {
                  console.error(err?.error?.message);
                  if (err?.error.message.includes('taken')) {
                    this.errors.errorMsg = this.translateService.instant('global.username_taken');
                  } else if (err?.error.message.includes('Email')) {
                    this.errors.errorMsg = this.translateService.instant('global.email_in_use');
                  }
                });
              }
            }
          });
        });
      }
    } else {
      this.toastrService.error(this.translateService.instant('global.update-error'));
      this.errors.errorMsg = this.translateService.instant('global.required-fields');
    }
  }

  navigateToHome() {
    this.router.navigate(['/dashboard']);
  }
}
