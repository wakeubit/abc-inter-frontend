/**
 * Define a set of actions that can be done by a specific user
 */

import { TranslateService } from "@ngx-translate/core";
import { RoleType } from "src/app/models/role/RoleType";

interface Actions {
  name: string,
  roles: string[],
  action: string,
  status: string
}

export class Action {
  constructor(
    private translateService: TranslateService
  ) { }

  public getActions(): Actions[] {
    return [
      // * => action can be done by all
      /*{
        'name': this.translateService.instant('actions.add'),
        roles: [RoleType.Deliverer], action: 'add_to_my_task',
        status: 'todo'
      },*/
      {
        'name': this.translateService.instant('actions.delete'),
        roles: [RoleType.Admin, RoleType.Commercial],
        action: 'delete',
        status: 'todo'
      },
      {
        'name': this.translateService.instant('actions.assign'),
        roles: [RoleType.Admin, RoleType.Commercial],
        action: 'assign',
        status: 'todo'
      },
      // { 'name': 'To another location', roles: ['deliverer'], action: 'change_location', status: 'todo' },
      {
        'name': this.translateService.instant('actions.deliver'),
        roles: [RoleType.Deliverer],
        action: 'deliver',
        status: 'delivery'
      },
      {
        'name': this.translateService.instant('actions.remove'),
        roles: [RoleType.Admin, RoleType.Commercial],
        action: 'return',
        status: 'delivery'
      }
    ];
  }
}
