const DataSubject = {
  "notification": {
    "taskAdded": false,
    "taskRemoved": false
  },
  "newPacketRegistration": null,
};

export { DataSubject };
