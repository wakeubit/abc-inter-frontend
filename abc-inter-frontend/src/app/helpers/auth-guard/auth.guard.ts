import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanActivateChild
} from '@angular/router';
import { TokenStorageService } from '../../services/jwt/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(
    private tokenStorage: TokenStorageService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const user = this.tokenStorage.currentUser();
    if (user) {
      // check if route is restricted by role
      if (user.roles && route.data.roles && route.data.roles.some(item => user.roles.includes(item))) {
        return true;
      }
      // role not authorized so redirect to unauthorized
      this.router.navigate(['/unauthorized']);
      return false;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const user = this.tokenStorage.currentUser();
    if (user) {
      // check if route is restricted by role
      if (user.roles && next.data.roles && next.data.roles.some(item => user.roles.includes(item))) {
        // role is authorize
        return true;
      }
      // role not authorised so redirect to unauthorized
      this.router.navigate(['/unauthorized']);
      return false;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
