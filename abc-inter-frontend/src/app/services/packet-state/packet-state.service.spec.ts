import { TestBed } from '@angular/core/testing';

import { PacketStateService } from './packet-state.service';

describe('PacketStateService', () => {
  let service: PacketStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PacketStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
