import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Packet } from '../../models/packet/packet';
import { Observable } from 'rxjs';
import { PacketStateGroup } from 'src/app/models/packet-state-group/packet-state-group';
import { environment } from 'src/environments/environment';
import { PacketState } from 'src/app/models/packet-state/packetState';
import { PacketTracking } from 'src/app/models/packet-tracking/packet-tracking';

const PACKET_STATUS_GROUP_URL = environment.apiUrl + '/packet-states-group';
const PACKET_STATUS_URL = environment.apiUrl + '/packet-states';
const PACKET_URL = environment.apiUrl + '/packets';

const URL = environment.apiUrl + '/packets';

@Injectable({
  providedIn: 'root'
})
export class PacketStateService {

  constructor(
    private http: HttpClient
  ) { }

  findAllPacketStateByPck(): Observable<Packet[]> {
    return this.http.get<Packet[]>(URL);
  }

  findPacketById(id: string): Observable<Packet> {
    return this.http.get<Packet>(`${URL}/${id}`);
  }

  findPacketStatesByTrackingNumber(trackingNumber: string): Observable<PacketState[]> {
    return this.http.get<PacketState[]>(`${PACKET_STATUS_URL}/${trackingNumber}`);
  }

  findPacketStatesByGlobalTrackingNumber(globalTrackingNumber: string): Observable<PacketTracking> {
    return this.http.get<PacketTracking>(`${PACKET_STATUS_GROUP_URL}/${globalTrackingNumber}`);
  }

  findPacketStatesByPacketUuid(packetUuid: string): Observable<PacketState[]> {
    return this.http.get<PacketState[]>(`${PACKET_URL}/${packetUuid}/packet-states`);
  }

  createPacketStateGroup(packetStateGroup: PacketStateGroup): Observable<any> {
    return this.http.post<Packet>(`${PACKET_STATUS_GROUP_URL}`, packetStateGroup);
  }
}
