import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { Html5Qrcode } from 'html5-qrcode';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  // constructor() {}
  private scannerSource = new BehaviorSubject<Html5Qrcode>(null);
  public currentScanner = this.scannerSource.asObservable();
  /**Changes the current instance */
  public changeScannerInstance(instance: Html5Qrcode) {
    this.scannerSource.next(instance);
  }
}
