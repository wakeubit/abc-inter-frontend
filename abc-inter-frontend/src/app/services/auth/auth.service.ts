import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TokenStorageService } from '../jwt/token-storage.service';
import { environment } from 'src/environments/environment';

const AUTH_API_URL = environment.apiUrl + '/api/auth/';

const HTTPOPTIONS = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private tokenStorage: TokenStorageService
  ) {
  }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API_URL + 'signin', {
      username: credentials.username,
      password: credentials.password
    }, HTTPOPTIONS).pipe(map(user => {
      // Store the user details and token to keep user logged in between page refreshes
      this.tokenStorage.saveUser(user);
      return user;
    }));
  }

  register(user): Observable<any> {
    return this.http.post(AUTH_API_URL + 'signup', {
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      tel: user.tel,
      gender: user.gender,
      password: user.password
    }, HTTPOPTIONS);
  }

  refreshToken(token: string) {
    return this.http.post(AUTH_API_URL + 'refreshtoken', {
      refreshToken: token
    }, HTTPOPTIONS);
  }

  logOut() {
    this.tokenStorage.clear();
    this.router.navigate(['login']);
    location.reload();
  }
}
