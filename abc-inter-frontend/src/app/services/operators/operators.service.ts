import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Operator } from '../../models/operator/operator';
import { TokenStorageService } from '../jwt/token-storage.service';
import { environment } from 'src/environments/environment';

const OPERATOR_URL = environment.apiUrl + '/operators';

@Injectable({
  providedIn: 'root'
})
export class OperatorsService {

  constructor(
    private http: HttpClient,
    private tokenstorage: TokenStorageService
  ) {
  }

  getAllOperators(): Observable<Operator[]> {
    return this.http.get<Operator[]>(OPERATOR_URL);
  }

  createOperator(operator: Operator) {
    return this.http.post<Operator>(OPERATOR_URL, {
      user: {
        userUuid: operator.user.userUuid,
        username: operator.user.username,
        password: operator.user.password,
        firstname: operator.user.firstname,
        lastname: operator.user.lastname,
        email: operator.user.email,
        tel: operator.user.tel,
        gender: operator.user.gender,
        roles: [
          {
            roleUuid: operator.user.roles[0]['roleUuid'],
            name: operator.user.roles[0]['name'],
            description: operator.user.roles[0]['description']
          }
        ]
      },
      operatorType: operator.operatorType,
      personalNumber: operator.personalNumber,
      operatorUuid: operator.operatorUuid = null
    });
  }

  getOperatorById(operatorUuid: string): Observable<Operator> {
    return this.http.get<Operator>(`${OPERATOR_URL}/${operatorUuid}`);
  }

  updateOperator(operator: Operator) {
    return this.http.put<Operator>(`${OPERATOR_URL}/${operator.operatorUuid}`, operator);
  }

  deleteOperatorById(id: string) {
    return this.http.delete(`${OPERATOR_URL}/${id}`);
  }
}
