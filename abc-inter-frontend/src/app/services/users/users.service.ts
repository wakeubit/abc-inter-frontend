import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user/user';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Customer } from '../../models/customer/customer';

const USER_URL = environment.apiUrl + '/user';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  addOne(user: User): Observable<User> {
    return this.http.post<User>(USER_URL, {
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      tel: user.tel,
      gender: user.gender,
      profileImg: user.profileImg,
      password: user.password,
    });
  }

  update(formData: FormData): Observable<any> {
    return this.http.post<User>(`${USER_URL}/image`, formData);
  }

  findById(id: string): Observable<User> {
    return this.http.get<User>(`${USER_URL}/${id}`);
  }

  updateById(user) {
    return this.http.put<User>(`${USER_URL}/${user.userUuid}`, user);
  }

  deleteOne(id: string): Observable<User> {
    return this.http.delete<User>(`${USER_URL}/${id}`);
  }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(USER_URL);
  }

  getCustomerByUserUuid(userUuid: string): Observable<Customer> {
    return this.http.get<Customer>(`${USER_URL}/${userUuid}/customers`);
  }
}
