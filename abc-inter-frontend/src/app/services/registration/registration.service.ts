import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Registration } from 'src/app/models/registration/registration';
import { Observable } from 'rxjs';
import { Packet } from 'src/app/models/packet/packet';
import { environment } from 'src/environments/environment';
import { PageResult } from 'src/app/models/packet/page-result';

const URL = environment.apiUrl + '/registrations';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  addRegistration(registration: Registration): Observable<Registration> {
    return this.http.post<Registration>(URL, registration);
  }

  updateRegistration(registration: Registration): Observable<Registration> {
    return this.http.put<Registration>(`${URL}/${registration.registrationUuid}`, registration);
  }

  updatePacketFromRegistration(registrationUuid: string, packet: Packet): Observable<Registration> {
    return this.http.put<Registration>(`${URL}/${registrationUuid}/packets/${packet.packetUuid}`, packet);
  }

  deletePacketFromRegistration(registrationUuid: string, packetUuid: string): Observable<Registration> {
    return this.http.delete<Registration>(`${URL}/${registrationUuid}/packets/${packetUuid}`);
  }

  findAll(page?: number, size?: number): Observable<PageResult<Registration>> {
    let queryParams = new HttpParams();
    if (page) {
      queryParams = queryParams.append("page", page);
    }
    if (size) {
      queryParams = queryParams.append("size", size);
    }
    return this.http.get<PageResult<Registration>>(URL, { params: queryParams });
  }

  findAllByStatus(status: string, page?: number, size?: number): Observable<PageResult<Registration>> {
    let queryParams = new HttpParams();
    if (status) {
      queryParams = queryParams.append("status", status);
    }
    if (page) {
      queryParams = queryParams.append("page", page);
    }
    if (size) {
      queryParams = queryParams.append("size", size);
    }
    return this.http.get<PageResult<Registration>>(`${URL}/filteredByStatus`, { params: queryParams });
  }

  findAllByAssignedTo(assignedTo: string, page?: number, size?: number): Observable<PageResult<Registration>> {
    let queryParams = new HttpParams();
    if (page) {
      queryParams = queryParams.append("page", page);
    }
    if (size) {
      queryParams = queryParams.append("size", size);
    }
    return this.http.get<PageResult<Registration>>(`${URL}/assignedTo/${assignedTo}`, { params: queryParams });
  }

  findAllContainingGlobalTrackingNumber(globalTrackingNumber: string, page?: number, size?: number): Observable<PageResult<Registration>> {
    let queryParams = new HttpParams();
    if (globalTrackingNumber) {
      queryParams = queryParams.append("globalTrackingNumber", globalTrackingNumber);
    }
    if (page) {
      queryParams = queryParams.append("page", page);
    }
    if (size) {
      queryParams = queryParams.append("size", size);
    }
    return this.http.get<PageResult<Registration>>(`${URL}/search`, { params: queryParams });
  }

  findById(registrationUuid: string): Observable<Registration> {
    return this.http.get<Registration>(`${URL}/${registrationUuid}`);
  }

  deleteById(registrationUuid: string): Observable<Registration> {
    return this.http.delete<Registration>(`${URL}/${registrationUuid}`);
  }
}
