import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Customer} from '../../models/customer/customer';
import { environment } from 'src/environments/environment';
import {map} from 'rxjs/operators';

const URL = environment.apiUrl + '/customers';
@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(
    private http: HttpClient
  ) { }

  findAllCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(URL);
  }

  findCustomerById(id: string): Observable<Customer> {
    return this.http.get<Customer>(`${URL}/${id}`);
  }

  addCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(URL, customer);
  }

  updateCustomer(customer: Customer): Observable<Customer> {
    return this.http.put<Customer>(`${URL}/${customer.customerUuid}`, customer);
  }

  deleteCustomerById(customerUuid: string): Observable<Customer> {
    return this.http.delete<Customer>(`${URL}/${customerUuid}`);
  }
}
