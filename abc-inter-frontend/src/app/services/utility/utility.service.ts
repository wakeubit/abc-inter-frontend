import {Injectable} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {User} from '../../models/user/user';
import {TokenStorageService} from '../jwt/token-storage.service';
import {UserType} from '../../models/user/userType';
import {BehaviorSubject} from 'rxjs';
import {RoleType} from '../../models/role/RoleType';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  private dataSubject = new BehaviorSubject<object>(new Object());
  currentData = this.dataSubject.asObservable();
  user: User;
  regPortableNumber = new RegExp('^(6)[0-9]{8}$', 'i');
  regFixOrPortableNumber = new RegExp('^[26][0-9]{8}$', 'i');

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService
  ) {
    this.user = tokenStorage.currentUser();
  }

  getUser() {
    return this.tokenStorage.currentUser();
  }

  get userId() {
    return this.user?.userUuid;
  }

  get username() {
    return this.user?.username;
  }

  get isCustomer() {
    return this.user && this.roleNames.includes(RoleType.Customer) &&
      !this.roleNames.includes(RoleType.Deliverer) &&
      !this.roleNames.includes(RoleType.Storekeeper) &&
      !this.roleNames.includes(RoleType.Admin);
  }

  get isDeliverer() {
    return this.user && this.roleNames.includes(RoleType.Deliverer) &&
      !this.roleNames.includes(RoleType.Storekeeper) &&
      !this.roleNames.includes(RoleType.Admin) &&
      !this.roleNames.includes(RoleType.Customer);
  }

  get isStorekeeper() {
    return this.user && this.roleNames.includes(RoleType.Storekeeper) &&
      !this.roleNames.includes(RoleType.Deliverer) &&
      !this.roleNames.includes(RoleType.Admin) &&
      !this.roleNames.includes(RoleType.Customer);
  }

  get isCommercial() {
    return this.user && this.roleNames.includes(RoleType.Commercial) &&
      !this.roleNames.includes(RoleType.Deliverer) &&
      !this.roleNames.includes(RoleType.Admin) &&
      !this.roleNames.includes(RoleType.Customer);
  }

  get isAdmin() {
    return this.user && this.roleNames.includes(RoleType.Admin);
  }

  get roleNames(): string[] {
    let result = [];
    this.user.roles.forEach(roleName => {
      result.push(roleName);
    });
    return result;
  }

  getUserType(): string {
    if (this.user && this.roleNames.includes(RoleType.Admin)) {
      return UserType.Admin;
    } else if (
      this.user && this.roleNames.includes(RoleType.Deliverer) ||
      this.user && this.roleNames.includes(RoleType.Storekeeper) ||
      this.user && this.roleNames.includes('ROLE_OPERATOR') ||
      this.user && this.roleNames.includes(RoleType.Commercial)
    ) {
      return UserType.Operator;
    } else if (
      this.user && this.roleNames.includes(RoleType.Customer)
    ) {
      return UserType.Customer;
    }
  }

  getPrincipalRole(): string {
    if (this.isCustomer) {
      return RoleType.Customer;
    } else if (this.isDeliverer) {
      return RoleType.Deliverer;
    } else if (this.isStorekeeper) {
      return RoleType.Storekeeper;
    } else if (this.isAdmin) {
      return RoleType.Admin;
    } else if (this.isCommercial) {
      return RoleType.Commercial;
    }
  }

  // useful when user username changed
  set username(username: string) {
    this.tokenStorage.setUsername(username);
  }

  changeDataSubject(newData) {
    this.dataSubject.next(newData);
  }

  public logout() {
    this.authService.logOut();
  }
}
