import { TestBed } from '@angular/core/testing';

import { PacketStoreService } from './packet-store.service';

describe('PacketStoreService', () => {
  let service: PacketStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PacketStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
