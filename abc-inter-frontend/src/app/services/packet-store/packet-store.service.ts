import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PacketStore } from 'src/app/models/packet-store/packet-store';
import { environment } from 'src/environments/environment';


const URL = environment.apiUrl + '/packetstores';

@Injectable({
  providedIn: 'root'
})
export class PacketStoreService {

  constructor(
    private http: HttpClient
  ) { }

  findAllStore(): Observable<PacketStore[]> {
    return this.http.get<PacketStore[]>(URL);
  }

  findStoreById(id: string): Observable<PacketStore> {
    return this.http.get<PacketStore>(`${URL}/${id}`);
  }

  addStore(packetStore: PacketStore): Observable<PacketStore> {
    return this.http.post<PacketStore>(URL, packetStore);
  }

  updateStore(packetStore: PacketStore): Observable<PacketStore> {
    return this.http.put<PacketStore>(`${URL}/${packetStore.packetStoreUuid}`, packetStore);
  }

  deleteStoreById(packetStoreUuid: string): Observable<PacketStore> {
    return this.http.delete<PacketStore>(`${URL}/${packetStoreUuid}`);
  }
}
