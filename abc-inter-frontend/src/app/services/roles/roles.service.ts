import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Role} from '../../models/role/role';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';

const URL = environment.apiUrl + '/roles';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(
    private http: HttpClient
  ) { }

  findAllRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(URL);
  }

  findRoleById(id: string): Observable<Role> {
    return this.http.get<Role>(`${URL}/${id}`);
  }

  deleteRoleById(roleUuid: string): Observable<Role> {
    return this.http.delete<Role>(`${URL}/${roleUuid}`);
  }

  updateRole(role: Role): Observable<Role> {
    return this.http.put<Role>(`${URL}/${role.roleUuid}`, role);
  }

  addRole(role: Role): Observable<Role> {
    return this.http.post<Role>(URL, role);
  }
}
