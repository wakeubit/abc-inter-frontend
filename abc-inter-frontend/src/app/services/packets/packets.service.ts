import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpParams } from '@angular/common/http';

const PACKETS_URL = `/packets`;
const PACKETS_STATUS_GROUP = `/packet-states-group`;

const HTTPOPTIONS = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

import {Packet} from '../../models/packet/packet';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageResult } from 'src/app/models/packet/page-result';

const URL = environment.apiUrl + '/packets';
@Injectable({
  providedIn: 'root'
})
export class PacketsService {

  constructor(
    private http: HttpClient
  ) { }

  findAllPackets(page?: number, size?: number): Observable<PageResult<Packet>> {
    let queryParams = new HttpParams();
    if (page) {
      queryParams = queryParams.append("page", page);
    }
    if (size) {
      queryParams = queryParams.append("size", size);
    }
    return this.http.get<PageResult<Packet>>(URL, {params: queryParams});
  }

  searchAllPacketsContainingTrackingNumber(trackingNumber: string, page?: number, size?: number): Observable<PageResult<Packet>> {
    let queryParams = new HttpParams();
    if (trackingNumber) {
      queryParams = queryParams.append("trackingNumber", trackingNumber);
    }
    if (page) {
      queryParams = queryParams.append("page", page);
    }
    if (size) {
      queryParams = queryParams.append("size", size);
    }
    return this.http.get<PageResult<Packet>>(`${URL}/search`, {params: queryParams});
  }

  findPacketById(id: string): Observable<Packet> {
    return this.http.get<Packet>(`${URL}/${id}`);
  }

  addPacket(form): Observable<Packet> {
    return this.http.post<Packet>(URL, form);
  }

  updatePacket(packet: Packet): Observable<Packet> {
    return this.http.put<Packet>(`${URL}/${packet.packetUuid}`, packet);
  }

  deletePacketById(id: string): Observable<Packet> {
    return this.http.delete<Packet>(`${URL}/${id}`);
  }

  // returns the list of the packets that already have been delivered
  findPacketsDelivered() {
    return this.http.get<any[]>(`${PACKETS_URL}/delivered`);
  }

  // returns the list of packets to be delivered
  findPacketsToBeDelivered() {
    return this.http.get<any[]>(`${PACKETS_URL}/to_be_delivered`);
  }

  // Returns the list of the packets that have to be delivered
  findPacketsReadyToBeDelivered() {
    return this.http.get<any[]>(`${PACKETS_URL}/ready_for_delivery`);
  }

  /**Gets the list of all packets in store */
  findPacketsInStore() {
    return this.http.get<any[]>(`${PACKETS_URL}/in_store`);
  }

  /**
   * Update the status one or many packets.
   * @param reqBody Json object containing all packets id, the new status and the operator who has updated the status.
   */
  updatePacketStatus(reqBody) {
    return this.http.post(`${PACKETS_STATUS_GROUP}/update`, {
      packets: reqBody.packets,
      statusName: reqBody.statusName,
      modifiedBy: reqBody.modifiedBy,
      deliveryDetails: reqBody.deliveryDetails
    }, HTTPOPTIONS);
  }
}
