import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PacketCategory} from '../../models/packet-category/packetCategory';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';

const PACKETCATEGORY = environment.apiUrl + '/packet-categories';

@Injectable({
  providedIn: 'root'
})
export class PacketCategoryService {

  constructor(private http: HttpClient) { }

  createPacketCategory(packetCategory: PacketCategory) {
    return this.http.post<PacketCategory>(PACKETCATEGORY, packetCategory);
  }

  getAllPacketCategories(): Observable<PacketCategory[]> {
    return this.http.get<PacketCategory[]>(PACKETCATEGORY);
  }

  deletePacketCategoryById(packetCategoryUuid: string) {
    return this.http.delete(`${PACKETCATEGORY}/${packetCategoryUuid}`);
  }

  getPacketCategoryById(packetCategoryUuid: string) {
    return this.http.get<PacketCategory>(`${PACKETCATEGORY}/${packetCategoryUuid}`);
  }

  updateCategoryById(packetCategory: PacketCategory) {
    return this.http.put<PacketCategory>(`${PACKETCATEGORY}/${packetCategory.packetCategoryUuid}`, packetCategory);
  }
}
