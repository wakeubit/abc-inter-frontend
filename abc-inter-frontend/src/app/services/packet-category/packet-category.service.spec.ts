import { TestBed } from '@angular/core/testing';

import { PacketCategoryService } from './packet-category.service';

describe('PacketCategoryService', () => {
  let service: PacketCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PacketCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
