import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Delivery} from '../../models/delivery/delivery';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';

const URL = environment.apiUrl + '/delivrances';

@Injectable({
  providedIn: 'root'
})
export class DeliveriesService {

  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<Delivery[]>(URL);
  }

  findById(id: string): Observable<Delivery> {
    return this.http.get<Delivery>(`${URL}/${id}`);
  }

  updateById(delivery: Delivery) {
    return this.http.put<Delivery>(`${URL}/${delivery.delivranceUuid}`, delivery);
  }

  deleteOne(id: string) {
    return this.http.delete<Delivery>(`${URL}/${id}`);
  }

  /*addOne(delivery: Delivery): Observable<Delivery> {
    return this.http.post<Delivery>(URL, {
      deliverBy: {
        operatorType: delivery.deliverBy.operatorType,
        personalNumber: delivery.deliverBy.personalNumber,
        operatorUuid: delivery.deliverBy.operatorUuid,
        user: {
          userUuid: delivery.deliverBy.user.userUuid,
          username: delivery.deliverBy.user.username,
          password: delivery.deliverBy.user.password,
          tokenExpired: delivery.deliverBy.user.tokenExpired,
          firstname: delivery.deliverBy.user.firstname,
          lastname: delivery.deliverBy.user.lastname,
          email: delivery.deliverBy.user.email,
          tel: delivery.deliverBy.user.tel,
          gender: delivery.deliverBy.user.gender,
          profileImg: delivery.deliverBy.user.profileImg
        }
      },
      delivranceDetails: delivery.delivranceDetails,
      // deliveredPackets: delivery.deliveredPackets
    });
  }*/

  addOne(delivery: Delivery): Observable<Delivery> {
    return this.http.post<Delivery>(URL, delivery);
  }
}
