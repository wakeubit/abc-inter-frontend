import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';

const USER_KEY = 'user';
const TOKEN_KEY = 'auth-token';
const REFRESHTOKEN_KEY = 'auth-refreshtoken';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(
    private localStorageService: LocalStorageService
  ) { }

  public saveUser(user): void {
    this.localStorageService.clear(USER_KEY);
    this.localStorageService.store(USER_KEY, user);
  }

  public currentUser() {
    return this.localStorageService.retrieve(USER_KEY);
  }

  public saveToken(token: string) {
    this.localStorageService.clear(TOKEN_KEY);
    this.localStorageService.store(TOKEN_KEY, token);

    const user = this.getUser();
    if (user.userUuid) {
      this.saveUser({ ...user, accessToken: token });
    }
  }

  public getToken(): string {
    return this.localStorageService.retrieve(TOKEN_KEY);
  }

  // change username in localstorage
  public setUsername(username: string) {
    this.currentUser().username = username;
    this.saveUser(this.currentUser());
  }

  clear() {
    this.localStorageService.clear(USER_KEY);
  }

  public saveRefreshToken(token: string): void {
    this.localStorageService.clear(REFRESHTOKEN_KEY);
    this.localStorageService.store(REFRESHTOKEN_KEY, token);
  }

  public getRefreshToken(): string | null {
    return this.localStorageService.retrieve(REFRESHTOKEN_KEY);
  }

  public getUser(): any {
    const user = this.localStorageService.retrieve(USER_KEY);

    if (user) {
      return user;
    }

    return {};
  }

}
