import { Injectable } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";
import { UtilityService } from "../utility/utility.service";

import { RoleType } from "src/app/models/role/RoleType";

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  children: Array<{}>;
  role: string[];
}

@Injectable({
  providedIn: "root",
})
export class RoutingService {
  /** Defines routes and set role that can access them */
  readonly ROUTES: RouteInfo[] = [
    {
      path: "/dashboard",
      title: this.translate.instant("global.dashboard"),
      icon: "fa fa-th-large",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString(),
        RoleType.Customer.toString(),
      ],
    },
    {
      path: "#",
      title: this.translate.instant("scanner.scanner"),
      icon: "fa fa-expand",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString(),
        RoleType.Customer.toString(),
      ],
    },
    {
      path: "/orders",
      title: this.translate.instant("global.orders"),
      icon: "ni ni-delivery-fast",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString(),
      ],
    },
    {
      path: "/packets-registration",
      title: this.translate.instant("registration.title"),
      icon: "far fa-registered",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString()
      ],
    },
    {
      path: "/packets",
      title: this.translate.instant("global.packets"),
      icon: "fa fa-box",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString()
      ],
    },
    {
      path: "#",
      title: this.translate.instant("global.deliveries"),
      icon: "fa fa-box",
      class: "",
      children: [
        {
          path: "/delivery/todeliver",
          title: this.translate.instant("deliveries.to_be_delivered"),
          icon: "fas fa-tasks",
          class: "",
          id: "to-deliver",
        },
        {
          path: "/deliveries",
          title: this.translate.instant("actions.deliver"),
          icon: "ni ni-satisfied",
          class: "",
          id: "delivered",
        },
      ],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString(),
      ],
    },
    {
      path: "/packet-store",
      title: this.translate.instant("packet-store.packet-stores"),
      icon: "fa fa-store",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString(),
      ],
    },
    {
      path: "/packet-category",
      title: this.translate.instant("global.packet-category"),
      icon: "fa fa-sitemap",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString()
      ],
    },
    {
      path: "/packet-tracking/abcinter",
      title: this.translate.instant("global.tracking"),
      icon: "fas fa-search-location",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString(),
        RoleType.Customer.toString(),
      ],
    },
    {
      path: "/shipping-cost",
      title: this.translate.instant("global.shipping-cost"),
      icon: "fa fa-suitcase",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString()
      ],
    },
    {
      path: "/customers",
      title: this.translate.instant("customers.customers"),
      icon: "fa fa-users",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString()
      ],
    },
    {
      path: "/operators",
      title: this.translate.instant("global.operators"),
      icon: "fa fa-users",
      class: "",
      children: [],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString()
      ],
    },
    {
      path: "/roles",
      title: this.translate.instant("roles.roles"),
      icon: "fa fa-user-tag",
      class: "",
      children: [],
      role: [RoleType.Admin.toString()],
    },
    {
      path: "#",
      title: this.translate.instant("global.settings"),
      icon: "ni ni-settings-gear-65",
      class: "",
      children: [
        {
          path: "/profile",
          title: this.translate.instant("global.profile"),
          icon: "ni ni-single-02",
          class: "",
          id: "profile",
        },
        {
          path: "/password",
          title: this.translate.instant("password.pwd-change"),
          icon: "ni ni-key-25",
          class: "",
          id: "pwd",
        },
      ],
      role: [
        RoleType.Admin.toString(),
        RoleType.Commercial.toString(),
        RoleType.Deliverer.toString(),
        RoleType.Customer.toString(),
      ],
    },
  ];

  /** Defines routes */
  routes: RouteInfo[] = [];

  constructor(
    private translate: TranslateService,
    private utilityService: UtilityService
  ) {
    // Populate routes by role
    if (this.utilityService.isAdmin) {
      this.ROUTES.filter((x) =>
        x.role.includes(RoleType.Admin.toString())
      ).forEach((role) => this.routes.push(role));
    } else if (this.utilityService.isCommercial) {
      this.ROUTES.filter((x) =>
        x.role.includes(RoleType.Commercial.toString())
      ).forEach((role) => this.routes.push(role));
    } else if (this.utilityService.isDeliverer) {
      this.ROUTES.filter((x) =>
        x.role.includes(RoleType.Deliverer.toString())
      ).forEach((role) => this.routes.push(role));
    } else if (this.utilityService.isCustomer) {
      this.ROUTES.filter((x) =>
        x.role.includes(RoleType.Customer.toString())
      ).forEach((role) => this.routes.push(role));
    }
  }
}
