import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ShippingCost} from '../../models/shippingCost/shippingCost';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';

const SHIPPINGCOST_URL = environment.apiUrl + '/shipping-costs';

@Injectable({
  providedIn: 'root'
})
export class ShippingCostService {

  constructor(private http: HttpClient) {
  }

  getShippingCostById(shippingCostUuid: string): Observable<ShippingCost> {
    return this.http.get<ShippingCost>(`${SHIPPINGCOST_URL}/${shippingCostUuid}`);
  }

  deleteShippingCostById(shippingCostUuid: string) {
    return this.http.delete(`${SHIPPINGCOST_URL}/${shippingCostUuid}`);
  }

  updateShippingCostById(shippingCost: ShippingCost): Observable<ShippingCost> {
    return this.http.put<ShippingCost>(`${SHIPPINGCOST_URL}/${shippingCost.shippingCostUuid}`, shippingCost);
  }

  addShippingCost(shippingCost: ShippingCost): Observable<ShippingCost> {
    return this.http.post<ShippingCost>(SHIPPINGCOST_URL, shippingCost);
  }

  getAllShippingCosts(): Observable<ShippingCost[]> {
    return this.http.get<ShippingCost[]>(SHIPPINGCOST_URL);
  }
}
