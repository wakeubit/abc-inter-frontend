import { Routes } from '@angular/router';

import { LoginComponent } from '../../pages/login/login.component';
import { RegisterComponent } from '../../pages/register/register.component';
import { PacketTrackingComponent } from 'src/app/pages/packet-tracking/packet-tracking.component';

export const AuthLayoutRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'packet-tracking/:nb', component: PacketTrackingComponent }
];
