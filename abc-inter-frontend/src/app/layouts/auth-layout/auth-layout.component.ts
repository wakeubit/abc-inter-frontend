import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TokenStorageService } from 'src/app/services/jwt/token-storage.service';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit, OnDestroy {
  public isCollapsed = true;
  public isLoggedIn: boolean;

  constructor(
    private router: Router,
    private tokenStorageService: TokenStorageService
  ) {
    this.isLoggedIn = this.tokenStorageService.currentUser() !== null;
  }

  ngOnInit() {
    var html = document.getElementsByTagName("html")[0];
    html.classList.add("auth-layout");
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("bg-default");
    this.router.events.subscribe(() => {
      this.isCollapsed = true;
    });
  }

  ngOnDestroy() {
    var html = document.getElementsByTagName("html")[0];
    html.classList.remove("auth-layout");
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("bg-default");
  }
}
