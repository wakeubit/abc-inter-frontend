import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { ComponentsModule } from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { NgxPrintModule } from 'ngx-print';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatProgressBarModule } from '@angular/material/progress-bar';

import {
  BarcodeGeneratorAllModule,
  QRCodeGeneratorAllModule,
} from "@syncfusion/ej2-angular-barcode-generator";
import { NgxQrcodeStylingModule } from "ngx-qrcode-styling";

import "ag-grid-enterprise";

import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "src/app/pages/dashboard/dashboard.component";
import { UsersComponent } from "../../pages/users/users.component";
import { UserProfileComponent } from "../../pages/user-profile/user-profile.component";
import { ShippingCostComponent } from "../../pages/shipping-cost/shipping-cost.component";
import { OperatorsComponent } from "../../pages/operators/operators.component";
import { ChangePasswordComponent } from "../../pages/password/change-password/change-password.component";
import { PacketsComponent } from "../../pages/packets/packets.component";
import { PacketCategoryComponent } from "../../pages/packet-category/packet-category.component";
import { RoleComponent } from "../../pages/roles/role.component";
import { CustomersComponent } from "../../pages/customers/customers.component";
import { DeliveryComponent } from "../../pages/delivery/delivery.component";
import { PacketStoreComponent } from "../../pages/packet-store/packet-store.component";
import { TaskComponent } from "../../pages/task/task.component";
import { PacketRegistrationComponent } from "../../pages/packet-registration/add/packet-registration.component";
import { RegistrationListComponent } from "../../pages/packet-registration/list/registration-list.component";
import { RegistrationDetailsComponent } from "../../pages/packet-registration/details/registration-details.component";
import { EditPacketComponent } from "../../pages/packets/edit/edit.component";
import { EditUserComponent } from "../../pages/users/edit-user/edit-user.component";
import { EditCustomerComponent } from "../../pages/customers/edit-customer/edit-customer.component";
import { EditAddressComponent } from "../../pages/address/edit-address/edit-address.component";
import { AddPacketComponent } from "../../pages/packets/add/add.component";
import { ToDeliverComponent } from "../../pages/delivery/to-deliver/to-deliver.component";
import { DeliveryDetailsComponent } from "../../pages/delivery/delivery-details/delivery-details.component";
import { UnauthorizedComponent } from "../../pages/errors/unauthorized/unauthorized.component";
import { NotfoundComponent } from "../../pages/errors/notfound/notfound.component";
import { PacketTrackingComponent } from "src/app/pages/packet-tracking/packet-tracking.component";

import { IconsComponent } from "../../pages/icons/icons.component";
import { MapsComponent } from "../../pages/maps/maps.component";
import { TablesComponent } from "../../pages/tables/tables.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    ComponentsModule,
    TranslateModule,
    NgxPaginationModule,
    AgGridModule.withComponents([]),
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    Ng2OrderModule,
    MatStepperModule,
    MatCardModule,
    MatDividerModule,
    MatBadgeModule,
    MatTableModule,
    MatTooltipModule,
    Ng2TelInputModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    NgxPrintModule,
    NgxMatSelectSearchModule,
    BarcodeGeneratorAllModule,
    QRCodeGeneratorAllModule,
    NgxQrcodeStylingModule,
  ],
  declarations: [
    DashboardComponent,
    UsersComponent,
    UserProfileComponent,
    ShippingCostComponent,
    OperatorsComponent,
    ChangePasswordComponent,
    PacketsComponent,
    PacketCategoryComponent,
    RoleComponent,
    CustomersComponent,
    DeliveryComponent,
    PacketStoreComponent,
    TaskComponent,
    PacketRegistrationComponent,
    RegistrationListComponent,
    RegistrationDetailsComponent,
    EditPacketComponent,
    EditUserComponent,
    EditCustomerComponent,
    EditAddressComponent,
    AddPacketComponent,
    ToDeliverComponent,
    DeliveryDetailsComponent,
    UnauthorizedComponent,
    NotfoundComponent,
    PacketTrackingComponent,

    TablesComponent,
    IconsComponent,
    MapsComponent,
  ],
})
export class AdminLayoutModule {}
