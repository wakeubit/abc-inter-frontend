import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { ShippingCostComponent } from '../../pages/shipping-cost/shipping-cost.component';
import { OperatorsComponent } from '../../pages/operators/operators.component';
import { ChangePasswordComponent } from '../../pages/password/change-password/change-password.component';
import { PacketsComponent } from '../../pages/packets/packets.component';
import { PacketCategoryComponent } from '../../pages/packet-category/packet-category.component';
import { RoleComponent } from '../../pages/roles/role.component';
import { CustomersComponent } from '../../pages/customers/customers.component';
import { DeliveryComponent } from '../../pages/delivery/delivery.component';
import { PacketStoreComponent } from '../../pages/packet-store/packet-store.component';
import { PacketRegistrationComponent } from '../../pages/packet-registration/add/packet-registration.component';
import { RegistrationListComponent } from '../../pages/packet-registration/list/registration-list.component';
import { RegistrationDetailsComponent } from 'src/app/pages/packet-registration/details/registration-details.component';
import { EditPacketComponent } from 'src/app/pages/packets/edit/edit.component';
import { EditCustomerComponent } from 'src/app/pages/customers/edit-customer/edit-customer.component';
import { EditAddressComponent } from 'src/app/pages/address/edit-address/edit-address.component';
import { AddPacketComponent } from 'src/app/pages/packets/add/add.component';
import { ToDeliverComponent } from 'src/app/pages/delivery/to-deliver/to-deliver.component';
import { DeliveryDetailsComponent } from 'src/app/pages/delivery/delivery-details/delivery-details.component';
import { AuthGuard } from 'src/app/helpers/auth-guard/auth.guard';
import { RoleType } from 'src/app/models/role/RoleType';
import { UnauthorizedComponent } from 'src/app/pages/errors/unauthorized/unauthorized.component';
import { NotfoundComponent } from 'src/app/pages/errors/notfound/notfound.component';
import { EditUserComponent } from 'src/app/pages/users/edit-user/edit-user.component';

export const AdminLayoutRoutes: Routes = [

  { path: 'dashboard', component: DashboardComponent },
  {
    path: 'operators',
    component: OperatorsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'packets',
    component: PacketsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'registrations/:registrationId/packets/edit/:packetId',
    component: EditPacketComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'packets/add/:registrationId',
    component: AddPacketComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'shipping-cost',
    component: ShippingCostComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial,
        RoleType.Deliverer
      ]
    }
  },
  {
    path: 'packet-category',
    component: PacketCategoryComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'customers',
    component: CustomersComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'customers/edit/:id',
    component: EditCustomerComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial,
      ]
    }
  },
  {
    path: 'users/edit/:id',
    component: EditUserComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial,
      ]
    }
  },
  {
    path: 'roles',
    component: RoleComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin
      ]
    }
  },
  {
    path: 'packet-store',
    component: PacketStoreComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial,
        RoleType.Deliverer
      ]
    }
  },
  {
    path: 'packets-registration',
    component: PacketRegistrationComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'orders',
    component: RegistrationListComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Deliverer,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'orders/details/:id',
    component: RegistrationDetailsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Deliverer,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'address/edit/:id/:type',
    component: EditAddressComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin
      ]
    }
  },
  { path: 'password', component: ChangePasswordComponent },
  { path: 'profile', component: UserProfileComponent },
  {
    path: 'deliveries',
    component: DeliveryComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Deliverer,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'delivery/todeliver',
    component: ToDeliverComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Deliverer,
        RoleType.Commercial
      ]
    }
  },
  {
    path: 'delivery/details/:id',
    component: DeliveryDetailsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        RoleType.Admin,
        RoleType.Deliverer,
        RoleType.Commercial
      ]
    }
  },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: 'error', component: NotfoundComponent },
  { path: 'tables', component: TablesComponent },
  { path: 'icons', component: IconsComponent },
  { path: 'maps', component: MapsComponent }
];
