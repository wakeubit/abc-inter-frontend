export enum CustomerType {
  Anonym = 'ANONYM',
  Privat = 'PRIVATE',
  Business = 'BUSINESS'
}
