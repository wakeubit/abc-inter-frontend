import { PacketState } from "../packet-state/packetState";

export class PacketTracking {
  registrationUuid: string;
  globalTrackingNumber: string;
  globalStatus: string;
  packetsStates: PacketState[];
}
