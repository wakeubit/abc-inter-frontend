import {STATUSTYPE} from "../statusType/statusType";
import {Address} from "../address/address";

export class PacketState {

  constructor(
    public status: string,
    public details: string,
    public actualLocation: Address,
    public createdAt: Date,
  ) {
  }
}
