export enum OperatorType {
  Admin = 'ADMIN',
  Deliverer = 'DELIVERER',
  Storekeeper = 'STOREKEEPER',
  Commercial = "COMMERCIAL"
}

export const allOperatorTypes = [
  OperatorType.Admin,
  OperatorType.Deliverer,
  OperatorType.Storekeeper,
  OperatorType.Commercial
];
