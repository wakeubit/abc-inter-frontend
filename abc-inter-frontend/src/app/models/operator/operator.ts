import { User } from '../user/user';

export class Operator {
  public operatorUuid: string;
  public user: User;
  public operatorType: string;
  public personalNumber: string;
}
