export class PageResult<T> {
  items: T[];
  currentPage: number;
  totalItems: number;
  totalPages: number;
}
