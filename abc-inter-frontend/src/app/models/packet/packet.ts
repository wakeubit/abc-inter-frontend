import { ShippingCost } from '../shippingCost/shippingCost';

export class Packet {
  packetUuid: string;
  currentStoreUuid: string;
  registrationUuid: string;
  delivranceUuid: string;
  trackingNumber: string;
  positionInStore: string;
  positionDetails: string;
  shippingCost: ShippingCost;
  weight: number;
  description: string;
}
