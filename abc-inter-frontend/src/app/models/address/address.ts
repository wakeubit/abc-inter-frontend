export class Address {
  constructor(
    public city: string,
    public country: string,
    public streetAndHouseNumber: string,
    public additionalInformation: string,
    public telAtAddress: number,
    public longitude: number,
    public latitude: number
  ) {}
}
