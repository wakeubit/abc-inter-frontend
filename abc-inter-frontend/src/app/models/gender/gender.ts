export enum Gender {
  Male= 'Male',
  Female = 'Female',
  Others = 'Others'
}

export const allGenders = [
  Gender.Male,
  Gender.Female,
  Gender.Others
];
