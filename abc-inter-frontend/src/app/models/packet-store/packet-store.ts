import {Address} from '../address/address';

export class PacketStore {
    packetStoreUuid: string;
    name: string;
    address: Address;
    volume: number;
}
