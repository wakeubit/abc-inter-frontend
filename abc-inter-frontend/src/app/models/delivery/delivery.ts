import { Operator } from '../operator/operator';
import { Packet } from '../packet/packet';

export class Delivery {
  delivranceUuid: string;
  deliverBy: Operator;
  delivranceDetails: string;
  deliveredPackets: Packet[];
  createdBy: string;
  modifiedBy: string;
  createdAt: string;
  modifiedAt: string;
}
