import { Address } from "../address/address";

export class PacketStateGroup {
  registrationUuid: string;
  status: string;
  actualLocation: Address;
  details: string;
  packetUuids: string[];
}
