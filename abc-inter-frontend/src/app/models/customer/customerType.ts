export enum CustomerType {
  Anonym = 'ANONYM',
  Private = 'PRIVATE',
  Business = 'BUSINESS',
}

export const allCustomerTypes = [
  CustomerType.Anonym,
  CustomerType.Business,
  CustomerType.Private
];
