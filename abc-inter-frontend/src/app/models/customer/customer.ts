import {User} from '../user/user';
import {CustomerType} from './customerType';
import {Address} from '../address/address';

export class Customer {
  customerUuid: string;
  user: User;
  customerType: CustomerType;
  postalAddress: Address;
  bonusCount: number;
}
