export class PacketCategory {

  constructor(
    public packetCategoryUuid: string,
    public name: string,
    public maxWeight: number,
    public maxLength: number,
    public maxWidth: number,
    public maxHeight: number
  ) {
  }
}
