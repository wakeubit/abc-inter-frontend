export enum ShippingType {
  Normal = 'NORMAL',
  Secure = 'SECURE',
  International = 'INTERNATIONAL'
}

