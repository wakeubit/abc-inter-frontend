import { Role } from '../role/role';
export interface User {
  userUuid: string;
  username: string;
  lastname: string;
  firstname: string;
  password: string;
  tokenExpired: boolean;
  email: string;
  tel: string;
  gender: string;
  profileImg: string;
  roles: Role[];
  created_at: string;
}

export class User {
  constructor(
    public userUuid: string,
    public username: string,
    public lastname: string,
    public firstname: string,
    public password: string,
    public tokenExpired: boolean,
    public email: string,
    public tel: string,
    public gender: string,
    public profileImg: string,
    public roles: Role[],
  ) {}
}
