const CURRENCYTYPE = {
    'EURO': 'EUR',
    'DOLLAR': 'DOLLAR',
    'FCFA': 'FCFA'
};

export { CURRENCYTYPE };
