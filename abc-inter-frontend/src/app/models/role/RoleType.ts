export enum RoleType {
  Customer = 'ROLE_CUSTOMER',
  Deliverer = 'ROLE_DELIVERER',
  Storekeeper = 'ROLE_STOREKEEPER',
  Admin = 'ROLE_ADMIN',
  Commercial = 'ROLE_COMMERCIAL'
}
