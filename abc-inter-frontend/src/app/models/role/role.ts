export class Role {
  roleUuid: string;
  name: string;
  description: string;
  constructor(roleUuid, name, description) {
    this.roleUuid = roleUuid;
    this.name = name;
    this.description = description;
  }

}
