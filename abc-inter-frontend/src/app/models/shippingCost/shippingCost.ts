import {PacketCategory} from '../packet-category/packetCategory';

export class ShippingCost {
  constructor(
    public shippingCostUuid: string,
    public price: number,
    public currency: string,
    public bonusPoints: number,
    public category: PacketCategory,
    public shippingType: string
  ) {
  }
}
