import { Address } from '../address/address';
import { Customer } from '../customer/customer';
import { Operator } from '../operator/operator';
import { PacketStore } from '../packet-store/packet-store';
import { Packet } from '../packet/packet';
import { User } from '../user/user';

export class Registration {
    registrationUuid: string;
    globalTrackingNumber: string;
    delivered: boolean;
    status: string;
    registeredBy: Operator;
    store: PacketStore;
    sender: Customer;
    receiver: User;
    registrationDetails: string;
    registeredPackets: Array<Packet>;
    receivingAddress: Address;
    totalPrice: number;
    assignedTo: string;
    currency: string;
    createdBy: string;
    modifiedBy: string;
    createdAt: string;
    modifiedAt: string;
}
