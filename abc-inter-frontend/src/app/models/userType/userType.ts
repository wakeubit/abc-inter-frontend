export enum UserType {
    Customer = 'CUSTOMER',
    Operator = 'OPERATOR',
    Admin = 'ADMIN'
}

