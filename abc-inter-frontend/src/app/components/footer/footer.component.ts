import { Component, OnInit } from '@angular/core';

import { abcInter } from 'src/app/shared/abcInter';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  today: Date;
  abcInterUrl: string;

  ngOnInit() {
    this.today = new Date();
    this.abcInterUrl = abcInter.url;
  }

}
