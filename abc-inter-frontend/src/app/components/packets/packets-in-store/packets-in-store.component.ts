import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from "@angular/forms";

import { ToastrService } from 'ngx-toastr';
import { UtilityService } from '../../../services/utility/utility.service';
import { PacketsService } from '../../../services/packets/packets.service';

import { STATUSTYPE } from '../../../models/statusType/statusType';

@Component({
  selector: 'app-packets-in-store',
  templateUrl: './packets-in-store.component.html',
  styleUrls: ['./packets-in-store.component.css']
})
export class PacketsInStoreComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  public gridApi;
  public gridColumnApi;
  /**Columns definition */
  public columnDefs = [{}];
  /**Group definition */
  public autoGroupColumnDef: {};
  /**Default configuration to apply to any column */
  public defaultColDef = {};
  /**Display the total amount of row*/
  public statusBar = {};
  /**Display right for columns settings and input filtering */
  public sideBar: {};
  /**Information to put into the table */
  public rowData = [];
  /**Rows to be emitted to the packet_to_be_delivered Component */
  @Output() selectedRows = new EventEmitter<[{}]>();

  public confirmHeaderTitle: string;
  /**List of options in dropDown */
  public actionsOnItem = [];
  isAdmin: boolean = false;
  isPacketSelected: boolean = false;
  public selectedPackets: Array<string>;

  /** control for the selected action */
  public actionCtrl: FormControl = new FormControl();
  public selectedAction: string;

  public totalRows: number = 0;

  /**Know if this component is called from others */
  isCall: boolean = false;

  constructor(
    private utilityService: UtilityService,
    private packetsService: PacketsService,
    private toastrService: ToastrService,
    private modalService: NgbModal
  ) {
    this.attributInitialisation();
  }

  ngOnInit(): void {
    // check whether logged In user is an admin or not
    this.isAdmin = this.utilityService.isAdmin;
    // table content initialisation
    this.getPacketsInStore();
    // check whether is call from other components
    this.isComponentCall();
  }

  attributInitialisation() {
    // Columns configuration
    this.columnDefs = [
      {
        headerName: 'Packet',
        children: [
          {
            headerName: 'PacketID',
            field: 'packetID',
            hide: true
          },
          {
            headerName: 'Tracking Number',
            field: 'trackingNb',
            columnGroupShow: 'closed'
          },
          {
            headerName: 'Category',
            field: 'packetCat',
            columnGroupShow: 'open'
          },
          {
            headerName: 'Dimension (W x H x L)',
            field: 'dimension',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: 'Store',
        children: [
          {
            headerName: 'StoreID',
            field: 'packetID',
            hide: true
          },
          {
            headerName: 'Name',
            field: 'storeName',
            columnGroupShow: 'closed'
          },
          {
            headerName: 'Address',
            field: 'storeAddress',
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: 'Receiver',
        children: [
          {
            headerName: 'Name',
            field: 'receiverName',
            filter: 'agSetColumnFilter',
            rowGroup: true,
            hide: true
          },
          {
            headerName: 'Address',
            field: 'receiverAddress',
            columnGroupShow: 'closed'
          },
          {
            headerName: 'Phone',
            field: 'receiverPhone',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Email',
            field: 'receiverEmail',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Gender',
            field: 'receiverGender',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: 'Sender',
        children: [
          {
            headerName: 'Name',
            field: 'senderName',
            sortable: false,
            columnGroupShow: 'closed'
          },
          {
            headerName: 'Phone',
            field: 'senderPhone',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Email',
            field: 'senderEmail',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Address',
            field: 'senderAddress',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      }
    ];

    this.defaultColDef = {
      menuTabs: ['filterMenuTab'],
      sortable: true,
      filter: true,
      resizable: true,
      minWidth: 150,
      suppressMovable: true,
      flex: 1
    };

    this.autoGroupColumnDef = {
      headerName: 'Name',
      field: 'receiverName',
      cellRenderer: 'agGroupCellRenderer',
      width: 250,
      flex: 1,
      pinned: 'left',
      headerTooltip: 'Recipient',
      cellRendererParams: {
        checkbox: true
      }
    };

    this.statusBar = {
      statusPanels: [
        { align: 'left', statusPanel: 'agTotalRowCountComponent' },
        { align: 'left', statusPanel: 'agSelectedRowCountComponent' }
      ]
    }

    this.sideBar = {
      toolPanels: [
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filter',
          toolPanel: 'agFiltersToolPanel'
        }
      ]
    }

    this.actionsOnItem = ['To be delivered'];
    this.selectedPackets = new Array<string>();
    this.confirmHeaderTitle = 'Confirmation';
  }

  /**API calls to save some information from the table */
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  getPacketsInStore() {
    this.packetsService.findPacketsInStore().subscribe(packets => {
      if (packets) {
        this.populateRowData(packets);
      }
    }, err => {
      if (err) {
        this.toastrService.error('Connection could not be established!');
      }
    });
  }

  /**Prepares information for each row of the table */
  private populateRowData(list) {
    // clear old information
    this.rowData = [];
    // build and add new row
    list.map(item => {
      // row initialisation
      var row = {
        receiverName: '',
        receiverAddress: '',
        receiverEmail: '',
        receiverGender: '',
        receiverPhone: '',
        packetID: '',
        trackingNb: '',
        packetCat: '',
        dimension: '',
        senderName: '',
        senderPhone: '',
        senderAddress: '',
        senderEmail: ''
      };

      row.receiverName = `${item.receiver.lastname} ${item.receiver.firstname}`;
      row.receiverAddress = `${item.receivingAddress.streetAndHouseNumber} (${item.receivingAddress.city} - ${item.receivingAddress.country})`;
      row.receiverEmail = item.receiver.email;
      row.receiverGender = item.receiver.gender;
      row.receiverPhone = `${item.receiver.tel} / ${item.receivingAddress.telAtAddress}`;
      row.packetID = item.packetUuid;
      row.packetCat = item.category.name;
      row.trackingNb = item.trackingNumber;
      row.dimension = `${item.width} X ${item.height} X ${item.length}`;
      row.senderName = `${item.sender.user.lastname} ${item.sender.user.firstname}`;
      row.senderAddress = `${item.sender.postalAddress.streetAndHouseNumber} (${item.sender.postalAddress.city} - ${item.sender.postalAddress.country})`;
      row.senderPhone = item.sender.user.tel;
      row.senderEmail = item.sender.user.email;
      //TODO: build also store information

      // add the currentrow to the list
      this.rowData.push(row);
      // increment the total rows
      ++this.totalRows;
    });
  }

  /**Gets the list of packets store */
  private getPacketstoreInformation(packetID: string) {
    // TODO: store the list of packetstore on init. This list will be filtered to assign each packet to his store
    // return this.packetStoreService.findStoreByPacketID(packetID).subscribe();
  }

  /**Saves the list  */
  getSelectedPackets(): Array<string> {
    // reset the packet value
    let packets: Array<string> = new Array<string>();
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => {
      if (node.groupData) {
        return { make: node.key, model: 'Group' };
      }
      return node.data;
    });
    selectedData.map(node => {
      packets.push(node.packetID);
    });
    // return the list
    return packets;
  }

  /** Checks whether any packet has been selected or not.
   * this settings is needed to konw when to display or hide input */
  public setConfig() {
    this.selectedPackets = this.getSelectedPackets();
    if (this.selectedPackets.length > 0) {
      this.isPacketSelected = true;
    } else {
      this.isPacketSelected = false;
      this.actionCtrl.setValue('');
    }
  }

  openConfirmationDialog(content: any) {
    // check whether any has been selected or not
    this.modalService.open(content, { centered: true, scrollable: true });
    // save the selected action
    this.selectedAction = this.actionCtrl.value;
    // save selected packet(s)
    this.selectedPackets = this.getSelectedPackets();
  }

  /**Calls to apply any selected action */
  applyAction() {
    const statusName = STATUSTYPE.TO_BE_DELIVERED;
    this.setPacketsStatus(statusName, this.utilityService.userId);
  }

  /**Changes the packets status amd saves the user id of the person who has performed this operation */
  private setPacketsStatus(statusName: string, operatorUserID: string) {
    // Builds the packets node
    let packets = [];
    this.selectedPackets.map(packetId => {
      packets.push({ 'packetUuid': packetId });
    });

    const reqBody = {
      'packets': packets,
      'statusName': statusName,
      'modifiedBy': operatorUserID
    };

    this.packetsService.updatePacketStatus(reqBody).subscribe(response => {
      if (response) {
        const selectedData = this.gridApi.getSelectedRows();
        // remove the rows from the current list
        this.gridApi.applyTransaction({ remove: selectedData });

        // Decrement the total rows also
        const counter = selectedData.length;
        this.totalRows -= counter;

        // Notify the user
        let message: string = selectedData.length > 1 ? 'Tasks have been successfully added.' : 'New task has been successfully added.';
        this.toastrService.success(message);
        // reset view config
        this.setConfig();
      }
    }, (error) => {
      if (error) {
        this.toastrService.error('This operation could not be performed!');
      }
    });
    // close popup confirmation window
    if (!this.isCall) {
      this.modalService.dismissAll();
    }
  }

  /**Chechs whether the component is called by another. In the fact some information in the view have to be hide. */
  isComponentCall() {
    this.utilityService.currentData.subscribe(dataSubjetct => {
      if (dataSubjetct != null) {
        this.isCall = dataSubjetct['isPacketStoreCall'];
      }
    });
  }

  //
  emitSelectedRows() {
    const selectedData = this.gridApi.getSelectedRows();
    this.selectedRows.emit(selectedData);
    // and change the selected packets status
    this.applyAction();
  }

}
