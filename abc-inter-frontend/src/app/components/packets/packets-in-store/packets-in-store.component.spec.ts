import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketsInStoreComponent } from './packets-in-store.component';

describe('PacketsInStoreComponent', () => {
  let component: PacketsInStoreComponent;
  let fixture: ComponentFixture<PacketsInStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketsInStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketsInStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
