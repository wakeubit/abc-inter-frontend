import { Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormControl } from "@angular/forms";
import { AgGridAngular } from 'ag-grid-angular';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReplaySubject, Subject } from 'rxjs';
import { debounceTime, delay, filter, takeUntil, tap, map } from 'rxjs/operators';

import { MatSelect } from '@angular/material/select';

import { ToastrService } from 'ngx-toastr';
import { UtilityService } from '../../../services/utility/utility.service';
import { PacketsService } from '../../../services/packets/packets.service';
import { OperatorsService } from 'src/app/services/operators/operators.service';

import { STATUSTYPE } from '../../../models/statusType/statusType';

/**Builds object of type agent */
interface Agent {
  id: String;
  name: String;
}

@Component({
  selector: 'app-packets-to-be-delivered',
  templateUrl: './packets-to-be-delivered.component.html',
  styleUrls: ['./packets-to-be-delivered.component.css']
})
export class PacketsToBeDeliveredComponent implements OnInit, OnDestroy {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  public gridApi;
  public gridColumnApi;
  /**Columns definition */
  public columnDefs = [{}];
  /**Group definition */
  public autoGroupColumnDef: {};
  /**Default configuration to apply to any column */
  public defaultColDef = {};
  /**Display the total amount of row*/
  public statusBar = {};
  /**Display right for columns settings and input filtering */
  public sideBar: {};
  /**Information to put into the table */
  public rowData = [];
  /**Rows to be emitted to the packet_to_be_delivered Component */
  @Output() selectedRows = new EventEmitter<[{}]>();
  isCall: boolean = false;

  public confirmHeaderTitle: string;
  /**List of options in dropDown */
  public actionsOnItem = [];
  isPacketSelected: boolean = false;
  isAdmin: boolean = false;
  public selectedPackets: Array<string>;
  public selectedOperatorUserID: string;
  public selectedOperatorName: string;

  public operatorsList: Agent[] = [];

  /** control for the selected operator */
  public operatorCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public operatorFilteringCtrl: FormControl = new FormControl();

  /** list of operator filtered by search keyword */
  public filteredOperators: ReplaySubject<Agent[]> = new ReplaySubject<Agent[]>(1);

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
  /** indicate search operation is in progress */
  public searching = false;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  public totalRows: number = 0;

  constructor(
    private utilityService: UtilityService,
    private packetsService: PacketsService,
    private toastrService: ToastrService,
    private modalService: NgbModal,
    private operatorService: OperatorsService
  ) {
    this.attributInitialisation();
  }

  ngOnInit(): void {
    this.isAdmin = this.utilityService.isAdmin;
    this.getPacketsToBeDelivered();
    // Initialize select option for admin logged in user
    if (this.isAdmin) {
      this.operatorsList = this.getAllOperators();
      this.operatorFilteringCtrl.valueChanges
        .pipe(
          filter(search => !!search),
          tap(() => this.searching = true),
          takeUntil(this._onDestroy),
          debounceTime(200),
          map(search => {
            if (!this.operatorsList) { return []; }
            return this.operatorsList.filter(operator => operator.name.toLocaleLowerCase().indexOf(search) > -1);
          }),
          delay(500),
          takeUntil(this._onDestroy)
        )
        .subscribe(filteredOperator => {
          this.searching = false;
          this.filteredOperators.next(filteredOperator);
        }, error => {
          this.searching = false;
        });

      this.selectedOperatorName = '';
    }

    // check whether this component is been called
    this.isComponentCall();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  attributInitialisation() {
    // Columns configuration
    this.columnDefs = [
      {
        headerName: 'Packet',
        children: [
          {
            headerName: 'PacketID',
            field: 'packetID',
            hide: true,
          },
          {
            headerName: 'Tracking Number',
            field: 'trackingNb',
            columnGroupShow: 'closed'
          },
          {
            headerName: 'Category',
            field: 'packetCat',
            columnGroupShow: 'open'
          },
          {
            headerName: 'Dimension (W x H x L)',
            field: 'dimension',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: 'Receiver',
        children: [
          {
            headerName: 'Name',
            field: 'receiverName',
            filter: 'agSetColumnFilter',
            rowGroup: true,
            hide: true
          },
          {
            headerName: 'Address',
            field: 'receiverAddress',
            columnGroupShow: 'closed'
          },
          {
            headerName: 'Phone',
            field: 'receiverPhone',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Email',
            field: 'receiverEmail',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Gender',
            field: 'receiverGender',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: 'Sender',
        children: [
          {
            headerName: 'Name',
            field: 'senderName',
            sortable: false,
            columnGroupShow: 'closed'
          },
          {
            headerName: 'Phone',
            field: 'senderPhone',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Email',
            field: 'senderEmail',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: 'Address',
            field: 'senderAddress',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      }
    ];

    this.defaultColDef = {
      menuTabs: ['filterMenuTab'],
      sortable: true,
      filter: true,
      resizable: true,
      minWidth: 150,
      suppressMovable: true,
      flex: 1
    };

    this.autoGroupColumnDef = {
      headerName: 'Name',
      field: 'receiverName',
      cellRenderer: 'agGroupCellRenderer',
      width: 250,
      flex: 1,
      pinned: 'left',
      headerTooltip: 'Recipient',
      cellRendererParams: {
        checkbox: true
      }
    };

    this.statusBar = {
      statusPanels: [
        { align: 'left', statusPanel: 'agTotalRowCountComponent' },
        { align: 'left', statusPanel: 'agSelectedRowCountComponent' }
      ]
    }

    this.sideBar = {
      toolPanels: [
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filter',
          toolPanel: 'agFiltersToolPanel'
        }
      ]
    }

    const action = this.utilityService.isAdmin ? 'Assign to an agent' : 'Add to my todo';
    this.actionsOnItem = [action];
    this.selectedPackets = new Array<string>();
    this.confirmHeaderTitle = 'Confirmation';
  }

  /**API calls to save some information from the table */
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  /**Calls to get the list of packets that have to be delivered */
  getPacketsToBeDelivered() {
    this.packetsService.findPacketsToBeDelivered().subscribe(packets => {
      if (packets) {
        this.populateRowData(packets);
      }
    }, err => {
      if (err) {
        this.toastrService.error('Connection could not be established!');
      }
    });
  }

  /**Calls to save the list of registered operators */
  getAllOperators(): Agent[] {
    let list: Agent[] = [];
    this.operatorService.getAllOperators().subscribe(operators => {
      if (operators) {
        operators.map(operator => {
          const agent = {
            'id': operator.user.userUuid,
            'name': `${operator.user.firstname} ${operator.user.lastname} (${operator.personalNumber})`
          };
          // add to the list
          list.push(agent);
        });
      }
    }, (err) => {
      if (err) {
        this.toastrService.error('List of agents could not be intialized');
      }
    });
    return list;
  }

  /**Prepares information for each row of the table */
  private populateRowData(list) {
    // clear old information
    this.rowData = [];
    // build and add new row
    list.map(item => {
      const row = this.createRow(item);
      // add the current row to the list
      this.rowData.push(row);
      // increment the total rows
      ++this.totalRows;
    });
  }

  private createRow(item): {} {
    // row initialisation
    var row = {
      receiverName: '',
      receiverAddress: '',
      receiverEmail: '',
      receiverGender: '',
      receiverPhone: '',
      packetID: '',
      trackingNb: '',
      packetCat: '',
      dimension: '',
      senderName: '',
      senderPhone: '',
      senderAddress: '',
      senderEmail: ''
    };

    row.receiverName = `${item.receiver.lastname} ${item.receiver.firstname}`;
    row.receiverAddress = `${item.receivingAddress.streetAndHouseNumber} (${item.receivingAddress.city} - ${item.receivingAddress.country})`;
    row.receiverEmail = item.receiver.email;
    row.receiverGender = item.receiver.gender;
    row.receiverPhone = `${item.receiver.tel} / ${item.receivingAddress.telAtAddress}`;
    row.packetID = item.packetUuid;
    row.packetCat = item.category.name;
    row.trackingNb = item.trackingNumber;
    row.dimension = `${item.width} X ${item.height} X ${item.length}`;
    row.senderName = `${item.sender.user.lastname} ${item.sender.user.firstname}`;
    row.senderAddress = `${item.sender.postalAddress.streetAndHouseNumber} (${item.sender.postalAddress.city} - ${item.sender.postalAddress.country})`;
    row.senderPhone = item.sender.user.tel;
    row.senderEmail = item.sender.user.email;

    return row;
  }

  /**Saves the list  */
  getSelectedPackets(): Array<string> {
    // reset the packet value
    let packets: Array<string> = new Array<string>();
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => {
      if (node.groupData) {
        return { make: node.key, model: 'Group' };
      }
      return node.data;
    });
    selectedData.map(node => {
      packets.push(node.packetID);
    });
    // return the list
    return packets;
  }

  /**Assign selected packets to an agent. If no operator is given,
   * then the current logged in user will save as the person who will complete the shipment.
   * Otherwise the logged In user must select the agent.*/
  private assignPackets(statusName: string, operatorUserID: string) {
    // Builds the packets node
    let packets = [];
    this.selectedPackets.map(packetId => {
      packets.push({ 'packetUuid': packetId });
    });

    const reqBody = {
      'packets': packets,
      'statusName': statusName,
      'modifiedBy': operatorUserID
    };

    this.packetsService.updatePacketStatus(reqBody).subscribe(response => {
      if (response) {
        const selectedData = this.gridApi.getSelectedRows();
        this.gridApi.applyTransaction({ remove: selectedData });
        // decrement the total rows also
        this.totalRows -= selectedData.length;
        // Notification for the user
        let message: string = `The task has been successfully assigned to ${this.selectedOperatorName}.`;
        if (!this.isAdmin) {
          message = 'This task has been successfully added to your list.';
        }
        this.toastrService.success(message);
        // hide input
        this.setConfig();
      }
    }, (error) => {
      if (error) {
        this.toastrService.error('This operation could not be performed!');
      }
    });

    // close popup confirmation window
    this.modalService.dismissAll();
  }

  /**Calls when new rows has been emitted */
  private onRowsEmitted(emittedRows: [{}]) {
    //TODO: fetch the output removing information concerning the store
    // refresh the table
    this.gridApi.applyTransaction({ add: emittedRows });
  }

  /** Checks whether any packet has been selected or not.
   * this settings is needed to konw when to display or hide input */
  public setConfig() {
    this.selectedPackets = this.getSelectedPackets();
    if (this.selectedPackets.length > 0) {
      this.isPacketSelected = true;
    } else {
      this.isPacketSelected = false;
      // reset the value of the selected operator
      this.operatorCtrl.setValue('');
    }
  }

  openConfirmationDialog(content: any) {
    this.modalService.open(content, { centered: true, scrollable: true });
    // save the selected agent
    this.selectedOperatorUserID = this.operatorCtrl.value?.id;
    this.selectedOperatorName = this.operatorCtrl.value?.name;
  }

  /**Calls to apply any selected action */
  applyAction() {
    const statusName = STATUSTYPE.READY_FOR_DELIVERY;
    if (this.isAdmin) {
      if (this.selectedOperatorUserID !== 'undefined' || this.selectedOperatorUserID != null) {
        this.assignPackets(statusName, this.selectedOperatorUserID);
      }
    } else {
      // give the current logged In userID
      this.assignPackets(statusName, this.utilityService.userId);
    }
  }

  /**Chechs whether the component is called by another. In the fact some information in the view have to be hide. */
  isComponentCall() {
    this.utilityService.currentData.subscribe(dataSubjetct => {
      if (dataSubjetct != null) {
        this.isCall = dataSubjetct['isPacketToBeDeliveredCall'];
      }
    });
  }

  /**Displays the list of packets in store in a modal window */
  displayPacketInStore(packetList) {
    // prevent the packetStore component, that it is a call
    this.utilityService.changeDataSubject({ 'isPacketStoreCall': true });
    // show the list
    this.modalService.open(packetList, { centered: true, scrollable: true, size: 'lg' });
  }

  /**Calls if this component has been called from the to_do list */
  emitSelectedRows() {
    this.selectedRows.emit(this.gridApi.getSelectedRows());
    // and change the selected packets status
    this.applyAction();
  }
}
