import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketsToBeDeliveredComponent } from './packets-to-be-delivered.component';

describe('PacketsToBeDeliveredComponent', () => {
  let component: PacketsToBeDeliveredComponent;
  let fixture: ComponentFixture<PacketsToBeDeliveredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketsToBeDeliveredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketsToBeDeliveredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
