import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";
import { AgGridAngular } from 'ag-grid-angular';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ToastrService } from 'ngx-toastr';
import { UtilityService } from '../../../services/utility/utility.service';
import { PacketsService } from '../../../services/packets/packets.service';

import { STATUSTYPE } from '../../../models/statusType/statusType';

@Component({
    selector: 'app-packets-ready-for-delivery',
    templateUrl: './packets-ready-for-delivery.component.html',
    styleUrls: ['./packets-ready-for-delivery.component.css']
})
export class PacketsReadyForDeliveryComponent implements OnInit {

    @ViewChild('agGrid') agGrid: AgGridAngular;
    public gridApi;
    public gridColumnApi;
    /**Columns definition */
    public columnDefs = [{}];
    /**Group definition */
    public autoGroupColumnDef: {};
    /**Default configuration to apply to any column */
    public defaultColDef = {};
    /**Display the total amount of row*/
    public statusBar = {};
    /**Display right for columns settings and input filtering */
    public sideBar: {};
    /**Information to put into the table */
    public rowData = [];

    public confirmHeaderTitle: string;
    /**List of options in dropDown */
    public actionsOnItem = [];
    /** List of reasons */
    public reasons = [];

    isPacketSelected: boolean = false;

    /** control for the selected operator */
    public actionCtrl: FormControl = new FormControl();
    /**Control message input */
    public msgCtrl: FormControl = new FormControl();

    isInputValid = false;
    isAdmin: boolean = false;
    isDecline: boolean = false;
    totalRows: number = 0;

    public userConfirmMsg: string;
    public errorMessage: String;
    isCall: boolean = false;

    constructor(
        private utilityService: UtilityService,
        private packetsService: PacketsService,
        private toastrService: ToastrService,
        private modalService: NgbModal
    ) {
        this.attributInitialisation();
    }

    ngOnInit(): void {
        this.isAdmin = this.utilityService.isAdmin;
        // initialize operator todo list
        this.getPacketsReadyToBeDelivered();
    }

    attributInitialisation() {
        this.columnDefs = [
            {
                headerName: 'Receiver',
                children: [
                    {
                        headerName: 'Name',
                        field: 'receiverName',
                        filter: 'agSetColumnFilter',
                        rowGroup: true,
                        hide: true
                    },
                    {
                        headerName: 'Address',
                        field: 'receiverAddress',
                        columnGroupShow: 'closed'
                    },
                    {
                        headerName: 'Gender',
                        field: 'receiverGender',
                        sortable: false,
                        columnGroupShow: 'open'
                    },
                    {
                        headerName: 'Email',
                        field: 'receiverEmail',
                        sortable: false,
                        columnGroupShow: 'open'
                    },
                    {
                        headerName: 'Phone',
                        field: 'receiverPhone',
                        sortable: false,
                        columnGroupShow: 'open'
                    }
                ]
            },
            {
                headerName: 'Packet',
                children: [
                    {
                        headerName: 'PacketID',
                        field: 'packetID',
                        hide: true,
                    },
                    {
                        headerName: 'Tracking Number',
                        field: 'trackingNb',
                        columnGroupShow: 'closed'
                    },
                    {
                        headerName: 'Category',
                        field: 'packetCat',
                        columnGroupShow: 'open'
                    },
                    {
                        headerName: 'Dimension (W x H x L)',
                        field: 'dimension',
                        sortable: false,
                        columnGroupShow: 'open'
                    }
                ]
            },
            {
                headerName: 'Sender',
                children: [
                    {
                        headerName: 'Name',
                        field: 'senderName',
                        sortable: false,
                        columnGroupShow: 'closed'
                    },
                    {
                        headerName: 'Email',
                        field: 'senderEmail',
                        sortable: false,
                        columnGroupShow: 'open'
                    },
                    {
                        headerName: 'Phone',
                        field: 'senderPhone',
                        sortable: false,
                        columnGroupShow: 'open'
                    },
                    {
                        headerName: 'Address',
                        field: 'senderAddress',
                        sortable: false,
                        columnGroupShow: 'open'
                    }
                ]
            },
            {
                headerName: 'Responsible',
                headerTooltip: 'The person in charge.',
                children: [
                    {
                        headerName: 'Personl Number',
                        field: 'personalNb',
                        columnGroupShow: 'closed',
                        hide: !this.utilityService.isAdmin
                    },
                    {
                        headerName: 'Name',
                        field: 'responsibleName',
                        columnGroupShow: 'open',
                        sortable: false,
                        hide: !this.utilityService.isAdmin
                    },
                    {
                        headerName: 'Telephone',
                        field: 'responsiblePhone',
                        columnGroupShow: 'open',
                        sortable: false,
                        hide: !this.utilityService.isAdmin
                    }
                ]
            }
        ];

        this.defaultColDef = {
            menuTabs: ['filterMenuTab'],
            sortable: true,
            filter: true,
            resizable: true,
            minWidth: 150,
            suppressMovable: true,
            flex: 1
        };

        this.autoGroupColumnDef = {
            headerName: 'Name',
            field: 'receiverName',
            cellRenderer: 'agGroupCellRenderer',
            width: 250,
            flex: 1,
            pinned: 'left',
            headerTooltip: 'Recipient',
            cellRendererParams: {
                checkbox: true
            }
        };

        this.statusBar = {
            statusPanels: [
                { align: 'left', statusPanel: 'agTotalRowCountComponent' },
                { align: 'left', statusPanel: 'agSelectedRowCountComponent' }
            ]
        };

        // We want to display sidebar only for admin user
        if (this.utilityService.isAdmin) {
            this.sideBar = {
                toolPanels: [
                    {
                        id: 'filters',
                        labelDefault: 'Filters',
                        labelKey: 'filters',
                        iconKey: 'filter',
                        toolPanel: 'agFiltersToolPanel'
                    },
                    {
                        id: 'columns',
                        labelDefault: 'Columns',
                        labelKey: 'columns',
                        iconKey: 'columns',
                        toolPanel: 'agColumnsToolPanel',
                        hide: true
                    }
                ]
            };
        }

        this.actionsOnItem = ['Decline', 'Mark as Done'];
        this.reasons = ['Client not present', 'Oder reason'];
    }

    /**Chechs whether the component is called by another. In the fact some information in the view have to be hide. */
    isComponentCall() {
        this.utilityService.currentData.subscribe(dataSubjetct => {
            if (dataSubjetct != null) {
                this.isCall = dataSubjetct['isPacketReadyForDeliveryCall'];
            }
        });
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    getPacketsReadyToBeDelivered() {
        // this.readyForDeliveryPacketsList = new Array<any>();
        this.packetsService.findPacketsReadyToBeDelivered().subscribe(packets => {
            if (packets) {
                // check whether the current logged in user is an operator and admin or not.
                if (this.utilityService.isAdmin) {
                    // If the user is an admin than display all packets respectively the operator who is in charge
                    // this.readyForDeliveryPacketsList = packets;
                    this.populateRowData(packets);
                } else {
                    // fetch the list and retreive only entry that have been saved by the operator
                    this.populateRowData(this.groupByOperator(packets, this.utilityService.userId));
                }
            }
        }, err => {
            if (err) {
                this.toastrService.error('Connection could not be established!');
            }
        });
    }

    public groupByOperator(list, operatorUserID: String): string[] {
        const result = [];
        list.forEach(item => {
            if (item.operator.operatorUserID === operatorUserID) {
                result.push(item);
            }
        });
        return result;
    }

    // add information to the table
    private populateRowData(list) {
        // clear old information
        this.rowData = [];
        // build and add new information for the table
        list.map(item => {
            const row = this.createRow(item);
            // add the current row to the list
            this.rowData.push(row);
            // increment the total rows
            ++this.totalRows;
        });
    }

    private createRow(item): {} {
        // row initialisation
        const row = {
            receiverName: '',
            receiverAddress: '',
            receiverEmail: '',
            receiverGender: '',
            receiverPhone: '',
            packetID: '',
            trackingNb: '',
            packetCat: '',
            dimension: '',
            senderName: '',
            senderPhone: '',
            senderAddress: '',
            senderEmail: '',
            personalNb: '',
            responsibleName: '',
            responsiblePhone: ''
        };

        row.receiverName = `${item.packet.receiver.lastname} ${item.packet.receiver.firstname}`;
        row.receiverAddress = `${item.packet.receivingAddress.street} (${item.packet.receivingAddress.city} - ${item.packet.receivingAddress.country})`;
        row.receiverEmail = item.packet.receiver.email;
        row.receiverGender = item.packet.receiver.gender;
        row.receiverPhone = `${item.packet.receiver.phone} / ${item.packet.receivingAddress.telephone}`;
        row.packetID = item.packet.ID;
        row.packetCat = item.packet.category.name;
        row.trackingNb = item.packet.trackingNumber;
        row.dimension = `${item.packet.dimension.width} X ${item.packet.dimension.height} X ${item.packet.dimension.length}`;
        row.senderName = `${item.packet.sender.lastname} ${item.packet.sender.firstname}`;
        row.senderAddress = `${item.packet.sender.address.street} (${item.packet.sender.address.city} - ${item.packet.sender.address.country})`;
        row.senderPhone = item.packet.sender.phone;
        row.senderEmail = item.packet.sender.email;
        row.personalNb = item.operator.personalNumber;
        row.responsibleName = item.operator.name;
        row.responsiblePhone = item.operator.phone;
        return row;
    }

    /** returns a list of packets that have been selected */
    getSelectedPackets(): Array<string> {
        // reset the packet value
        let packets: Array<string> = new Array<string>();
        const selectedNodes = this.agGrid.api.getSelectedNodes();
        const selectedData = selectedNodes.map(node => {
            if (node.groupData) {
                return { make: node.key, model: 'Group' };
            }
            return node.data;
        });

        selectedData.map(node => {
            packets.push(node.packetID);
        });
        // return the list
        return packets;
    }

    getSelectedOpt() {
        const selectedOpt = this.actionCtrl.value;
        if (selectedOpt.includes('Decline')) {
            this.isDecline = true;
            this.isInputValid = false;
            this.confirmHeaderTitle = 'What is the reason ?';
        } else {
            this.isDecline = false;
            this.isInputValid = true;
            this.confirmHeaderTitle = 'Confirm that you have received your shipment.';
        }
        // clear msgCtrl value
        this.msgCtrl.setValue('');
    }

    /** Display information base on selected options */
    confirm(modalContent) {
        this.modalService.open(modalContent, { centered: true, scrollable: false });
    }

    applyAction() {
        // TODO: msg contain also the value of the reason that has been selected
        const msg = this.msgCtrl.value;
        // fetch what action to apply
        if (this.isDecline) { // TODO: Reason is needed
            this.setPacketsStatus(STATUSTYPE.TO_BE_DELIVERED);
        } else {
            // If the packet has been delivered, than add it to the list of done. Recipient must sign.
            this.setPacketsStatus(STATUSTYPE.DELIVERED, msg);
        }
    }

    setPacketsStatus(newStatus: String, signature?: String) {
        const packets = [];
        this.getSelectedPackets().map(packetId => {
            packets.push({ 'packetUuid': packetId });
        });

        // object to send building
        const object = {
            'packets': packets,
            'statusName': newStatus,
            'modifiedBy': this.utilityService.userId,
            'deliveryDetails': signature
        };

        this.packetsService.updatePacketStatus(object).subscribe(response => {
            if (response) {
                const selectedData = this.gridApi.getSelectedRows();
                this.gridApi.applyTransaction({ remove: selectedData });
                const message = selectedData.length > 1 ? 'Packets have been successful declined' : 'The Packet has been successful declined';
                this.toastrService.success(message);
                // hide input
                this.setConfig();
                // set the total rows value
                this.totalRows -= packets.length;
            }
        }, (error) => {
            if (error) {
                this.toastrService.error('Error while trying decline packet(s)!');
            }
        });
        // close popup confirmation window
        this.modalService.dismissAll();
    }

    /** Checks whether any packet has been selected or not.*/
    public setConfig() {
        if (this.getSelectedPackets().length > 0) {
            this.isPacketSelected = true;
        } else {
            this.isPacketSelected = false;
        }

        // reset the value of the selected action
        this.actionCtrl.setValue('');
        this.msgCtrl.setValue('');
        this.isDecline = false;
        this.isInputValid = false;
    }

    displayPacketToBeDelivered(packetList) {
        // prevent the packetStore component, that it is a call
        this.utilityService.changeDataSubject({ 'isPacketToBeDeliveredCall': true });
        // show the list
        this.modalService.open(packetList, { centered: true, scrollable: true, size: 'lg' });
    }

    /**Calls when new rows has been emitted */
    private onRowsEmitted(emittedRows: [{}]) {
        // set total rows value
        this.totalRows += emittedRows.length;
        // add new rows to the list
        this.gridApi.applyTransaction({ add: emittedRows });
    }
}
