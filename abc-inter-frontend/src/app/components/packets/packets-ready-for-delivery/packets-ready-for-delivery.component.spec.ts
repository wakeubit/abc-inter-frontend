import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketsReadyForDeliveryComponent } from './packets-ready-for-delivery.component';

describe('PacketsReadyForDeliveryComponent', () => {
  let component: PacketsReadyForDeliveryComponent;
  let fixture: ComponentFixture<PacketsReadyForDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketsReadyForDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketsReadyForDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
