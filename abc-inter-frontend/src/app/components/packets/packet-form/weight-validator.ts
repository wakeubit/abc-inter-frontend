import { PacketCategory } from '../../../models/packet-category/packetCategory';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function weightValidator(
  selectedPacketCategory: PacketCategory
): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const weight = control.value;
    if (!weight || !selectedPacketCategory) {
      return null;
    }
    const maxWeight = selectedPacketCategory.maxWeight;
    return weight > maxWeight || weight <= 0 ? { weightExceeded: { value: weight } } : null;
  };
}
