import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketsDeliveredComponent } from './packets-delivered.component';

describe('PacketsDeliveredComponent', () => {
  let component: PacketsDeliveredComponent;
  let fixture: ComponentFixture<PacketsDeliveredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketsDeliveredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketsDeliveredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
