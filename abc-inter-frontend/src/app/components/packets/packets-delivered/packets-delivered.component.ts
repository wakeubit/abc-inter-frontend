import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';

import { ToastrService } from 'ngx-toastr';
import { UtilityService } from '../../../services/utility/utility.service';
import { DeliveriesService } from 'src/app/services/deliveries/deliveries.service';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { Delivery } from 'src/app/models/delivery/delivery';
import { Registration } from 'src/app/models/registration/registration';
import { TranslateService } from '@ngx-translate/core';

interface DeliveryInfo {
  registration: Registration,
  customer: string,
  destination: string,
  delivery: Delivery
}

@Component({
  selector: 'app-packets-delivered',
  templateUrl: './packets-delivered.component.html',
  styleUrls: ['./packets-delivered.component.css']
})
export class PacketsDeliveredComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;

  public columnDefs = [{}];

  public autoGroupColumnDef: {};

  public defaultColDef = {};

  public statusBar = {};

  public sideBar: {};

  public rowData = [];

  public selectedAction: String;

  public confirmHeaderTitle: String;

  public totalRows: number = 0;

  data: Array<DeliveryInfo>;

  deliveriesList: Array<DeliveryInfo>;

  constructor(
    private utilityService: UtilityService,
    private toastrService: ToastrService,
    private deliveriesService: DeliveriesService,
    private registrationService: RegistrationService,
    private translateService: TranslateService
  ) {
    this.attributInitialisation();
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.getDeliveries();
  }

  attributInitialisation() {
    this.columnDefs = [
      {
        headerName: this.translateService.instant('deliveries.delivery_date'),
        children: [
          {
            headerName: 'Date',
            field: 'date',
            hide: false,
            columnGroupShow: 'closed'
          },
          {
            headerName: this.translateService.instant('deliveries.delivery_id'),
            field: 'deliveryID',
            filter: 'agSetColumnFilter',
            hide: !this.utilityService.isAdmin,
            columnGroupShow: 'open'
          },
          {
            headerName: this.translateService.instant('deliveries.delivery_nb'),
            field: 'globalNB',
            hide: true,
            rowGroup: true
          }
        ]
      },
      {
        headerName: this.translateService.instant('registration.receiver'),
        children: [
          {
            headerName: this.translateService.instant('global.name'),
            field: 'receiverName',
            columnGroupShow: 'closed'
          },
          {
            headerName: this.translateService.instant('global.address'),
            field: 'receiverAddress',
            columnGroupShow: 'open'
          },
          {
            headerName: this.translateService.instant('global.gender'),
            field: 'receiverGender',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: this.translateService.instant('global.email'),
            field: 'receiverEmail',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: this.translateService.instant('global.phone'),
            field: 'receiverPhone',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: this.translateService.instant('global.packets'),
        children: [
          {
            headerName: this.translateService.instant('registration.packet_id'),
            field: 'packetID',
            hide: true,
          },
          {
            headerName: this.translateService.instant('packets.tracknumber'),
            field: 'trackingNb',
            columnGroupShow: 'closed'
          },
          {
            headerName: this.translateService.instant('global.category'),
            field: 'packetCat',
            columnGroupShow: 'open'
          },
          {
            headerName: this.translateService.instant('packets.weight'),
            field: 'weight',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: this.translateService.instant('registration.sender'),
        children: [
          {
            headerName: this.translateService.instant('global.name'),
            field: 'senderName',
            sortable: false,
            columnGroupShow: 'closed'
          },
          {
            headerName: this.translateService.instant('global.email'),
            field: 'senderEmail',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: this.translateService.instant('global.phone'),
            field: 'senderPhone',
            sortable: false,
            columnGroupShow: 'open'
          },
          {
            headerName: this.translateService.instant('global.address'),
            field: 'senderAddress',
            sortable: false,
            columnGroupShow: 'open'
          }
        ]
      },
      {
        headerName: this.translateService.instant('deliveries.done_by'),
        children: [
          {
            headerName: this.translateService.instant('operators.personal-number'),
            field: 'personalNb',
            sortable: false,
            columnGroupShow: 'closed',
            hide: false
          },
          {
            headerName: this.translateService.instant('global.name'),
            field: 'operatorName',
            sortable: false,
            columnGroupShow: 'open',
            hide: !this.utilityService.isAdmin
          },
          {
            headerName: this.translateService.instant('global.phone'),
            field: 'operatorPhone',
            sortable: false,
            columnGroupShow: 'open',
            hide: !this.utilityService.isAdmin
          },
          {
            headerName: this.translateService.instant('global.email'),
            field: 'operatorEmail',
            sortable: false,
            columnGroupShow: 'open',
            hide: !this.utilityService.isAdmin
          }
        ]
      }
    ];

    this.defaultColDef = {
      menuTabs: ['filterMenuTab'],
      sortable: true,
      filter: true,
      resizable: true,
      minWidth: 150,
      suppressMovable: true,
      flex: 1
    };

    this.autoGroupColumnDef = {
      headerName: this.translateService.instant('registration.registNB'),
      field: 'globalNB',
      cellRenderer: 'agGroupCellRenderer',
      width: 250,
      flex: 1,
      pinned: 'left',
      headerTooltip: '',
      cellRendererParams: {
        checkbox: false
      }
    };

    this.statusBar = {
      statusPanels: [
        { align: 'left', statusPanel: 'agTotalRowCountComponent' }
      ]
    }

    // Display sidebar only for user admin
    if (this.utilityService.isAdmin) {
      this.sideBar = {
        toolPanels: [
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel'
          }
        ]
      }
    }
  }

  getDeliveries() {
    this.data = new Array<DeliveryInfo>();
    this.deliveriesList = new Array<DeliveryInfo>();
    this.deliveriesService.findAll().subscribe(deliveries => {
      if (deliveries) {
        // compute total packets on each delivery
        for (let i = 0; i < deliveries.length; ++i) {
          this.getRegistration(deliveries[i].deliveredPackets[0].registrationUuid).then(registration => {
            const data: DeliveryInfo = {
              registration: registration,
              customer: `${registration.sender.user.lastname} ${registration.sender.user.firstname}`,
              destination: `${registration.receivingAddress.streetAndHouseNumber} (${registration.receivingAddress.city})`,
              delivery: deliveries[i]
            }
            // filter deliveries by operator
            if (this.utilityService.isAdmin || this.utilityService.isStorekeeper) {
              this.data.push(data);
              this.populateRowData(this.data);
            } else {
              if (deliveries[i].deliverBy.user.userUuid == this.utilityService.userId) {
                this.data.push(data);
                this.populateRowData(this.data);              }
            }
          });
        }
        this.deliveriesList = this.data;
      }
    }, err => {
      if (err?.error?.error) {
        console.error(err?.error?.error);
      }
    });
  }

  getRegistration(registrationID: string): Promise<Registration> {
    return new Promise((resolve, reject) => {
      this.registrationService.findById(registrationID).subscribe(registration => {
        if (registration) {
          resolve(registration);
        }
      });
    });
  }

  getUserDeliveries(data: DeliveryInfo[]): Promise<DeliveryInfo[]> {
    return new Promise((resolve, reject) => {
      let result = data.filter(item => {
        return item;
      });
      resolve(result);
    });
  }

  private createRow(item: DeliveryInfo): {}[] {
    let rows = [];

    for (let i = 0; i < item.delivery.deliveredPackets.length; ++i) {
      const row = {
        globalNB: item.registration.globalTrackingNumber,
        deliveryID: item.delivery.delivranceUuid,
        registrationNB: item.registration.globalTrackingNumber,
        date: this.reverseTime(item.registration?.createdAt.split('T')[0]) + ', ' + item.registration?.createdAt.split('T')[1].split('\.')[0],
        receiverName: `${item.registration.receiver.lastname} ${item.registration.receiver.firstname}`,
        receiverAddress: `${item.registration.receivingAddress.streetAndHouseNumber} (${item.registration.receivingAddress.city} - ${item.registration.receivingAddress.country})`,
        receiverEmail: item.registration.receiver.email,
        receiverGender: item.registration.receiver.gender,
        receiverPhone: `${item.registration.receiver.tel} / ${item.registration.receivingAddress.telAtAddress}`,
        packetID: item.delivery.deliveredPackets[i].packetUuid,
        packetCat: item.delivery.deliveredPackets[i].shippingCost.category.name,
        trackingNb: item.delivery.deliveredPackets[i].trackingNumber,
        weight: `${item.delivery.deliveredPackets[i].weight}`,
        senderName: `${item.registration.sender.user.lastname} ${item.registration.sender.user.firstname}`,
        senderAddress: `${item.registration.sender.postalAddress.streetAndHouseNumber} (${item.registration.sender.postalAddress.city} - ${item.registration.sender.postalAddress.country})`,
        senderPhone: item.registration.sender.user.tel.toString(),
        senderEmail: item.registration.sender.user.email,
        personalNb: item.delivery.deliverBy.personalNumber,
        operatorName: `${item.delivery.deliverBy.user.lastname} ${item.delivery.deliverBy.user.firstname}`,
        operatorPhone: item.delivery.deliverBy.user.tel.toString(),
        operatorEmail: item.delivery.deliverBy.user.email,
        operatorUserID: item.delivery.deliverBy.user.userUuid,
      };

      rows.push(row);
    }
    return rows;
  }

  reverseTime(time: string): string {
    return time.split("-").reverse().join("-");
  }

  // Add information to the table
  private populateRowData(list: DeliveryInfo[]) {
    this.rowData = [];
    // Build and add new information for the table
    list.map(item => {
      const rows = this.createRow(item);
      for (let i=0; i < rows.length; ++i) {
        this.rowData.push(rows[i]);
      }
      ++this.totalRows;
    });
  }

}
