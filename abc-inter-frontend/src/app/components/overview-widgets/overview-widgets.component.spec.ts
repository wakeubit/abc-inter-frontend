import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewWidgetsComponent } from './overview-widgets.component';

describe('OverviewWidgetsComponent', () => {
  let component: OverviewWidgetsComponent;
  let fixture: ComponentFixture<OverviewWidgetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewWidgetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
