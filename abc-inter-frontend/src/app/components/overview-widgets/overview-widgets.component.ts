import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { STATUSTYPE } from 'src/app/models/statusType/statusType';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { DeliveriesService } from 'src/app/services/deliveries/deliveries.service';
import { OperatorsService } from 'src/app/services/operators/operators.service';
import { PacketStoreService } from 'src/app/services/packet-store/packet-store.service';
import { PacketsService } from 'src/app/services/packets/packets.service';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { ShippingCostService } from 'src/app/services/shipping-cost/shipping-cost.service';
import { UtilityService } from 'src/app/services/utility/utility.service';

@Component({
  selector: 'app-overview-widgets',
  templateUrl: './overview-widgets.component.html',
  styleUrls: ['./overview-widgets.component.css']
})
export class OverviewWidgetsComponent implements OnInit {

  public brandName = "AbcInter";
  public overviews = [];
  public totalToBeDelivered: number = 0;
  public totalDeliveriesInprogress: number = 0;
  public totalReturnedDeliveries: number = 0;
  public totalDeliveries: number = 0;
  public totalOperators: number = 0;
  public totalCustomers: number = 0;
  public totalPackets: number = 0;
  public totalStores: number = 0;
  public totalShippingCost: number = 0;

  constructor(
    private utilityService: UtilityService,
    private translateService: TranslateService,
    private registrationService: RegistrationService,
    private deliveryService: DeliveriesService,
    private operatorService: OperatorsService,
    private customerService: CustomersService,
    private packetService: PacketsService,
    private storeService: PacketStoreService,
    private shippingCostService: ShippingCostService
  ) { }

  ngOnInit(): void {
    this.initData();
  }

  private initData() {
    // operators list
    this.getAllOperators();

    // customers list
    this.getAllCustomers();

    // packets list
    this.getAllPacktes();

    // stores list
    this.getAllStores();

    // shipping cost list
    this.getAllShippingCosts();

    this.getTotalDeliveriesInprogress();

    this.getAllRegistrations().then(result => {
      this.totalToBeDelivered = result;
      // get also deliveries
      this.getAllDeliveries().then(result => {
        this.totalDeliveries = result;
        this.overviews = [
          this.addRow(this.translateService.instant('deliveries.already_done'), 'fas fa-check', this.totalDeliveries, 'bg-success', ''),
          this.addRow(this.translateService.instant('global.packets'), 'fas fa-box', this.totalPackets, 'bg-gradient-default', ''),
          this.addRow(this.translateService.instant('packet-store.packet-stores'), 'fas fa-store', this.totalStores, 'bg-default', ''),
          this.addRow(this.translateService.instant('global.shipping-cost'), 'fas fa-user-alt', this.totalShippingCost, 'bg-gradient-info', ''),
          this.addRow(this.translateService.instant('customers.customers'), 'fas fa-user-alt', this.totalCustomers, 'bg-info', '')
        ];

        if (this.utilityService.isAdmin) {
          this.overviews.push(this.addRow(this.translateService.instant('global.operators'), 'fas fa-user-tie', this.totalOperators, 'bg-gradient-primary', ''));
        }
      });
    });
  }

  private addRow(name: string, icon: string, total: number, bg: string, link: string): RowData {
    let row: RowData = {
      name: name,
      icon: icon,
      total: total,
      bg: bg,
      link: link
    }
    return row;
  }

  getTotalDeliveriesInprogress() {
    this.registrationService.findAllByStatus(STATUSTYPE.READY_FOR_DELIVERY).subscribe(result => {
      if (result) {
        this.totalDeliveriesInprogress = result.totalItems;
      }
    }, err => {
      console.error(err?.error?.message);
    });
  }

  getAllRegistrations(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.registrationService.findAllByStatus(STATUSTYPE.IN_STORE).subscribe(result => {
        if (result) {
          // filter those who have the status "IN_STORE"
          const toBeDelivered = result.totalItems;
          resolve(toBeDelivered);
        }
      }, err => {
        console.error(err?.error?.message);
      });
    });
  }

  getAllDeliveries(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.deliveryService.findAll().subscribe(deliveries => {
        deliveries = deliveries.filter(x => x.deliveredPackets.length > 0 && x.deliverBy);
        // filter deliveries by user
        if (this.utilityService.isDeliverer) {
          const userDeliveries = deliveries.filter(delivery => {
            return delivery.deliverBy.user.userUuid === this.utilityService.userId;
          });
          resolve(userDeliveries.length);
        } else {
          resolve(deliveries.length);
        }
      }, err => {
        console.error(err?.error?.message);
      });
    });
  }

  getAllOperators() {
    this.operatorService.getAllOperators().subscribe(operators => {
      if (operators) {
        this.totalOperators = operators.length;
      }
    }, err => {
      console.error(err?.error?.message);
    });
  }

  getAllCustomers() {
    this.customerService.findAllCustomers().subscribe(customers => {
      if (customers) {
        this.totalCustomers = customers.length;
      }
    }, err => {
      console.error(err?.error?.message);
    });
  }

  getAllPacktes() {
    this.packetService.findAllPackets().subscribe(result => {
      if (result) {
        this.totalPackets = result.totalItems;
      }
    }, err => {
      console.error(err?.error?.message);
    });
  }

  getAllStores() {
    this.storeService.findAllStore().subscribe(stores => {
      if (stores) {
        this.totalStores = stores.length;
      }
    }, err => {
      console.error(err?.error?.message);
    });
  }

  getAllShippingCosts() {
    this.shippingCostService.getAllShippingCosts().subscribe(shippingCosts => {
      if (shippingCosts) {
        this.totalShippingCost = shippingCosts.length;
      }
    }, err => {
      console.error(err?.error?.message);
    })
  }

}

interface RowData {
  name: string,
  icon: string,
  total: number,
  bg: string,
  link: string
}
