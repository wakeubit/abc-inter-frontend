import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDividerModule } from '@angular/material/divider';
import { MatBadgeModule } from '@angular/material/badge';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


import 'ag-grid-enterprise';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2TelInputModule } from 'ng2-tel-input';

import { BarcodeGeneratorAllModule, QRCodeGeneratorAllModule } from '@syncfusion/ej2-angular-barcode-generator';
import { NgxQrcodeStylingModule } from 'ngx-qrcode-styling';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { VerticalMenuComponent } from './vertical-menu/vertical-menu.component';
import { PacketsDeliveredComponent } from './packets/packets-delivered/packets-delivered.component';
import { PacketsToBeDeliveredComponent } from './packets/packets-to-be-delivered/packets-to-be-delivered.component';
import { PacketsReadyForDeliveryComponent } from './packets/packets-ready-for-delivery/packets-ready-for-delivery.component';
import { PacketsInStoreComponent } from './packets/packets-in-store/packets-in-store.component';
import { ReceiptImpressionComponent } from './impressions/receipt/receipt-impression.component';
import { PacketFormComponent } from './packets/packet-form/packet-form.component';
import { UserRegistrationFormComponent } from './user-registration-form/user-registration-form.component';
import { CustomerRegistrationFormComponent } from './customer-registration-form/customer-registration-form.component';
import { AddressFormComponent } from './address-form/address-form.component';
import { OverviewWidgetsComponent } from './overview-widgets/overview-widgets.component';
import { PriceListComponent } from './price-list/price-list.component';
import { StatisticComponent } from './statistic/statistic.component';
import { ScannerComponent } from './scanner/scanner.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
    TranslateModule,
    Ng2TelInputModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    MatMenuModule,
    MatCardModule,
    MatStepperModule,
    MatDividerModule,
    MatBadgeModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    BarcodeGeneratorAllModule,
    QRCodeGeneratorAllModule,
    NgxQrcodeStylingModule,
    ZXingScannerModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    VerticalMenuComponent,
    PacketsDeliveredComponent,
    PacketsToBeDeliveredComponent,
    PacketsReadyForDeliveryComponent,
    PacketsInStoreComponent,
    ReceiptImpressionComponent,
    PacketFormComponent,
    UserRegistrationFormComponent,
    CustomerRegistrationFormComponent,
    AddressFormComponent,
    OverviewWidgetsComponent,
    PriceListComponent,
    StatisticComponent,
    ScannerComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    VerticalMenuComponent,
    PacketsDeliveredComponent,
    PacketsToBeDeliveredComponent,
    PacketsReadyForDeliveryComponent,
    PacketsInStoreComponent,
    ReceiptImpressionComponent,
    PacketFormComponent,
    UserRegistrationFormComponent,
    CustomerRegistrationFormComponent,
    AddressFormComponent,
    OverviewWidgetsComponent,
    PriceListComponent,
    StatisticComponent,
    ScannerComponent
  ]
})
export class ComponentsModule { }
