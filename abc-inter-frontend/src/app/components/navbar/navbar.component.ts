import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Location } from '@angular/common';

import { RoutingService } from '../../services/routing-service/routing.service';

import { UtilityService } from '../../services/utility/utility.service';
import { UsersService } from '../../services/users/users.service';
import { RegistrationService } from 'src/app/services/registration/registration.service';

import { DataSubject } from 'src/app/variables/dataSubject';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ])
  ]
})
export class NavbarComponent implements OnInit {

  public focus;

  public listTitles: any[];

  public location: Location;

  ROUTES = [];

  displayName: string;

  userId: string;

  isDefaultAvatar = false;

  host = environment.apiUrl + '/user/avatar/';

  imageUrl: string;

  isAvatarChanged = false;

  public userAvatarUrl: string = null;

  public brand: string = 'Abc Inter';

  totalTasks = 0;

  isRoleDeliver: boolean = false;

  constructor(
    location: Location,
    private routingService: RoutingService,
    private utilityService: UtilityService,
    private usersService: UsersService,
    private registrationService: RegistrationService
  ) {
    this.location = location;
    this.ROUTES = this.routingService.routes;
  }

  ngOnInit() {
    this.listTitles = this.ROUTES.filter(listTitle => listTitle);
    this.userId = this.utilityService.userId;
    this.getUserInfo();
    this.getUserAvatar(this.userId);
    this.userAvatarUrl = `${this.host}${this.userId}`;
    this.getUserTotalCurrentTasks();
  }

  /** Display current page title */
  getTitle() {
    let title = this.location.prepareExternalUrl(this.location.path());
    if (title.charAt(0) === '#') {
      title = title.slice(1);
    }
    return this.listTitles.find(x => x.path === title)?.title ?? 'Dashboard';
  }

  getUserAvatar(userUuid: string) {
    this.usersService.findById(userUuid).subscribe(
      {
        next: (user) => {
          if (null != user) {
            if (null == user.profileImg || user.profileImg === '') {
              this.isDefaultAvatar = true;
            }
          }
        },
        error: () => {
          this.isDefaultAvatar = true;
        },
      }
    )
  }

  getUserInfo() {
    this.isRoleDeliver = this.utilityService.isDeliverer;
    this.utilityService.currentData.subscribe(dataSubject => {
      // check if the username has changed and hole the new value to be displaying
      if (null != dataSubject) {
        if (dataSubject.hasOwnProperty('username')) {
          this.displayName = dataSubject['username'];
        }
        if (dataSubject.hasOwnProperty('image')) {
          this.imageUrl = dataSubject['image'];
          this.isAvatarChanged = true;
          this.isDefaultAvatar = false;
        }
      } else {
        // get the username from the localstorage if the value in the Observer is null
        this.displayName = this.utilityService.username;
      }
    });
  }

  userTaskObserver() {
    this.utilityService.currentData.subscribe(dataSubject => {
      if (dataSubject) {
        const isAdd: boolean = dataSubject['notification']['taskAdded'];
        const isRemove: boolean = dataSubject['notification']['taskRemoved'];
        if (isAdd) {
          ++this.totalTasks;
          // value reinitialization
          DataSubject.notification.taskAdded = false;
        }

        if (isRemove) {
          --this.totalTasks;
          DataSubject.notification.taskRemoved = false;
        }
      }
    });
  }

  getUserTotalCurrentTasks() {
    this.registrationService.findAllByAssignedTo(this.utilityService.userId).subscribe(result => {
      if (result) {
        this.totalTasks = result?.totalItems;
      }
    });
    // observe the user todolist
    this.userTaskObserver();
  }

  toggleMenu() {
    const sidebarwidth = document.getElementById('sidebar').style.width;
    const curWindowSize = window.screen.width;
    if (curWindowSize < 961) {
      if (sidebarwidth == '' || sidebarwidth == '0px') {
        this.resizeWindow('inline', '260px', '0');
      } else {
        this.resizeWindow('none', '0', '0');
      }
    } else {
      if (sidebarwidth == '' || sidebarwidth == '260px') {
        this.resizeWindow('none', '0', '0');
      } else {
        this.resizeWindow('inline', '260px', '260px');
      }
    }
  }

  resizeWindow(state: string, width: string, marginLeft: string) {
    document.getElementById('sidebar').style.display = state;
    document.getElementById('sidebar').style.width = width;
    document.getElementById('parent-content').style.marginLeft = marginLeft;
  }

  logout(): void {
    this.utilityService.logout();
  }

  hideMessage() {
    document.getElementById('msg-box').style.display = 'none';
  }
}
