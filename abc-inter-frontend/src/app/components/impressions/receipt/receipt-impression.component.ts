import { Component, OnInit } from '@angular/core';
import { DisplayTextModel } from '@syncfusion/ej2-angular-barcode-generator';

import { UtilityService } from '../../../services/utility/utility.service';

import { Registration } from '../../../models/registration/registration';
import { Packet } from 'src/app/models/packet/packet';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-receipt-impression',
  templateUrl: './receipt-impression.component.html',
  styleUrls: ['./receipt-impression.component.css']
})
export class ReceiptImpressionComponent implements OnInit {

  public readonly packetstat_url = environment.apiUrl + '/packet-states-group';
  public registration: Registration;
  public barcodeText: DisplayTextModel;

  constructor(private utilityService: UtilityService) { 
    this.barcodeText = {
      text: '123456789'
    }
  }

  ngOnInit(): void {
    this.getRegistration();
  }

  private getRegistration() {
    this.utilityService.currentData.subscribe(dataSubject => {
      if (dataSubject) {
        this.registration = dataSubject['newPacketRegistration'];
      }
    });
  }

  public codeRegistrationNB(): string {
    this.barcodeText = {
      text: `${this.registration?.globalTrackingNumber}`
    }
    return `No. ${this.registration?.globalTrackingNumber}`;
  }

  public codePacketInformation(packet: Packet): string {
    return `ID: ${packet.packetUuid}, No. suivi: ${packet.trackingNumber}, Poids: ${packet.weight}kg, ` + 
    `Exp. ${this.registration?.sender.user.lastname} ${this.registration?.sender.user.firstname} (Tel: ${this.registration?.sender.user.tel}), ` + 
    `Rec. ${this.registration?.receiver.lastname} ${this.registration?.receiver.firstname} (Tel: ${this.registration?.receiver.tel}), ` +
    `Dest. ${this.registration?.receivingAddress.streetAndHouseNumber ?? ''} (${this.registration?.receivingAddress.city ?? ''}), ` + 
    `Magasin: ${this.registration?.store.name} ${this.registration?.store.address.streetAndHouseNumber ?? ''} (${this.registration?.store.address.city ?? ''}), `+
    `Fait par: ${this.registration?.registeredBy.personalNumber}, le ${this.registration?.createdAt.split('T')[0]}`;
  }

  public toTrackingnumberQrCode(trackingNb: string): string {
    // Todo: Change it. Only the tracking number will be needed
    return `REG.: ${trackingNb}`;
  }
}
