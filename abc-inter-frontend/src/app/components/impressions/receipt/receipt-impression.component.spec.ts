import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptImpressionComponent } from './receipt-impression.component';

describe('PrintReceiptComponent', () => {
  let component: ReceiptImpressionComponent;
  let fixture: ComponentFixture<ReceiptImpressionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptImpressionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptImpressionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
