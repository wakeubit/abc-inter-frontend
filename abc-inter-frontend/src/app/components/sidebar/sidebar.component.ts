import { Component, OnInit } from '@angular/core';
//import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';
import { RoutingService } from '../../services/routing-service/routing.service';

import { UtilityService } from '../../services/utility/utility.service';
import { UsersService } from '../../services/users/users.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  ROUTES = [];
  public menuItems: any[];
  public isCollapsed = true;
  public userId: string;
  public isDefaultAvatar = false;
  public host = environment.apiUrl + '/user/avatar/';
  public imageUrl: string;
  public isAvatarChanged = false;
  public userAvatarUrl: string = null;
  public displayName: string = null;
  public user = null;
  public menuState = 'in';

  constructor(
    private router: Router,
    private routingService: RoutingService,
    private utilityService: UtilityService,
    private usersService: UsersService
  ) {
    this.ROUTES = routingService.routes;
    // default user value
    this.user = { 'firstname': this.utilityService.user.username };
  }

  ngOnInit() {
    this.menuItems = this.ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
    this.userId = this.utilityService.userId;
    this.getUserInfo();
    this.getUserAvatar(this.userId);
    this.userAvatarUrl = `${this.host}${this.userId}`;
  }

  getUserAvatar(userUuid: string) {
    this.usersService.findById(userUuid).subscribe((user) => {
      if (null != user) {
        // save user
        this.user = user;
        if (null == user.profileImg || user.profileImg === '') {
          this.isDefaultAvatar = true;
        }
      }
    }, err => {
      this.isDefaultAvatar = true;
    });
  }

  getUserInfo() {
    this.utilityService.currentData.subscribe(dataSubject => {
      // check if the username has changed and hole the new value to be displaying
      if (null != dataSubject) {
        if (dataSubject.hasOwnProperty('username')) {
          this.displayName = dataSubject['username'];
        }
        if (dataSubject.hasOwnProperty('image')) {
          this.imageUrl = dataSubject['image'];
          this.isAvatarChanged = true;
          this.isDefaultAvatar = false;
        }
      } else {
        // get the username from the localstorage if the value in the Observer is null
        this.displayName = this.utilityService.username;
      }
    });
  }

  logout(): void {
    this.utilityService.logout();
  }
}
