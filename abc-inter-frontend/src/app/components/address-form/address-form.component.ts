import { Component, EventEmitter, OnInit, Output, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Registration } from 'src/app/models/registration/registration';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { Address } from '../../models/address/address';
import {UtilityService} from '../../services/utility/utility.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})
export class AddressFormComponent implements OnInit, AfterViewInit {

  formGroup: FormGroup;

  address: Address;

  type: string;

  @Output()
  shareObject: EventEmitter<{}> = new EventEmitter<{}>();

  registrationID: string;

  registration: Registration;
  isFixPhoneValid: boolean;
  errors = {
    fixPhoneNb: '',
    phoneNb: '',
    errorMessage: ''
  };

  constructor(
    private formBuilder: FormBuilder,
    private activatedroute: ActivatedRoute,
    private utilityService: UtilityService,
    private translate: TranslateService,
    private registrationService: RegistrationService
  ) {
  }

  ngOnInit(): void {
    this.initData();
  }

  ngAfterViewInit() {
    this.emitAddress();
  }

  initForm() {
    this.formGroup = this.formBuilder.group({
      cityCtrl: ['Douala', Validators.required],
      countryCtrl: ['Cameroun', Validators.required],
      streetAndHNBCtrl: ['', Validators.required],
      fixNumberCtrl: '',
      detailsCtrl: ''
    });
  }

  initAddressFormValues() {
    this.activatedroute.paramMap.subscribe(params => {
      this.registrationID = params.get('id');
      this.type = params.get('type');
    });

    // Value of the parameter type help to know where service to call
    if (this.registrationID !== null && this.type !== null) {
      if (this.type === 'packet') {
        // than will need to call the registration service
        this.registrationService.findById(this.registrationID).subscribe(registration => {
          if (registration) {
            this.formGroup.setValue({
              cityCtrl: registration.receivingAddress.city,
              countryCtrl: registration.receivingAddress.country,
              streetAndHNBCtrl: registration.receivingAddress.streetAndHouseNumber,
              fixNumberCtrl: registration.receivingAddress.telAtAddress,
              detailsCtrl: registration.receivingAddress.additionalInformation
            });
          }
        }, err => {
          console.error(err?.error?.message);
        });
      }
    }
  }

  initData() {
    this.initForm();
    this.initAddressFormValues();
  }


  fetchRegistration(): Promise<Registration> {
    return new Promise((resolve, reject) => {
      this.registrationService.findById(this.registrationID).subscribe(result => {
        resolve(result);
      }, err => {
        console.error(err?.error?.message);
      })
    });
  }

  emitAddress() {
    this.formGroup.valueChanges.subscribe(x => {
      const address = new Address(
        this.formGroup.value.cityCtrl,
        this.formGroup.value.countryCtrl,
        this.formGroup.value.streetAndHNBCtrl,
        this.formGroup.value.detailsCtrl,
        this.formGroup.value.fixNumberCtrl,
        0,
        0
      );
      this.fetchRegistration().then(result => {
        this.shareObject.emit({
          'address': address,
          'registration': result,
          'type': this.type
        });
      });
    });
  }

  checkValidFixPhoneNumber(event) {
    const reg = this.utilityService.regFixOrPortableNumber;
    this.checkFixPhone(reg.test(event.target.value));
  }

  private checkFixPhone(isValid: boolean) {
    this.isFixPhoneValid = isValid;
    if (this.isFixPhoneValid) {
      this.errors.fixPhoneNb = '';
    } else {
      this.errors.fixPhoneNb = this.translate.instant('global.phone-input-error');
    }
  }
}
