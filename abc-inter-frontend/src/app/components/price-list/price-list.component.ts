import { Component, OnInit } from '@angular/core';
import { ShippingCost } from 'src/app/models/shippingCost/shippingCost';
import { ShippingCostService } from 'src/app/services/shipping-cost/shipping-cost.service';

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.css']
})
export class PriceListComponent implements OnInit {

  public shippingCosts: ShippingCost[];

  constructor(
    private shippingCostService: ShippingCostService
  ) { }

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.getAllShippingCosts();
  }

  getAllShippingCosts() {
    this.shippingCostService.getAllShippingCosts().subscribe(shippingCosts => {
      if (shippingCosts) {
        this.shippingCosts = shippingCosts;
      }
    }, err => {
      console.error(err?.error?.message);
    });
  }
}
