import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/models/user/user';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-user-registration-form',
  templateUrl: './user-registration-form.component.html',
  styleUrls: ['./user-registration-form.component.css']
})
export class UserRegistrationFormComponent implements OnInit, AfterViewInit {

  telOptions = { initialCountry: 'cm', onlyCountries: ['cm'], customPlaceholder: '6 92 45 37 34' };

  genders = [];

  userFormGroup: FormGroup;

  hideInput = false;

  userToEdit: User;

  initialGender: string;

  isPhoneValid: boolean = false;

  errors = {
    phoneNb: '',
    errorMessage: ''
  };

  isUser: boolean = false;

  @Output() shareObject: EventEmitter<{}> = new EventEmitter<{}>();

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private activatedroute: ActivatedRoute,
    private userService: UsersService,
    private registrationService: RegistrationService
  ) {
  }

  ngOnInit(): void {
    this.initData();
  }

  ngAfterViewInit() {
    this.emitUserObject();
    this.onInputChange();
  }

  initData() {
    this.userFormFieldsInit();
    this.initUserFormValues();
  }

  userFormFieldsInit() {
    this.userFormGroup = this.formBuilder.group({
      usernameCtrl: ['', Validators.required],
      firstnameCtrl: ['', Validators.required],
      lastnameCtrl: ['', Validators.required],
      emailCtrl: ['', Validators.email],
      phoneCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      passwordCtrl: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      rPasswordCtrl: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  initGenderOptions(initialValue: string): void {
    if (initialValue.toLowerCase() === 'female') {
      this.genders = [
        this.translate.instant('global.male'),
        this.translate.instant('global.others')
      ]
    } else if (initialValue.toLowerCase() === 'male') {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.others')
      ]
    } else if (initialValue.toLowerCase() === 'others') {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.male')
      ]
    } else {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.male'),
        this.translate.instant('global.others')
      ]
    }
  }

  convertSelectedGender(value: string): string {
    if (
      value.toLowerCase() === 'female' ||
      value.toLowerCase() === 'feminin' ||
      value.toLowerCase() === 'féminin' ||
      value.toLowerCase() === 'global.female'
    ) {
      return 'Female';
    }
    if (
      value.toLowerCase() === 'male' ||
      value.toLowerCase() === 'masculin' ||
      value.toLowerCase() === 'global.male'
    ) {
      return 'Male';
    }
    if (
      value.toLowerCase() === 'others' ||
      value.toLowerCase() === 'autres' ||
      value.toLowerCase() === 'global.others'
    ) {
      return 'Others';
    }
    return 'Others';
  }

  displayInitialGenderValue(value: string): string {
    if (value.toLowerCase() === 'female' || value.toLowerCase() === 'feminin') {
      return this.translate.instant('global.female');
    }
    if (value.toLowerCase() === 'male' || value.toLowerCase() === 'masculin') {
      return this.translate.instant('global.male');
    }
    if (value.toLowerCase() === 'others' || value.toLowerCase() === 'autres') {
      return this.translate.instant('global.others');
    }
    return this.translate.instant('global.others');
  }

  initUserFormValues() {
    let id = null;
    this.activatedroute.paramMap.subscribe(params => {
      id = params.get('id');
    });

    // check whether the packet receiver has to be updated or not. We find the user by the parameter id
    if (id !== null) {
      let isUser = false;
      this.getAllUsers().then(users => {
        for (let i = 0; i < users.length; ++i) {
          if (users[i].userUuid === id) {
            isUser = true;
            break;
          }
        }

        if (isUser) {
          this.isUser = true;
          this.userService.findById(id).subscribe(user => {
            if (user) {
              this.userToEdit = user;
              this.initialGender = this.displayInitialGenderValue(user.gender);
              this.initGenderOptions(this.initialGender);

              this.userFormGroup.setValue({
                usernameCtrl: user.username,
                firstnameCtrl: user.firstname,
                lastnameCtrl: user.lastname,
                emailCtrl: user.email,
                phoneCtrl: user.tel,
                genderCtrl: this.initialGender,
                passwordCtrl: user.password,
                rPasswordCtrl: user.password
              });
            }
          }, err => {
            console.error(err?.error?.message);
          });
        } else {
          this.registrationService.findById(id).subscribe(registration => {
            if (registration) {
              this.userToEdit = registration.receiver;
              this.initialGender = this.displayInitialGenderValue(this.userToEdit.gender);
              this.initGenderOptions(this.initialGender);

              this.userFormGroup.setValue({
                usernameCtrl: this.userToEdit.username,
                firstnameCtrl: this.userToEdit.firstname,
                lastnameCtrl: this.userToEdit.lastname,
                emailCtrl: this.userToEdit.email,
                phoneCtrl: this.userToEdit.tel,
                genderCtrl: this.initialGender,
                passwordCtrl: this.userToEdit.password,
                rPasswordCtrl: this.userToEdit.password
              });
            }
          }, err => {
            console.error(err?.error?.message);
          });
        }
      });
    }
  }

  emitUserObject() {
    this.userFormGroup.valueChanges.subscribe(x => {
      const user = {
        userUuid: this.userToEdit.userUuid,
        username: this.userFormGroup.value.usernameCtrl,
        lastname: this.userFormGroup.value.lastnameCtrl,
        firstname: this.userFormGroup.value.firstnameCtrl,
        password: this.userFormGroup.value.passwordCtrl,
        tokenExpired: false,
        email: this.userFormGroup.value.emailCtrl,
        tel: this.userFormGroup.value.phoneCtrl,
        gender: this.convertSelectedGender(this.userFormGroup.value.genderCtrl),
        profileImg: '',
        roles: this.userToEdit.roles
      };
      this.shareObject.emit({
        'user': user,
        'phoneStatus': this.userFormGroup.value.phoneCtrl === user.tel ? true : this.isPhoneValid,
        'rPwd': this.userFormGroup.value.rPasswordCtrl,
        'isUser': this.isUser
      });
    });
  }

  getAllUsers(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      this.userService.findAll().subscribe(users => {
        resolve(users);
      }, err => {
        console.error(err?.error?.message);
      })
    });
  }

  autoCompleteForm() {
    this.getAllUsers().then(users => {
      let userFound = false;
      for (let i = 0; i < users.length; ++i) {
        if (users[i].tel === this.userFormGroup.get('phoneCtrl').value) {
          userFound = true;
          this.userToEdit = users[i];
          this.initialGender = this.displayInitialGenderValue(users[i].gender);
          this.initGenderOptions(this.initialGender);

          this.userFormGroup.setValue({
            usernameCtrl: users[i].username,
            firstnameCtrl: users[i].firstname,
            lastnameCtrl: users[i].lastname,
            emailCtrl: users[i].email,
            phoneCtrl: users[i].tel,
            genderCtrl: this.initialGender,
            passwordCtrl: users[i].password,
            rPasswordCtrl: users[i].password
          });
          break;
        }
      }

      if (!userFound) {
        this.userToEdit.userUuid = null;
      }
    });
  }

  telInputObject(obj) {
    obj.setCountry('cm');
  }

  checkPhoneValue(isValid) {
    this.isPhoneValid = isValid;
    if (this.isPhoneValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  onInputChange() {
    this.userFormGroup.get('phoneCtrl').valueChanges.subscribe(value => {
      if (value.length != 9) {
        this.userFormGroup.get('usernameCtrl').setValue('');
        this.userFormGroup.get('firstnameCtrl').setValue('');
        this.userFormGroup.get('lastnameCtrl').setValue('');
        this.userFormGroup.get('emailCtrl').setValue('');
        this.userFormGroup.get('genderCtrl').setValue('');
        this.userFormGroup.get('passwordCtrl').setValue('');
        this.userFormGroup.get('rPasswordCtrl').setValue('');
      }
    });

    this.userFormGroup.get('rPasswordCtrl').valueChanges.subscribe(value => {
      if (this.userFormGroup.get('passwordCtrl').value !== this.userFormGroup.get('rPasswordCtrl').value) {
        this.errors.errorMessage = this.translate.instant('password.pwd_not_match');
      } else {
        this.errors.errorMessage = '';
      }
    });

    this.userFormGroup.get('passwordCtrl').valueChanges.subscribe(value => {
      if (this.userFormGroup.get('passwordCtrl').value !== this.userFormGroup.get('rPasswordCtrl').value) {
        this.errors.errorMessage = this.translate.instant('password.pwd_not_match');
      } else {
        this.errors.errorMessage = '';
      }
    });
  }
}
