import { Component, OnInit, AfterViewInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";

import {
  Html5Qrcode,
  Html5QrcodeScanType,
  Html5QrcodeSupportedFormats,
} from "html5-qrcode";
import { CameraDevice } from "html5-qrcode/esm/core";
import { TranslateService } from "@ngx-translate/core";
import { PacketStateService } from "src/app/services/packet-state/packet-state.service";
import { UtilityService } from "src/app/services/utility/utility.service";
import { SharedDataService } from "src/app/services/sharedData/shared-data.service";

@Component({
  selector: "app-scanner",
  templateUrl: "./scanner.component.html",
  styleUrls: ["./scanner.component.css"],
})
export class ScannerComponent implements OnInit, AfterViewInit {
  @Output()
  isRedirected: EventEmitter<true> = new EventEmitter<true>();
  // Defines html5Qrcode
  html5Qrcode: Html5Qrcode;
  // Defines supportedFormats
  supportedFormats: Html5QrcodeSupportedFormats[] = [
    Html5QrcodeSupportedFormats.QR_CODE,
    Html5QrcodeSupportedFormats.CODABAR,
    Html5QrcodeSupportedFormats.CODE_128,
    Html5QrcodeSupportedFormats.AZTEC,
    Html5QrcodeSupportedFormats.EAN_13,
    Html5QrcodeSupportedFormats.CODE_93,
    Html5QrcodeSupportedFormats.CODE_39,
    Html5QrcodeSupportedFormats.EAN_8,
    Html5QrcodeSupportedFormats.ITF,
    Html5QrcodeSupportedFormats.MAXICODE,
    Html5QrcodeSupportedFormats.DATA_MATRIX,
    Html5QrcodeSupportedFormats.PDF_417,
    Html5QrcodeSupportedFormats.UPC_A,
    Html5QrcodeSupportedFormats.UPC_E,
    Html5QrcodeSupportedFormats.UPC_EAN_EXTENSION,
  ];

  scannerConfig = {
    // set how fast the code has to scan
    fps: 10,
    qrbox: { width: 450, height: 450 },
    rememberLastUsedCamera: true,
    // Only support camera scan type.
    supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
    formatsToSupport: this.supportedFormats,
    // support barcode scan
    experimentalFeatures: {
      useBarCodeDetectorIfSupported: true,
    },
  };
  // Defines devices.
  devices: CameraDevice[] = [];
  // Defines currentDeviceId
  currentDeviceId: string;
  // Defines currentDeviceIndex
  currentDeviceIndex = 0;
  // Defines scanResult;
  scanResult = {};
  // Defines scanned
  scanned: boolean;
  // Defines isCustomer
  isCustomer: boolean;
  // Defines isPacket
  isPacket: boolean;
  // Defines isRegistration
  isRegistration: boolean;
  //
  loaded: boolean;

  constructor(
    private router: Router,
    private utilityService: UtilityService,
    private packetStateService: PacketStateService,
    private translate: TranslateService,
    private sharedDataService: SharedDataService
  ) {
    this.currentDeviceId = "";
    this.scanned = false;
    this.loaded = true;
  }

  ngOnInit(): void {
    this.isCustomer =
      this.utilityService.isCustomer ||
      !this.utilityService.userId /** user is not connected */;
  }

  ngAfterViewInit(): void {
    // get id where to display the content
    const scanner = document.getElementById("reader");
    this.html5Qrcode = new Html5Qrcode(scanner.id);
    Html5Qrcode.getCameras().then((devices) => {
      if (devices && devices.length) {
        // get the id of the back camera. iff the value is null, then set the first camera that has be found
        this.currentDeviceIndex = devices.findIndex(device => {
          device.label.includes('rear') || device.label.includes('Rear')
        });
        if (this.currentDeviceIndex === -1) this.currentDeviceIndex = 0;
        this.currentDeviceId = devices[this.currentDeviceIndex].id;
        this.startScanner(this.currentDeviceId);
        this.devices = devices;
      } else {
        scanner.innerHTML = this.translate.instant('scanner.no_camera');
        this.loaded = false;
      }
    });
    // change instance
    this.sharedDataService.changeScannerInstance(this.html5Qrcode);
  }

  private startScanner(cameraId: string) {
    this.html5Qrcode.start(
      cameraId,
      this.scannerConfig,
      (x, y) => this.onScanSuccess(x, y),
      (error) => this.onScanFailure(error)
    );
    this.loaded = false;
  }

  private stopScanner() {
    if (this.html5Qrcode.isScanning) {
      this.html5Qrcode.stop();
    }
  }

  // Calls to change the current camera
  public changeDevice() {
    if (this.devices.length > 1) {
      this.stopScanner();
      this.currentDeviceIndex = this.devices.findIndex(
        (x) => x.id !== this.currentDeviceId
      );
      if (this.currentDeviceIndex === -1) this.currentDeviceIndex = 0;
      this.currentDeviceId = this.devices[this.currentDeviceIndex].id;
      this.startScanner(this.currentDeviceId);
    }
  }

  private onScanSuccess(decodedText: string, decodedResult) {
    this.scanned = true;
    // play audio
    let audio: HTMLAudioElement = new Audio(
      '../../../assets/song/scanner-beep.mp3'
    );
    audio.play();
    // stop scanner after < 1s
    setTimeout(() => {
      this.readInformation(decodedText, decodedResult);
      this.stopScanner();
    }, 200);
  }

  private onScanFailure(error) {
    this.scanned = false;
  }

  readInformation(result: string, decodedResult) {
    let isFound = false;
    let regNb = "";

    if (result.includes("REG.:") || result.includes("No.:")) {
      this.isPacket = result.includes("REG.:");
      this.isRegistration = !this.isPacket;
      regNb = result.split(":")[1].replace(" ", "");
      if (regNb.substring(0, 1) == "P" || regNb.substring(0, 1) == "R") {
        this.packetStateService
          .findPacketStatesByGlobalTrackingNumber(regNb)
          .subscribe({
            next: (result) => {
              if (result.globalTrackingNumber) {
                isFound = true;
              }
            },
            error: (err) => {
              // info not found
              this.handleScanEvent(regNb, isFound, err);
            },
            complete: () => {
              this.handleScanEvent(regNb, isFound);
            },
          });
      } else {
        this.handleScanEvent(result, isFound);
      }
    } else {
      this.handleScanEvent(result, isFound);
    }
  }

  private handleScanEvent(result: string, isFound: boolean, error?: any) {
    this.scanned = true;
    if (error) {
      console.error(error);
    }

    if (isFound && this.isCustomer /** also not connected user */) {
      // notify child
      this.isRedirected.emit(true);
      // Redirect the customer to the status of packets
      this.router.navigate(["packet-tracking", result]);
    } else if (isFound && !this.isCustomer) {
      if (result.includes('No.')) {
        result = result.split('.')[1].replace(' ', '').trim();
      }
      this.scanResult = {
        isOneField: true,
        text: `${this.translate.instant('registration.registNB')}: ${result}`,
      };
    } else {
      if (result.includes("No. suivi:") || result.includes("Poids:")) {
        const contents = result.split(",");
        this.scanResult = {
          isOneField: false,
          id: `${this.translate.instant('registration.packet_id')}: ${contents[0].split(":")[1]}`,
          tracking: `${this.translate.instant('packets.tracknumber')}: ${contents[1].split(":")[1]}`,
          weight: `${this.translate.instant('packets.weight')}: ${contents[2].split(":")[1]}`,
          sender: `${this.translate.instant('registration.sender')}: ${contents[3].split(".")[1]}`,
          receiver: `${this.translate.instant('registration.receiver')}: ${contents[4].split(".")[1]}`,
          destination: `Destination: ${contents[5].split(".")[1]}`,
          storename: `${this.translate.instant('registration.store_name')}: ${contents[6].split(":")[1]}`,
          autor: `${this.translate.instant('deliveries.done_by')}: ${contents[7].split(":")[1]}`,
        };
      } else {
        this.scanResult = {
          isOneField: true,
          text: result,
        };
      }
    }
  }
}
