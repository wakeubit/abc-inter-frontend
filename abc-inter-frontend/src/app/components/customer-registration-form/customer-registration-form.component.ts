import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerType } from '../../models/customer/customerType';
import { Customer } from '../../models/customer/customer';
import { TranslateService } from '@ngx-translate/core';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { ActivatedRoute } from '@angular/router';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { RegistrationService } from 'src/app/services/registration/registration.service';
import { User } from 'src/app/models/user/user';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-customer-registration-form',
  templateUrl: './customer-registration-form.component.html',
  styleUrls: ['./customer-registration-form.component.css']
})
export class CustomerRegistrationFormComponent implements OnInit, AfterViewInit {

  customerTypes = [];

  initialCustomerType: string;

  genders = [];

  initialGender: string;

  formGroup: FormGroup;

  customer: Customer;

  @Output()
  shareObject: EventEmitter<{}> = new EventEmitter<{}>();

  telOptions = { initialCountry: 'cm', onlyCountries: ['cm'], customPlaceholder: '6 92 45 37 34' };

  isAdmin = false;

  isPhoneValid = false;

  isCustomer = false;

  customerToEdit: Customer;

  errors = {
    fixPhoneNb: '',
    phoneNb: '',
    errorMessage: ''
  };
  isFixPhoneValid: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private utilityService: UtilityService,
    private activatedroute: ActivatedRoute,
    private customerService: CustomersService,
    private registrationService: RegistrationService,
    private userService: UsersService
  ) {
  }

  ngOnInit(): void {
    this.isAdmin = this.isCurrentUserAdmin();
    this.dataInit();
    this.onPhoneChange();
  }

  isCurrentUserAdmin(): boolean {
    return this.utilityService.isAdmin;
  }

  ngAfterViewInit(): void {
    this.emitCustomerObject();
  }

  initForm() {
    this.formGroup = this.formBuilder.group({
      customerTypeCtrl: ['', Validators.required],
      usernameCtrl: ['', Validators.required],
      firstnameCtrl: ['', Validators.required],
      lastnameCtrl: ['', Validators.required],
      emailCtrl: '',
      phoneCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      passwordCtrl: ['', Validators.required],
      cityCtrl: ['Douala', Validators.required],
      countryCtrl: ['Cameroun', Validators.required],
      streetAndHNBCtrl: ['', Validators.required],
      fixNumberCtrl: '',
      detailsCtrl: ''
    });
  }

  initGenderOptions(initialValue: string): void {
    if (initialValue.toLowerCase() === 'female') {
      this.genders = [
        this.translate.instant('global.male'),
        this.translate.instant('global.others')
      ];
    } else if (initialValue.toLowerCase() === 'male') {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.others')
      ];
    } else if (initialValue.toLowerCase() === 'others') {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.male')
      ];
    } else {
      this.genders = [
        this.translate.instant('global.female'),
        this.translate.instant('global.male'),
        this.translate.instant('global.others')
      ];
    }
  }

  convertSelectedGender(value: string): string {
    if (
      value.toLowerCase() === 'female' ||
      value.toLowerCase() === 'feminin' ||
      value.toLowerCase() === 'féminin' ||
      value.toLowerCase() === 'global.female'
    ) {
      return 'Female';
    }
    if (
      value.toLowerCase() === 'male' ||
      value.toLowerCase() === 'masculin' ||
      value.toLowerCase() === 'global.male'
    ) {
      return 'Male';
    }
    if (
      value.toLowerCase() === 'others' ||
      value.toLowerCase() === 'autres' ||
      value.toLowerCase() === 'global.others'
    ) {
      return 'Others';
    }
    return 'Others';
  }

  getInitialGenderValue(value: string): string {
    if (value.toLowerCase() === 'female' || value.toLowerCase() === 'feminin') {
      return this.translate.instant('global.female');
    }
    if (value.toLowerCase() === 'male' || value.toLowerCase() === 'masculin') {
      return this.translate.instant('global.male');
    }
    if (value.toLowerCase() === 'others' || value.toLowerCase() === 'autres') {
      return this.translate.instant('global.others');
    }
    return this.translate.instant('global.others');
  }

  getInitialCustomerTypeValue(value: string): string {
    if (value.toLowerCase() === CustomerType.Anonym.toLowerCase()) {
      return this.translate.instant('global.anonym');
    }

    if (
      value.toLowerCase() === CustomerType.Private.toLowerCase() ||
      value.toLowerCase() === 'privee' ||
      value.toLowerCase() === 'privée'
    ) {
      return this.translate.instant('global.private');
    }

    if (value.toLowerCase() === CustomerType.Business.toLowerCase()) {
      return this.translate.instant('global.business');
    }
  }

  initCustomerTypeOptions(initialValue: string): void {
    if (initialValue?.toLowerCase() === 'anonym') {
      this.customerTypes = [
        this.translate.instant('global.private'),
        this.translate.instant('global.business')
      ];
    } else if (
      initialValue?.toLowerCase() === 'private' ||
      initialValue?.toLowerCase() === 'privee' ||
      initialValue?.toLowerCase() === 'privée'
    ) {
      this.customerTypes = [
        this.translate.instant('global.business'),
        this.translate.instant('global.anonym')
      ];
    } else if (initialValue?.toLowerCase() === 'business') {
      this.customerTypes = [
        this.translate.instant('global.private'),
        this.translate.instant('global.anonym')
      ];
    } else {
      this.customerTypes = [
        this.translate.instant('global.anonym'),
        this.translate.instant('global.business'),
        this.translate.instant('global.private')
      ];
    }
  }

  convertSelectedCustomerType(selectedValue: string): string {
    if (
      selectedValue.toLowerCase() === 'privee' ||
      selectedValue.toLowerCase() === 'privée' ||
      selectedValue.toLowerCase() === 'private' ||
      selectedValue.toLowerCase() === 'global.private'
    ) {
      return CustomerType.Private;
    }

    if (
      selectedValue.toLowerCase() === 'business' ||
      selectedValue.toLowerCase() === 'global.business'
    ) {
      return CustomerType.Business;
    }

    if (
      selectedValue.toLowerCase() === 'anonym' ||
      selectedValue.toLowerCase() === 'global.anonym'
    ) {
      return CustomerType.Anonym;
    }
  }

  initCustomerFormValues() {
    let id = null;
    this.activatedroute.paramMap.subscribe(params => {
      id = params.get('id');
    });

    if (id !== null) {
      // check whether to update packet sender or a simple customer
      let isCustomer = false;
      this.getAllCustomer().then(customers => {
        for (let i = 0; i < customers.length; ++i) {
          if (customers[i].customerUuid === id) {
            isCustomer = true;
            break;
          }
        }

        if (isCustomer) {
          this.isCustomer = true;
          this.customerService.findCustomerById(id).subscribe(customer => {
            if (customer) {
              this.customerToEdit = customer;
              this.initialCustomerType = this.getInitialCustomerTypeValue(this.customerToEdit.customerType);
              this.initCustomerTypeOptions(this.initialCustomerType);
              this.initialGender = this.getInitialGenderValue(this.customerToEdit.user.gender);
              this.initGenderOptions(this.initialGender);

              this.formGroup.setValue({
                customerTypeCtrl: this.initialCustomerType,
                usernameCtrl: this.customerToEdit.user.username,
                firstnameCtrl: this.customerToEdit.user.firstname,
                lastnameCtrl: this.customerToEdit.user.lastname,
                emailCtrl: this.customerToEdit.user.email,
                phoneCtrl: this.customerToEdit.user.tel,
                genderCtrl: this.initialGender,
                passwordCtrl: this.customerToEdit.user.password,
                cityCtrl: this.customerToEdit.postalAddress.city,
                countryCtrl: this.customerToEdit.postalAddress.country,
                streetAndHNBCtrl: this.customerToEdit.postalAddress.streetAndHouseNumber,
                fixNumberCtrl: this.customerToEdit.postalAddress.telAtAddress,
                detailsCtrl: this.customerToEdit.postalAddress.additionalInformation
              });
            }
          }, err => {
            console.error(err?.error?.message);
          });
        } else {
          this.registrationService.findById(id).subscribe(registration => {
            if (registration) {
              this.customerToEdit = registration.sender;
              this.initialCustomerType = this.getInitialCustomerTypeValue(this.customerToEdit.customerType);
              this.initCustomerTypeOptions(this.initialCustomerType);
              this.initialGender = this.getInitialGenderValue(this.customerToEdit.user.gender);
              this.initGenderOptions(this.initialGender);

              this.formGroup.setValue({
                customerTypeCtrl: this.initialCustomerType,
                usernameCtrl: this.customerToEdit.user.username,
                firstnameCtrl: this.customerToEdit.user.firstname,
                lastnameCtrl: this.customerToEdit.user.lastname,
                emailCtrl: this.customerToEdit.user.email,
                phoneCtrl: this.customerToEdit.user.tel,
                genderCtrl: this.initialGender,
                passwordCtrl: this.customerToEdit.user.password,
                cityCtrl: this.customerToEdit.postalAddress.city,
                countryCtrl: this.customerToEdit.postalAddress.country,
                streetAndHNBCtrl: this.customerToEdit.postalAddress.streetAndHouseNumber,
                fixNumberCtrl: this.customerToEdit.postalAddress.telAtAddress,
                detailsCtrl: this.customerToEdit.postalAddress.additionalInformation
              });
            }
          }, err => {
            console.error(err?.error?.message);
          });
        }
      });
    } else {
      console.error('Server down');
    }
  }

  dataInit() {
    this.initForm();
    this.initCustomerFormValues();
  }

  getAllCustomer(): Promise<Customer[]> {
    return new Promise((resolve, reject) => {
      this.customerService.findAllCustomers().subscribe(customers => {
        resolve(customers);
      }, err => {
        console.error(err?.error?.message);
      });
    });
  }

  getAllUsers(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      this.userService.findAll().subscribe(users => {
        resolve(users);
      }, err => {
        console.error(err?.error?.message);
      });
    });
  }

  autoCompleteForm() {
    this.getAllCustomer().then(customers => {
      let customerFound = false;
      for (let i = 0; i < customers.length; ++i) {
        if (customers[i].user.tel === this.formGroup.get('phoneCtrl').value) {
          customerFound = true;
          this.customerToEdit = customers[i];
          this.initialCustomerType = this.getInitialCustomerTypeValue(customers[i].customerType);
          this.initCustomerTypeOptions(this.initialCustomerType);
          this.initialGender = this.getInitialGenderValue(customers[i].user.gender);
          this.initGenderOptions(this.initialGender);
          this.formGroup.setValue({
            customerTypeCtrl: this.initialCustomerType,
            usernameCtrl: customers[i].user.username,
            firstnameCtrl: customers[i].user.firstname,
            lastnameCtrl: customers[i].user.lastname,
            emailCtrl: customers[i].user.email,
            phoneCtrl: customers[i].user.tel,
            genderCtrl: this.initialGender,
            passwordCtrl: customers[i].user.password,
            cityCtrl: customers[i].postalAddress.city,
            countryCtrl: customers[i].postalAddress.country,
            streetAndHNBCtrl: customers[i].postalAddress.streetAndHouseNumber,
            fixNumberCtrl: customers[i].postalAddress.telAtAddress,
            detailsCtrl: customers[i].postalAddress.additionalInformation
          });
          break;
        }
      }

      if (!customerFound) {
        this.customerToEdit.customerUuid = null;
        this.customerToEdit.user.userUuid = null;
        // check whether the user has been already registered as reciever or not
        this.getAllUsers().then(users => {
          for (let i = 0; i < users.length; ++i) {
            if (users[i].tel === this.formGroup.get('phoneCtrl').value) {
              this.customerToEdit.user.userUuid = users[i].userUuid;
              this.initialGender = this.getInitialGenderValue(users[i].gender);
              this.initGenderOptions(this.initialGender);
              this.formGroup.setValue({
                customerTypeCtrl: '',
                usernameCtrl: users[i].username,
                firstnameCtrl: users[i].firstname,
                lastnameCtrl: users[i].lastname,
                emailCtrl: users[i].email,
                phoneCtrl: users[i].tel,
                genderCtrl: this.initialGender,
                passwordCtrl: users[i].password,
                cityCtrl: '',
                countryCtrl: '',
                streetAndHNBCtrl: '',
                fixNumberCtrl: '',
                detailsCtrl: ''
              });
              break;
            }
          }
        });
      }
    });
  }

  checkValidPhoneNumber(event) {
    const reg = this.utilityService.regPortableNumber;
    this.checkPhoneValue(reg.test(event.target.value));
  }

  checkPhoneValue(isValid) {
    this.isPhoneValid = isValid;
    if (this.isPhoneValid) {
      this.errors.phoneNb = '';
    } else {
      this.errors.phoneNb = this.translate.instant('global.phone-input-error');
    }
  }

  emitCustomerObject() {
    this.formGroup.valueChanges.subscribe(x => {
      const customer = {
        customerUuid: this.customerToEdit.customerUuid,
        customerType: this.convertSelectedCustomerType(this.formGroup.value.customerTypeCtrl),
        bonusCount: 0,
        user: {
          userUuid: this.customerToEdit.user.userUuid,
          username: this.formGroup.value.usernameCtrl,
          lastname: this.formGroup.value.lastnameCtrl,
          firstname: this.formGroup.value.firstnameCtrl,
          password: this.formGroup.value.passwordCtrl,
          email: this.formGroup.value.emailCtrl,
          tel: this.formGroup.value.phoneCtrl,
          profileImg: this.customerToEdit.user.profileImg,
          gender: this.convertSelectedGender(this.formGroup.value.genderCtrl),
          roles: this.customerToEdit.user.roles
        },
        postalAddress: {
          city: this.formGroup.value.cityCtrl,
          country: this.formGroup.value.countryCtrl,
          streetAndHouseNumber: this.formGroup.value.streetAndHNBCtrl,
          telAtAddress: this.formGroup.value.fixNumberCtrl,
          additionalInformation: this.formGroup.value.detailsCtrl,
          latitude: 0,
          longitude: 0
        }
      };
      this.shareObject.emit({
        'customer': customer,
        'phoneStatus': this.formGroup.value.phoneCtrl === this.customerToEdit.user.tel ? true : this.isPhoneValid,
        'isCustomer': this.isCustomer
      });
    });
  }

  onPhoneChange() {
    this.formGroup.get('phoneCtrl').valueChanges.subscribe(value => {
      if (value.length != 9) {
        this.formGroup.get('usernameCtrl').setValue('');
        this.formGroup.get('firstnameCtrl').setValue('');
        this.formGroup.get('lastnameCtrl').setValue('');
        this.formGroup.get('emailCtrl').setValue('');
        this.formGroup.get('genderCtrl').setValue('');
        this.formGroup.get('customerTypeCtrl').setValue('');
        this.formGroup.get('passwordCtrl').setValue('');
        this.formGroup.get('cityCtrl').setValue('');
        this.formGroup.get('countryCtrl').setValue('');
        this.formGroup.get('streetAndHNBCtrl').setValue('');
        this.formGroup.get('fixNumberCtrl').setValue('');
        this.formGroup.get('detailsCtrl').setValue('');
      }
    });
  }

  checkValidFixPhoneNumber(event) {
    const reg = this.utilityService.regFixOrPortableNumber;
    this.checkFixPhone(reg.test(event.target.value));
  }

  private checkFixPhone(isValid: boolean) {
    this.isFixPhoneValid = isValid;
    if (this.isFixPhoneValid) {
      this.errors.fixPhoneNb = '';
    } else {
      this.errors.fixPhoneNb = this.translate.instant('global.phone-input-error');
    }
  }

}
