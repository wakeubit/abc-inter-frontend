import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
declare var $: any;
import { Html5Qrcode } from 'html5-qrcode';

import { RoutingService } from '../../services/routing-service/routing.service';
import { SharedDataService } from 'src/app/services/sharedData/shared-data.service';

@Component({
  selector: 'app-vertical-menu',
  templateUrl: './vertical-menu.component.html',
  styleUrls: ['./vertical-menu.component.scss']
})
export class VerticalMenuComponent implements OnInit {

  ROUTES = [];

  public menuItems: any[];

  public html5Qrcode: Html5Qrcode;

  constructor(
    private routingService: RoutingService,
    private modalService: NgbModal,
    private sharedDataService: SharedDataService
  ) {
    this.ROUTES = this.routingService.routes;
  }
 
  ngOnInit() {
    this.menuItems = this.ROUTES.filter(menuItem => menuItem);
  }

  startScanner(content) {
    this.modalService.open(content, { centered: true, scrollable: true, size: 'xl' });
  }

  closeModal(close:boolean) {
    this.modalService.dismissAll();
  }

  onStopClicked() {
    // close the modal
    this.modalService.dismissAll();
    // Get current scanner instance and stop it
    this.sharedDataService.currentScanner.subscribe((curInstance) => {
      if (curInstance && curInstance.isScanning) {
        curInstance.stop();
      }
    }).unsubscribe();
  }
}
